using System;
using System.Collections;
namespace Vision
{
	/// <summary>
	/// 
	/// </summary>
	public class StPointList : System.Collections.ArrayList
	{
		public int[,] DistanceMap;
		public ArrayList NoPointPairList=new ArrayList(); //store the points that can  not be the point pair.
		public StPointList()
		{
			// 
			// TODO: Add constructor logic here
			//
		}
		
		//add a point to the point list.		
		public bool AddPoint(Point2D myPoint)
		{
			int i=0 ;
			foreach(Point2D pt in this)
			{
				/** 
				 * Check if the point is already in the list 
				 */ 
				if(pt.X == myPoint.X && pt.Y == myPoint.Y)
					i++;
			}
			if (i==0 ) {
				this.Add(myPoint);/** < add the point to the list */ 
				return true;} 
			else return false; /**< return false if the point in the list */
			
		}

		public bool DeletePoint(Point2D myPoint)
		{
			int i=0 ;
			foreach(Point2D pt in this)
			{
				if(pt.X == myPoint.X && pt.X == myPoint.X)
					i++;
			}
			if (i==0 ) 
			{return false;}
			else
			{
				this.Remove(myPoint);
				return true;}
			
		}

		public int ReadCount()
		{
			return this.Count;
		}

		public Point2D ReadPoint(int myIndex)
		{
			if (myIndex<=this.Count)
			return (Point2D)this[myIndex];
			else
			return null;
		}

		public StCurve FindNearestPointPair(int lowrange)
		{
			int i=0,j=0,shortestdis=0,n=0,m=0;
					
			for(i=0;i<this.Count;i++)
				for(j=i+1;j<this.Count;j++)
				{			
					if (((Point2D)this[i]).Valid==0 && ((Point2D)this[j]).Valid==0)
					{
						if(shortestdis==0)
						{
							if (DistanceMap[i,j]!=0&&DistanceMap[i,j]>lowrange)
							shortestdis=DistanceMap[i,j];
						}
						if (DistanceMap[i,j]<=shortestdis &&DistanceMap[i,j]!=0&&DistanceMap[i,j]>lowrange)
						{
							n=i;
							m=j;
							shortestdis=DistanceMap[i,j];
						}
					}
				}

			if(n+m>0) /** if such point exists */
			{
				StCurve cr= new StCurve();
				cr.AddPoint((Point2D)this[n]);
				cr.AddPoint((Point2D)this[m]);
				return cr;
			}
			else
				return null;
			
		}
 
		public bool InvalidPoint(Point2D myPoint)
		{   
			int i=0 ;
			foreach(Point2D pt in this)
			{
				if(pt.X == myPoint.X && pt.Y == myPoint.Y)
				{	pt.Valid=1;
					i++;}
			}
			if (i==0 ) 
			return false;
			else
			return true;
		}

		public bool ValidPoint(Point2D myPoint)
		{   
			int i=0 ;
			foreach(Point2D pt in this)
			{
				if(pt.X == myPoint.X && pt.Y == myPoint.Y)
				{
					pt.Valid=0;
					i++;}
			}
			if (i==0 ) 
			    return false;
			else
				return true;
		}
		public bool InvalidPoint(int myIndex)
		{	
			if (this.Count>myIndex){
				Point2D pt=(Point2D)this[myIndex];
				pt.Valid=1;
				return true;}
			else
				return false;
		}

		public bool MapDistanceSqr()
		{   int i= this.Count;
			if (i==0) {return false;}
			else
			{   DistanceMap= new int[i,i];
				int j=0,k=0;
				for(j=0;j<i;j++)
					for(k=0;k<i;k++)
					{					
						DistanceMap[j,k]=((Point2D)this[j]).DistanceSqr((Point2D)this[k]);
					}
				return true;}
		}

		public Point2D FindNearestPoint(int myPointindex)
		{
			
			int i=0,j,shortestdis=0;
			for(j=0;j<this.Count;j++)
				if (((Point2D)this[j]).Valid==0)
					if (DistanceMap[myPointindex,j]!=0)
						{
							shortestdis=DistanceMap[myPointindex,j];
							if (shortestdis!=0)
							break;
						}
			if (shortestdis==0) return null;

			for(j=0;j<this.Count;j++)
				if (((Point2D)this[j]).Valid==0)
					if (DistanceMap[myPointindex,j]<=shortestdis &&DistanceMap[myPointindex,j]!=0)
					{
							shortestdis=DistanceMap[myPointindex,j];			
							i=j;
					}
				return (Point2D) this[i];

		}

		public int SearchPoint(Point2D myPoint)
		{
			int i=0,j=0,n=0;
			for(i=0;i<this.Count;i++)
			{
				Point2D pt=(Point2D)this[i];
				if(pt.X == myPoint.X && pt.Y == myPoint.Y)
				{
					j=i;
					n++;
				}
			}
			if (n==0) {return -1;}
			else{return j;}
		}

		public int FindNearestInvalidDistance(Point2D searchpoint, Point2D avoidpoint)
		{
			int i=0,j=0,shortestdis=0;
            int spindex=0,apindex=0;
			spindex=this.SearchPoint(searchpoint);
			apindex=this.SearchPoint(avoidpoint);
			for(j=0;j<this.Count;j++)
				if (((Point2D)this[j]).Valid==1&&j!=apindex)
					if (DistanceMap[spindex,j]!=0)//DistanceMap[myPointindex,j]<= lowrange&&
					{
						shortestdis=DistanceMap[spindex,j];
						if (shortestdis!=0) break;
					}
			if (shortestdis==0) return -1;

			for(j=0;j<this.Count;j++)
				if (((Point2D)this[j]).Valid==1&&j!=apindex)
					if (DistanceMap[spindex,j]<=shortestdis &&DistanceMap[spindex,j]!=0)//&& DistanceMap[myPointindex,j]<= lowrange
					{
						shortestdis=DistanceMap[spindex,j];			
						i=j;
					}
			return ((Point2D) this[i]).DistanceSqr(searchpoint);

		}

		public StCurve FindNearestPointPair()
		{
    		int i=0,j=0,shortestdis=0,n=0,m=0;//,invaliddishd=0,invaliddistl=0;
			do
			{
				shortestdis=0;n=0;m=0;//invaliddishd=0;invaliddistl=0;
				for(i=0;i<this.Count;i++)
				{
					for(j=i+1;j<this.Count;j++)
						if (((Point2D)this[i]).Valid==0 && ((Point2D)this[j]).Valid==0)
							if (DistanceMap[i,j]!=0)//DistanceMap[i,j]<= lowrange&&
								if ((NoPointPairList.IndexOf(((Point2D)this[i]))==-1)&&(NoPointPairList.IndexOf(((Point2D)this[j]))==-1))
								{
									//NoPointPairList
									shortestdis=DistanceMap[i,j];
									if(shortestdis!=0) break;
								}
					if(shortestdis!=0) break;
				}
				if (shortestdis==0)
					return null;
				for(i=0;i<this.Count;i++)
					for(j=0;j<this.Count;j++)
					{			
						if (((Point2D)this[i]).Valid==0 && ((Point2D)this[j]).Valid==0)
						{

							if (DistanceMap[i,j]!=0&&shortestdis==0)//DistanceMap[i,j]<= lowrange&&
//								if ((NoPointPairList.IndexOf(((Point2D)this[i]))==-1)&&(NoPointPairList.IndexOf(((Point2D)this[j]))==-1))
								{									
									shortestdis=DistanceMap[i,j];
									
								}

							if (DistanceMap[i,j]<=shortestdis &&DistanceMap[i,j]!=0)//&& DistanceMap[i,j]<= lowrange
//								if ((NoPointPairList.IndexOf(((Point2D)this[i]))==-1)&&(NoPointPairList.IndexOf(((Point2D)this[j]))==-1))
								{
									n=i;
									m=j;
									shortestdis=DistanceMap[i,j];
								}
						}
					}
				StCurve cr= new StCurve();
//				invaliddishd=FindNearestInvalidDistance((Point2D)this[n],(Point2D)this[m]);	
			    /*if (shortestdis>invaliddishd&&invaliddishd!=-1)
					NoPointPairList.Add((Point2D)this[n]);
				invaliddistl=FindNearestInvalidDistance((Point2D)this[m],(Point2D)this[n]);	
				if (shortestdis>invaliddistl&invaliddistl!=-1)
					NoPointPairList.Add((Point2D)this[m]);
				if ((!(shortestdis>invaliddishd&&invaliddishd!=-1))&&(!(shortestdis>invaliddistl&invaliddistl!=-1)))
				{*/
					cr.AddPoint((Point2D)this[n]);
					cr.AddPoint((Point2D)this[m]);
					return cr;
				//}
			}while(true);
			
		}
	}
}
