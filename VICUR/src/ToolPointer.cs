using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawTools
{
	/// <summary>
	/// Pointer tool
	/// </summary>
	public class ToolPointer : DrawTools.Tool
	{
        private enum SelectionMode
        {
            None,
            NetSelection,   // group selection is active
            Move,           // object(s) are moves
            Size            // object is resized
        }

        private SelectionMode selectMode = SelectionMode.None;

        // Object which is currently resized:
        private DrawObject resizedObject;
        private int resizedObjectHandle;
		
		

        // Keep state about last and current point (used to move and resize objects)
        private Point lastPoint = new Point(0,0);
        private Point startPoint = new Point(0, 0);

		public ToolPointer()
		{
		}

        /// <summary>
        /// Left mouse button is pressed
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseDown(PictureBox sender, MouseEventArgs e)
        {
            selectMode = SelectionMode.None;
            Point point = new Point(e.X, e.Y);

            // Test for resizing (only if control is selected, cursor is on the handle)
            int n = ((CRT.Mainform)sender.Parent).GraphicsList.SelectionCount;

            for ( int i = 0; i < n; i++ )
            {
                DrawObject o = ((CRT.Mainform)sender.Parent).GraphicsList.GetSelectedObject(i);
                int handleNumber = o.HitTest(point);
				
                if ( handleNumber > 0 )
                {
                    selectMode = SelectionMode.Size;

                    // keep resized object in class members
                    resizedObject = o;
                    resizedObjectHandle = handleNumber;

                    // Since we want to resize only one object, unselect all other objects
                    ((CRT.Mainform)sender.Parent).GraphicsList.UnselectAll();
                    o.Selected = true;		            

                    break;
                }
                
				

            }

            // Test for move (cursor is on the object)
            if ( selectMode == SelectionMode.None )
            {
                int n1 = ((CRT.Mainform)sender.Parent).GraphicsList.Count;
                DrawObject o = null;

                for ( int i = 0; i < n1; i++ )
                {
                    if ( ((CRT.Mainform)sender.Parent).GraphicsList[i].HitTest(point) == 0 )
                    {
                        o = ((CRT.Mainform)sender.Parent).GraphicsList[i];
                        break;
                    }
                }

                if ( o != null )
                {
                    selectMode = SelectionMode.Move;

                    // Unselect all if Ctrl is not pressed and clicked object is not selected yet
                    if ( ( Control.ModifierKeys & Keys.Control ) == 0  && !o.Selected )
                        ((CRT.Mainform)sender.Parent).GraphicsList.UnselectAll();

                    // Select clicked object
					o.Selected = true;
                }
            }

            // Net selection
            
			

            lastPoint.X = e.X;
            lastPoint.Y = e.Y;
            startPoint.X = e.X;
            startPoint.Y = e.Y;

            sender.Capture = true;


            

            sender.Refresh();
        }


        /// <summary>
        /// Mouse is moved.
        /// None button is pressed, ot left button is pressed.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseMove(PictureBox sender, MouseEventArgs e)
        {
            Point point = new Point(e.X, e.Y);

            // set cursor when mouse button is not pressed
            if ( e.Button == MouseButtons.None )
            {
                Cursor cursor = null;

                for ( int i = 0; i < ((CRT.Mainform)sender.Parent).GraphicsList.Count; i++ )
                {
                    int n = ((CRT.Mainform)sender.Parent).GraphicsList[i].HitTest(point);

                    if ( n > 0 )
                    {
                        cursor = ((CRT.Mainform)sender.Parent).GraphicsList[i].GetHandleCursor(n);
                        break;
                    }
                }

                if ( cursor == null )
                    cursor = Cursors.Default;

                sender.Cursor = cursor;

                return;
            }

            if ( e.Button != MouseButtons.Left )
                return;

            /// Left button is pressed

            // Find difference between previous and current position
            int dx = e.X - lastPoint.X;
            int dy = e.Y - lastPoint.Y;

            lastPoint.X = e.X;
            lastPoint.Y = e.Y;

            // resize
            if ( selectMode == SelectionMode.Size )
            {
                if ( resizedObject != null )
                {
					for(int i=0;i<((CRT.Mainform)sender.Parent).GraphicsList.Count;i++)
					{
						if(!((CRT.Mainform)sender.Parent).GraphicsList[i].Equals(resizedObject))
						{
												
							for(int j=1;j<=((CRT.Mainform)sender.Parent).GraphicsList[i].HandleCount;j++)
							{
								Point pt = ((CRT.Mainform)sender.Parent).GraphicsList[i].GetHandle(j);
								int distance = (pt.X-point.X)*(pt.X-point.X)+(pt.Y-point.Y)*(pt.Y-point.Y);
								if(distance<=10*10)
								{
									point.X = pt.X;
									point.Y = pt.Y;
									break;
								}
							}

							
						}
					}
                    

                    //last to resize seleted point
					resizedObject.MoveHandleTo(point, resizedObjectHandle);
                    
                    sender.Refresh();
                }
            }
			// move
			if ( selectMode == SelectionMode.Move )
			{
				int n = ((CRT.Mainform)sender.Parent).GraphicsList.SelectionCount;

				for ( int i = 0; i < n; i++ )
				{
					((CRT.Mainform)sender.Parent).GraphicsList.GetSelectedObject(i).Move(dx, dy);
				}
                 sender.Refresh();
			}



        }

        /// <summary>
        /// Right mouse button is released
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseUp(PictureBox sender, MouseEventArgs e)
        {
            

            if ( resizedObject != null )
            {
                // after resizing
                resizedObject.Normalize();
                resizedObject = null;
            }

			
            sender.Capture = false;
            sender.Refresh();
        }

	}
}
