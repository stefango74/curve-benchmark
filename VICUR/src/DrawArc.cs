using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
namespace DrawTools
{
	/// <summary>
	/// Ellipse graphic object
	/// </summary>
	/// 

	public class DrawArc : DrawTools.DrawLine	{
		private int msubno = 1;
		private double mgrade = 1.0;
		private int selectLineno = 0;
		private Pen pen1=new Pen(Color.Blue);
        private ArrayList pointArray; 
        private double arcLength;
		public double Radius;
		public Point Center;
		public double Alpha;
		public double Alpha1;
		public double Alpha3;

		private Rectangle rectangle;
		public DrawArc()
		{
			pointArray = new ArrayList();
			//SetRectangle(0, 0, 1, 1);
			Initialize();
		}

		/*public DrawArc(int x, int y, int width, int height)
		{
			//Rectangle = new Rectangle(x, y, width, height);
			Initialize();
		}*/

		
		public override void Draw(Graphics g)
		{
			g.SmoothingMode = SmoothingMode.AntiAlias;

			Pen pen = new Pen(Color, PenWidth);
			//pts= new Point[pointArray.Count];
            //for(int i=0;i<pointArray.Count;i++) pts[i]=(Point)pointArray[i];
	       // g.DrawClosedCurve(pen,pts);
            //g.DrawArc(pen,DrawRectangle.GetNormalizedRectangle(Rectangle),30,70);
			//g.DrawEllipse(pen, DrawRectangle.GetNormalizedRectangle(Rectangle));
			//DrawSubDivCircle(msubno, mgrade,g,pen);
			if(pointArray.Count>=3)
					Arc(g,pen,(Point)pointArray[0], (Point)pointArray[1], (Point)pointArray[2]);
			pen.Dispose();
		}
		public void AddPoint(Point point)
		{
			
			pointArray.Add(point);

		}
		
		public override int HandleCount
		{
			get
			{
				return pointArray.Count;
			}
		}
		public double ArcLength
		{
			get
			{
				return arcLength;
			}
		}

		public override Point GetHandle(int handleNumber)
		{
			if ( handleNumber < 1 )
				handleNumber = 1;

			if ( handleNumber > pointArray.Count )
				handleNumber = pointArray.Count;

			return ((Point)pointArray[handleNumber - 1]);
		}

		/// <summary>
		/// Hit test.
		/// Return value: -1 - no hit
		///                0 - hit anywhere
		///                > 1 - handle number
		/// </summary>
		/// <param name="point"></param>
		/// <returns></returns>
		public override int HitTest(Point point)
		{
			if ( Selected )
			{
				for ( int i = 1; i <= HandleCount; i++ )
				{
					if ( GetHandleRectangle(i).Contains(point) )
						return i;
				}
			}

			if ( PointInObject(point) )
				return 0;

			return -1;
		}

		protected override bool PointInObject(Point point)
		{
			
            setRect((Point)pointArray[0], (Point)pointArray[1], (Point)pointArray[2]);
			return rectangle.Contains(point);
			
		}

		public override void MoveHandleTo(Point point, int handleNumber)
		{
			if ( handleNumber < 1 )
				handleNumber = 1;

			if ( handleNumber > pointArray.Count)
				handleNumber = pointArray.Count;

			pointArray[handleNumber-1] = point;

			            
			Invalidate();
		}

		public override void Move(int deltaX, int deltaY)
		{
			int n = pointArray.Count;
			Point point;

			for ( int i = 0; i < n; i++ )
			{
				point = new Point( ((Point)pointArray[i]).X + deltaX, ((Point)pointArray[i]).Y + deltaY);

				pointArray[i] = point;
			}

			Invalidate();
		}
       

    
		public override void SelcetSubline()
		{
				
			msubno=Msubno;
			mgrade=Mgrade;
		}

		public override int GetSubdivno()
		{
			
			return msubno;
		}

		public override double GetGrading()
		{
			return mgrade;
		}

		public override void SetHighlight(int i,Pen pen)
		{
			selectLineno = i;
			pen1 = pen;
		}
	
		public int SolveSimEqn(double a11, double a12, double b1,
			double a21, double a22, double b2,
			ref double  x1, ref double x2)
	   {
		double det = a11*a22-a12*a21;
		if (det!=0)
	    {
		 x1 = (b1*a22-b2*a12)/det;
		 x2 = (b2*a11-b1*a21)/det;
		 return 1;
	    }
	    else
	     return 0;
       }

		public double Dist(Point P1, Point P2)
	    {
		 long x,y;
		 x = P1.X-P2.X;
		 y = P1.Y-P2.Y;
		return Math.Sqrt(x*x+y*y);
	    }
		public void setRect(Point p1, Point p2, Point p3)
		{
			int x1 = p1.X;
			int x2 = p2.X;
			int x3 = p3.X;
			int y1 = p1.Y;
			int y2 = p2.Y;
			int y3 = p3.Y;
			int x12 = x1*x1;
			int y12 = y1*y1;
			double x=0,y=0;
			SolveSimEqn(2*(x1-x2),2*(y1-y2),x12-x2*x2+y12-y2*y2,2*(x1-x3),2*(y1-y3),x12-x3*x3+y12-y3*y3,ref x,ref y);
			Point centre = new Point((int)x,(int)y);
			double alpha1 = AngleBetween(p1,centre,p3);
			Point d = new Point(centre.X-p1.X,centre.Y-p1.Y);
			int radius = (int)Math.Sqrt(d.X*d.X+d.Y*d.Y);
			rectangle = new Rectangle(centre.X-radius,centre.Y-radius,2*radius,2*radius);

		}

	public void Arc(Graphics g,Pen pen,Point p1, Point p2, Point p3)
	    {
		 if ( Dist(p1,p2)<0.1 || Dist(p2,p3)<0.1 || Dist(p1,p3)<0.1) 
	     {
		   g.DrawLine(pen,p1,p3);
		   return;
	     }
	     //if( Colinear(p1,p2,p3)) 
         //{
	     // Line(dc,p1,p3);
	     // return;
         //}
	int x1 = p1.X;
	int x2 = p2.X;
	int x3 = p3.X;
	int y1 = p1.Y;
	int y2 = p2.Y;
	int y3 = p3.Y;
	int x12 = x1*x1;
	int y12 = y1*y1;
	double x=0,y=0;
	SolveSimEqn(2*(x1-x2),2*(y1-y2),x12-x2*x2+y12-y2*y2,2*(x1-x3),2*(y1-y3),x12-x3*x3+y12-y3*y3,ref x,ref y);
	Point centre = new Point((int)x,(int)y);Center = centre;
	double alpha1 = AngleBetween(p1,centre,p3);
	Point d = new Point(centre.X-p1.X,centre.Y-p1.Y);
	int radius = (int)Math.Sqrt(d.X*d.X+d.Y*d.Y);Radius = Math.Sqrt(d.X*d.X+d.Y*d.Y);

	double alpha2 = AngleBetween(p1,centre,p2);
	double alpha;
	if ( alpha1>0) 
      {    
	if (alpha2>0) 
       {
	if (alpha2<alpha1)
	alpha = alpha1;
	else
	//alpha = -TWOPI+alpha1;
      alpha = -2*Math.PI+alpha1;
     }
	else
	alpha = -2*Math.PI+alpha1;
     }
	else 
     {
	if (alpha2<0) 
     {
	if (alpha2>alpha1)
	  alpha = alpha1;
	else
	  alpha = 2*Math.PI+alpha1;
     }
	else
	  alpha = 2*Math.PI+alpha1;
     }
       
        Alpha = alpha;//p1---p3 angle;
		Alpha1 = AngleWithXAxis(p1,centre);//p1,center with XAxis
        Alpha3 = AngleWithXAxis(p3,centre);//p3,center with XAxis

		if (alpha>0)
		{
			g.DrawArc(pen,centre.X-radius,centre.Y-radius,2*radius,2*radius,(int)(180*AngleWithXAxis(p1,centre)/Math.PI),(int)(180*alpha/Math.PI));
			arcLength = radius*alpha;
		}
			//dc->Arc(centre.x-radius,centre.y+radius,centre.x+radius,centre.y-radius,p1.x,p1.y,p3.x,p3.y);
		else
		{
			//dc->Arc(centre.x-radius,centre.y+radius,centre.x+radius,centre.y-radius,p3.x,p3.y,p1.x,p1.y);
			g.DrawArc(pen,centre.X-radius,centre.Y-radius,2*radius,2*radius,(int)(180*AngleWithXAxis(p3,centre)/Math.PI),-(int)(180*alpha/Math.PI));
            arcLength = radius*Math.Abs(alpha);
		}
  
    
	}
	public	double AngleBetween(Point p0, Point p1,	Point p2)
	{
		//TPoint2d v1 = p0-p1;
		//TPoint2d v2 = p2-p1;
		Point newPt;
		newPt=Rotate(-AngleWithXAxis(p0,p1),p2,p1);
		//v2.Rotate(-v1.AngleWithXAxis());
		return AngleWithXAxis(newPt,p1);
	}
	public Point Rotate(double theta,Point p1,Point p2)
    {
		double m_X = (double)(p1.X-p2.X);
		double m_Y = (double)(p1.Y-p2.Y);
		double c = Math.Cos(theta);
		double s = Math.Sin(theta);
		double temp = c*m_X - s*m_Y;
		m_Y = s*m_X + c*m_Y;
		m_X = temp;

		return new Point((int)(m_X+p2.X),(int)(m_Y+p2.Y));
    }

	public double AngleWithXAxis(Point p1,Point p2) 
    {
		double m_X = (double)(p1.X-p2.X);
		double m_Y = (double)(p1.Y-p2.Y);
		if (m_X>0)
		  return Math.Atan(m_Y/m_X);
		else 
		{
			if (m_X<0)
			{
				if (m_Y>=0)
				return Math.PI + Math.Atan(m_Y/m_X);
				else
				return Math.Atan(m_Y/m_X) - Math.PI;
			}
			else
			{
				if (m_Y>0)
				return 0.5*Math.PI;
				else
				{
					if (m_Y<0)
					return -0.5*Math.PI;
					else
					return 0;
				}
			}
	     }
      }

	//Add...

	}
}
