using System;
using System.Windows.Forms;
using System.Drawing;

namespace DrawTools
{
	/// <summary>
	/// Ellipse graphic object
	/// </summary>
	public class DrawPoint : DrawTools.DrawRectangle
	{
		public DrawPoint()
		{
			SetRectangle(0, 0, 1, 1);
			Initialize();
		}

		public DrawPoint(int x, int y, int width, int height)
		{
			Rectangle = new Rectangle(x, y, width, height);
			Initialize();
		}

		public override void Draw(Graphics g)
		{
			Pen pen = new Pen(Color, PenWidth);

			g.FillEllipse(pen.Brush, DrawRectangle.GetNormalizedRectangle(Rectangle));
            
			pen.Dispose();
		}


	}
}
