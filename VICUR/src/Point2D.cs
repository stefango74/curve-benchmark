using System;
using System.Drawing;

namespace Vision
{
	/// <summary>
	/// Summary description for Point2D.
	/// </summary>
	/**
	 * Describe the characteristics of a point in 2D/3D
	 * - coordinates (x,y)/(x,y,z)
	 * - point operations: 
	 *		* distance between two points
	 *		* addition
	 *		* equality
	 *		* online????
	 */ 
	
	[Serializable]
	public class Point2D
	{
		private int x;
		private int y;
		private int z;
		private int curveid;
		private bool d3d; //3D points,alpha

		//An: angle at point
		private double angle=-1;

		public double Angle
		{
			get{return this.angle;}
			set{this.angle = value;}
		}
				
		public System.Collections.ArrayList potentialConnectedPoint = new System.Collections.ArrayList();

		//public int potentialConnected = 0; //for free point that is already considered twice with the curve endpoint
		//public ArrayList  potentialConnectedPoint = new ArrayList(); //for free point that is already considered twice with the curve endpoint

		//An
	//	public Point2D curveEndPoint=null;
		public System.Collections.ArrayList edgeEndPoint = new System.Collections.ArrayList();
			
		public int X
		{
			get{return this.x;}
			set{this.x = value;}
		}

		public int Y
		{
			get{return this.y;}
			set{this.y = value;}
		}
		public int Z
		{
			get{return this.z;}
			set{this.z = value;}
		}	
		public int CurveID
		{
			get{return this.curveid;}
			set{this.curveid = value;}
		}	

		public bool D3D
		{
			get{return this.d3d;}
			set{this.d3d = value;}
		}	
		public Point2D(int x, int y)
		{			
			this.x = x;
			this.y = y;
			this.Z = 0;
			this.D3D=false;
			this.curveid=0;
		}
		public Point2D(int x, int y,int z)
		{			
			this.x = x;
			this.y = y;
			this.z = z;
			this.D3D=true;
			this.curveid=0;
		}
		
		public static Point2D operator+ (Point2D one, Point2D two)
		{
			if (!one.d3d)
				return new Point2D(one.x + two.x, one.y + two.y);
			else
			{
				Point2D pt= new Point2D(one.x + two.x, one.y + two.y,one.z+two.z);
				pt.D3D=true;
				return pt;
			}
		}

		public static Point2D operator- (Point2D one, Point2D two)
		{
			if (!one.d3d)
				return new Point2D(one.x - two.x, one.y - two.y);
			else
			{
				Point2D pt= new Point2D(one.x - two.x, one.y - two.y,one.z-two.z);
				pt.D3D=true;
				return pt;
			}
		}

		// now is using this operator overload solution
		public void Plus(Point2D other)
		{
			this.x += other.x;
			this.y += other.y;
			if(d3d)
				this.z += other.z;
		}
		
		public bool Equals(Point2D other)
		{
			if(d3d)
			{
				if( ( this.x == other.x ) && (this.y == other.y) &&(this.z==other.z))	
					return true;
				else 
					return false;
			}
			else
			{
				if( ( this.x == other.x ) && (this.y == other.y) )	
				return true;
				else 
				return false;
			}
		}

		public double Distance(Point2D other)
		{
			if( (this.Equals(other)) )
			{
				return 0;
			}
			else
			{
				if (!d3d)
				return Math.Sqrt( Math.Pow(this.x - other.x, 2) + Math.Pow(this.y - other.y, 2) );
				else
				return Math.Sqrt( Math.Pow(this.x - other.x, 2) + Math.Pow(this.y - other.y, 2) +Math.Pow(this.z-other.z,2));
			}
		}
		
		/**
		 * ?????
		 */ 
		public bool OnLine(Point2D ptone, Point2D pttwo)
		{
			if( 1.03 * this.Distance(ptone) >= ( this.Distance(pttwo) + pttwo.Distance(ptone) ) )
				return true;
			else 
				return false;
		}

		public int DistanceSqr(Point2D myPoint)
		{
			if( (this.Equals(myPoint)) )
			{
				return 0;
			}
			else
			{
				if (!d3d)
				return  (this.x - myPoint.x)*(this.x - myPoint.x)+ (this.y - myPoint.y)*(this.y - myPoint.y) ;
				else
				return  (this.x - myPoint.x)*(this.x - myPoint.x)+ (this.y - myPoint.y)*(this.y - myPoint.y) +(this.z - myPoint.z)*(this.z - myPoint.z);
			}
		}

		public int Valid=0;
	}
}
