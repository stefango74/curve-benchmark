using System;

	/// <summary>
	/// //implement sampling methods
	/// </summary>
	public class Sampling
	{
		
		internal int distancesamplingstep;
		internal int scanningsamplingstep;
		internal int randomincreaserate;
		
		internal bool samplebytime;
		internal bool samplebydistance;
		internal bool samplebyscanning;
		internal Vertex vert;	
		internal int lastX;
		internal int lastY;
		public Sampling(Vertex Vert)
		{
			// 
			// TODO: Add constructor logic here
			//
			vert = Vert;
			lastX=0;
			lastY=0;

		}
		virtual public int DistanceSamplingStep
		{
			set
			{
				if (value>100)
					value=100;
				if (value<1)
					value=1;
				distancesamplingstep = value*value;
				Refresh();
			}
		
		}
		virtual public int ScanningSamplingStep
		{
			set
			{
				if (value>100)
					value=100;
				if (value<1)
					value=1;
				scanningsamplingstep = value;
				Refresh();
			}
		
		}
		virtual public int RandomIncreaseRate
		{
			set
			{
				if (value>100)
					value=100;
				if (value<0)
					value=0;
				randomincreaserate = value;
				Refresh();
			}
		
		}
		virtual public bool SampleByTime
		{
			set
			{
				samplebytime=value;
				Refresh();
			}
		
		}
		virtual public bool SampleByDistance
		{
			set
			{
				samplebydistance=value;
				Refresh();
			}
		
		}
		virtual public bool SampleByScanning
		{
			set
			{
				samplebyscanning=value;
				Refresh();
			}
			get
			{
				return samplebyscanning;
			}
		}


		public void SetVertex( Vertex Vert)
		{

				vert = Vert;
				Refresh();

		}
		internal virtual int findPt(int i, int j)
		{
			for (int k = 0; k < vert.size(); k++)
				if (System.Math.Abs(i - vert.get_Renamed(k).X) <= 7 && System.Math.Abs(j - vert.get_Renamed(k).Y) <= 7)
					return k;
		
			return - 1;
		}
		public void Refresh()
		{
		
		}
		public bool DoSample(int x,int y)
		{
		int i,j;
		i=findPt(x,y);
			
		if (samplebytime)
		{
				if (i==-1)
				{
					vert.add(x,y);
					return true;					
				}
		}
		if (samplebydistance)
		{
			j=(x-lastX)*(x-lastX)+(y-lastY)*(y-lastY);
			if (j>distancesamplingstep)
			{
				if (i==-1)
				{
					vert.add(x,y);
					lastX=x;
					lastY=y;
					return true;					
				}
			}

		}
		return false;
	
		}

		public void DoScan(System.Drawing.Bitmap g)
		{
		int i,j;
		System.Drawing.Color cl;
			for(i=5;i<1200;i=i+scanningsamplingstep)
				for(j=5;j<1000;j=j+scanningsamplingstep)
				{
					cl=g.GetPixel(i,j);
					
					if ((cl.R==System.Drawing.Color.Black.R)&&(cl.G==System.Drawing.Color.Black.G)&&(cl.B==System.Drawing.Color.Black.B))
						if (findPt(i,j)==-1)
						vert.add(i,j);
				}
		}



	}

