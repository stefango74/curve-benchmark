using System;
using System.Windows.Forms;
using System.Drawing;

namespace DrawTools
{
	/// <summary>
	/// Ellipse graphic object
	/// </summary>
	/// 

	public class DrawEllipse : DrawTools.DrawRectangle
	{
		private int msubno = 1;
		private double mgrade = 1.0;
		private int selectLineno = 0;
        private Pen pen1=new Pen(Color.Blue);

		public DrawEllipse()
		{
            SetRectangle(0, 0, 1, 1);
            Initialize();
		}

        public DrawEllipse(int x, int y, int width, int height)
        {
            Rectangle = new Rectangle(x, y, width, height);
            Initialize();
        }

        public override void Draw(Graphics g)
        {
            
			Pen pen = new Pen(Color, PenWidth);
            
            g.DrawEllipse(pen, DrawRectangle.GetNormalizedRectangle(Rectangle));
            pen.Dispose();
        }
      
     public override void SelcetSubline()
		{
				
			msubno=Msubno;
			mgrade=Mgrade;
		}

		public override int GetSubdivno()
		{
			
			return msubno;
		}

		public override double GetGrading()
		{
			return mgrade;
		}

		public override void SetHighlight(int i,Pen pen)
		{
			selectLineno = i;
			pen1 = pen;
		}

	}
}
