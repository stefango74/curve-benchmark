using System;
using System.Collections;
namespace Vision
{
	/// <summary>
	/// 
	/// </summary>
	public class StCurveList : System.Collections.ArrayList
	{
		public StCurveList()
		{
			// 
			// TODO: Add constructor logic here
			//
		}

		public bool AddCurve(StCurve newcurve)
		{
			this.Add(newcurve);
			return true;
		}

		public bool CurveReconstruction(StPointList mypointlist,bool scf)
		{
			mypointlist.MapDistanceSqr();
			Optimismfilter op=new Optimismfilter();
			bool intersec=false;
			double dis;
			int lowrange;
			int j=0,k=0,g=0,headtaillengthsqr=0,eh=0,et=0,todo=0;//,avoiddis;
			do
			{
				todo=0;	
				lowrange=0; /**< minimum distance value to be considered */
				StCurve cr;
				do
				{
					cr= mypointlist.FindNearestPointPair(lowrange);//to be continued
					if (cr!=null)
					{
						Point2D c=cr.ReadHead();
						Point2D d=cr.ReadTail();
						if(this.CheckInterSection(c,d))
						{
							lowrange=c.DistanceSqr(d);
							todo=0;
						}
						else
						{
							todo=1;
						}
					}
					else
						todo=1;
				} while (todo==0);

				todo=0;
				
				
				if (cr!=null)
					do 
					{
						//find the point near head
						Point2D hd=cr.ReadHead();
						Point2D tl=cr.ReadTail();
						headtaillengthsqr=cr.ReadHeadToTailDistanceSqr();
						mypointlist.InvalidPoint(hd);
						mypointlist.InvalidPoint(tl);
						j=mypointlist.SearchPoint(hd);
						//eh=cr.ExternalLength(0);
						Point2D pth=mypointlist.FindNearestPoint(j);
						if (pth==null) break;
						k=hd.DistanceSqr(pth);
						dis=pth.Distance(hd);
						eh=cr.ExternalLengthEx(0,dis);
						eh=eh*eh;
						
				
						//find the point near tail										
						j=mypointlist.SearchPoint(tl);

						//et=cr.ExternalLength(-1);
						Point2D ptt=mypointlist.FindNearestPoint(j);
						dis=ptt.Distance(tl);
						et=cr.ExternalLengthEx(-1,dis);
						et=et*et;
						g=tl.DistanceSqr(ptt);
						
						if (k<=g)
						{
							//avoiddis=mypointlist.FindNearestInvalidDistance(hd,(Point2D)cr[1]);
							if (k<=headtaillengthsqr&&k<=eh)
							{
								if(!(this.CheckInterSection(pth,hd)||cr.CheckInterSection(pth,hd)))
									cr.PushPoint(pth);	
								else
									todo=1;
							}
							else 
								todo=1;

						}
						else if (k>=g)
						{
							//avoiddis=mypointlist.FindNearestInvalidDistance(tl,(Point2D)cr[cr.Count-2]);
							//if ((avoiddis!=-1&&g<=headtaillengthsqr&&g<=et&&g<=avoiddis)||(avoiddis==-1&&g<=et))
							//2005.7.28
							if (g<=headtaillengthsqr&&g<=et)
							{								
								if(!(this.CheckInterSection(ptt,tl)||cr.CheckInterSection(ptt,tl)))
									cr.AddPoint(ptt);	
								else
									todo=1;
							}
							else
								todo=1;
						}
						else if(k>headtaillengthsqr &&g>headtaillengthsqr)
						{
							if (headtaillengthsqr<=eh||headtaillengthsqr<=et)
							{
								cr.Closed=1;
								
							}
							todo=1;
						}
						else
							todo=1;
						
					} while (todo==0);
				else break;
				this.AddCurve(cr);
				
			} while (true);
			int i;
			
			////////////////////Sharp Corner Filter//////////////////////////////////
			if (scf)
				for(i=0;i<this.Count;i++)
				{
					ArrayList al=new ArrayList();
					al=op.SharpCornerFilter( (ArrayList)this[i],true);
					StCurve sc=new StCurve();
					for(j=0;j<al.Count;j++)
					{
						sc.Add((Point2D)al[j]);
					}
					this[i]=sc;
				}
			this.ConnectCurve();

/****  same as above
			if (scf)
				for(i=0;i<this.Count;i++)
				{
					ArrayList al=new ArrayList();
					al=op.SharpCornerFilter( (ArrayList)this[i],true);
					StCurve sc=new StCurve();
					for(j=0;j<al.Count;j++)
					{
						sc.Add((Point2D)al[j]);
					}
					this[i]=sc;
				}
*/
			StCurve curveA,curveB;

	/*
			for(i=0;i<this.Count;i++)
			{
				curveA=(StCurve)this[i];
				//if (op.SelfintersectionFilter(ref curveA))
				//	j++;
				
			}
	*/		

			for(i=0;i<this.Count;i++)
				for(j=i;j<this.Count;j++)
				{
					curveA=(StCurve)this[i];
					curveB=(StCurve)this[j];
					if(op.CurveConnector( ref curveA,ref curveB))
					{
						if (((StCurve)this[i]).Count==0)
						{
							this.RemoveAt(i);
							i--;
						}
						if (((StCurve)this[j]).Count==0)
						{
							this.RemoveAt(j);
							j--;
						}

					}
				}

			return true;
		}

		public bool ConnectCurve()
		{
			int i,j,k;
			StCurve clOne,clTwo;
			bool connect=false;
			for(i=0;i<this.Count-1;i++)
				for(j=i+1;j<this.Count;j++)
				{
					clOne=(StCurve)this[i];
					clTwo=(StCurve)this[j];
					Point2D ptA,ptB,ptC,ptD;
					ptA=(Point2D)clOne[0];
					ptB=(Point2D)clOne[clOne.Count-1];
					ptC=(Point2D)clTwo[0];
					ptD=(Point2D)clTwo[clTwo.Count-1];
					
					int p2pdis;
					
					double exedge,p2pedge,loA,loB;
					ArrayList segA=new ArrayList();
					ArrayList segB=new ArrayList();
					int ii,jj;
					
					// **Calculate distance between point counts **/
					for(ii=0;ii<clOne.Count-1;ii++)
					{
						jj=(((Point2D)clOne[ii]).Y-((Point2D)clOne[ii+1]).Y)*(((Point2D)clOne[ii]).Y-((Point2D)clOne[ii+1]).Y)
							+(((Point2D)clOne[ii]).X-((Point2D)clOne[ii+1]).X)*(((Point2D)clOne[ii]).X-((Point2D)clOne[ii+1]).X);	
						segA.Add(Math.Sqrt((double)jj));
					}

					// **Calculate distance between point counts **/
					for(ii=0;ii<clTwo.Count-1;ii++)
					{
						jj=(((Point2D)clTwo[ii]).Y-((Point2D)clTwo[ii+1]).Y)*(((Point2D)clTwo[ii]).Y-((Point2D)clTwo[ii+1]).Y)
							+(((Point2D)clTwo[ii]).X-((Point2D)clTwo[ii+1]).X)*(((Point2D)clTwo[ii]).X-((Point2D)clTwo[ii+1]).X);	
						segB.Add(Math.Sqrt((double)jj));
					}

						
					Optimismfilter op=new Optimismfilter();
			
					
					p2pdis=ptA.DistanceSqr(ptC);
					p2pedge=ptA.Distance(ptC);
					loA=0;
					loB=0;

					if (segA.Count!=0)
						loA=(double)segA[0];
					if(segB.Count!=0)
						loB=(double)segB[0];
					if (ptA.DistanceSqr(ptD)<p2pdis)
					{
						p2pedge=ptA.Distance(ptD);
						p2pdis=ptA.DistanceSqr(ptD);
						if(segB.Count!=0)
							loB=(double)segB[segB.Count-1];
					}
					if (ptB.DistanceSqr(ptC)<p2pdis)
					{
						p2pedge=ptB.Distance(ptC);
						p2pdis=ptB.DistanceSqr(ptC);
						if (segA.Count!=0)
							loA=(double)segA[segA.Count-1];
					}
					if (ptB.DistanceSqr(ptD)<p2pdis)
					{
						p2pedge=ptB.Distance(ptD);
						
						if (segA.Count!=0)
							loA=(double)segA[segA.Count-1];
						if(segB.Count!=0)
							loB=(double)segB[segB.Count-1];
					}
					bool extendableA=false,extendableB=false;
					if (segA.Count!=0)
					{
						exedge=op.ExternalLength(segA,p2pedge,loA);
						if (exedge>=p2pedge)
							extendableA=true;
					}
					if(segB.Count!=0)
					{
						exedge=op.ExternalLength(segB,p2pedge,loB);
						if (exedge>=p2pedge)
							extendableB=true;
					}
					
					// Test Three
					if( extendableA||extendableB) 
					{
						// Connect the two curves
						this.Remove(clOne);
						this.Remove(clTwo);
				
						StCurve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

						this.Add(clNew);
						connect=true;			
						break;
						
					}					
				
				}
			if(connect)
				this.ConnectCurve();
			return true;
		}

		public bool AbsorbIsolatePoints(ArrayList pl)
		{
			int i=0,j,indx=0;
			Point2D np;
			int dis=-1,distemp=0;
			for(i=0;i<pl.Count;i++)
			{
				for(j=0;j<this.Count;j++)
				{
					np=((StCurve)this[j]).FindNearestPoint((Point2D)pl[i]);
					distemp=np.DistanceSqr((Point2D)pl[i]);
					if (dis==-1) dis= distemp;
					if(dis<=distemp)
					{
						indx=j;
						dis=distemp;
					}

				}
				((StCurve)this[j]).AbSorbPoint((Point2D)pl[i]);
			}
			return true;
		}
		public bool TestInterSection(StCurve testcurve, StCurve othercurve)
		{
			return true;
		}

		public bool CheckInterSection(Point2D a,Point2D b)
		{
			
			Optimismfilter op=new Optimismfilter();
			int i,j;
			Point2D c;
			Point2D d;
			bool cr;
			cr=false;
			for(i=0;i<this.Count;i++)
			{
				StCurve sc=(StCurve)this[i];
				if (sc.Count>=2)
					for(j=1;j<sc.Count;j++)
					{
						c=(Point2D)sc[j-1];
						d=(Point2D)sc[j];
						if(!(c.Equals(b)||d.Equals(b)))
						{
							cr=op.CheckCross(c,d,a,b);
							if (cr)
								return cr;
						}
					}
			}
			return cr;
		}
		public StCurve ReadCurve(int CurveID)
		{
		StCurve cr;
		for(int i=0;i<this.Count;i++)
			{
				cr= (StCurve)this[i];
				if (cr.curveID==CurveID)
				return cr;
			}
			return null;
		}
		//An
		// connect 2 curves
		public StCurve ConnetTwoCurves2(ref StCurve clOne, ref StCurve clTwo, Point2D clOnePoint,Point2D clTwoPoint )
		{		
			//if clOne is first point in the curve 1
			if (clOne.IndexOf(clOnePoint, 0, clOne.Count)==0)
			{
				if (clTwo.IndexOf(clTwoPoint, 0, clTwo.Count)==0)
				{
					foreach(Point2D pt in clTwo)
					{
						clOne.Insert(0,pt);
					}
					return clOne;		
				}
				else
				{
					foreach(Point2D pt in clOne)
					{
						clTwo.Add(pt);
					}
					return clTwo;
				}


			}
			else
			{
				if (clTwo.IndexOf(clTwoPoint, 0, clTwo.Count)==0)
				{
					foreach(Point2D pt in clTwo)
					{
						clOne.Add(pt);
					}
					return clOne;	
				}
				else
				{
					for(int i=clTwo.Count-1; i > -1; i--)
					{
						Point2D pt = (Point2D)clTwo[i];
						clOne.Add(pt);
					}
					return clOne;
				}

			}
			
		}		

		public StCurve ConnetTwoCurves(ref StCurve clOne, ref StCurve clTwo)
		{
			int relationship = 0;

			relationship = clOne.CurveRelationship(clTwo);

			if(relationship == 1)
			{
				foreach(Point2D pt in clTwo)
				{
					clOne.Insert(0, pt);
				}
				return clOne;				
			}

			else if(relationship == 2)
			{
				foreach(Point2D pt in clOne)
				{
					clTwo.Add(pt);
				}
				return clTwo;
			}
			
			else if(relationship == 3)
			{
				foreach(Point2D pt in clTwo)
				{
					clOne.Add(pt);
				}
				return clOne;
			}

			else 
			{
				for(int i=clTwo.Count-1; i > -1; i--)
				{
					Point2D pt = (Point2D)clTwo[i];
					clOne.Add(pt);
				}
				return clOne;
			}			
		}		
	}
}
