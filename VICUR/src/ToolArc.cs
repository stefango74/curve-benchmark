using System;
using System.Windows.Forms;
using System.Drawing;
namespace DrawTools
{
	/// <summary>
	/// Ellipse tool
	/// </summary>
	public class ToolArc : DrawTools.ToolLine
	{
		private DrawArc newArc;
		private bool drawTwoPt=false,firstDraw=true;
		public ToolArc()
		{
			
		}

		public  override void OnMouseDown(PictureBox sender, MouseEventArgs e)
		{
			//AddNewObject(drawArea, new DrawEllipse(e.X, e.Y, 50, 50));
			if(firstDraw)
			{
				newArc = new DrawArc();
				
				
                newArc.AddPoint(new Point(e.X,e.Y));
				newArc.AddPoint(new Point(e.X+20,e.Y-10));
				newArc.AddPoint(new Point(e.X+40,e.Y));
                AddNewObject(sender, newArc);
				((CRT.Mainform)sender.Parent).GraphicsList[0].Objtype="Arc";
                drawTwoPt=true;
			}
			/*if(!drawTwoPt&!firstDraw)	{
				newArc.AddPoint(new Point(e.X,e.Y));
				newArc.AddPoint(new Point(e.X,e.Y));
				drawTwoPt=true;
									}*/
			firstDraw=false;

		}
		public override void OnMouseMove(PictureBox sender, MouseEventArgs e)
		{
			sender.Cursor = Cursor;

			//   if ( e.Button != MouseButtons.Left )
			//      return;

			if ( newArc == null )
				return;                 // precaution
            if(drawTwoPt){
			Point point = new Point(e.X, e.Y);
			
			
			
			newArc.MoveHandleTo(point,3);
			
			
             
				sender.Refresh();
			}

		}

		public  override void OnMouseUp(PictureBox sender, MouseEventArgs e)
		{
			// newPolygon = null;
			// base.OnMouseUp (drawArea, e);
			//if ( e.Button == MouseButtons.Right )
			//{  
				
				//drawArea.Refresh();
				firstDraw=true;
				newArc = null;
				base.OnMouseUp (sender, e);//}
			
		}
	}
}