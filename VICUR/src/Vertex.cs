// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   NNCrust.java
using System;
	[Serializable]
public class Vertex
{
	
	public Vertex()
	{
		///array list contains Drawing points structure, maximum 10
		v = new System.Collections.ArrayList(10);
		///array list contains Hashtable structure. maximum 10 
		e = new System.Collections.ArrayList(10);
	}
	
	public virtual void  add(int i, int j)
	{
		int m,n;
		n=0;

		///
		for(m=0;m<v.Count;m++)
		{
			if((((System.Drawing.Point)v[m]).X==i)&&(((System.Drawing.Point)v[m]).Y==j))
				n++;			
		}

		if(n==0)
		{
            
			v.Add(new System.Drawing.Point(i, j));
			e.Add(new System.Collections.Hashtable(2));
		}
	}
	
	public virtual void  addNeighbor(int i, int j, double d)
	{
		SupportClass.PutElement(((System.Collections.Hashtable) e[i]), j, d);
		SupportClass.PutElement(((System.Collections.Hashtable) e[j]), i, d);
	}
	
	public virtual void  cleanUp()
	{
		for (int i = 0; i < v.Count; i++)
		{
			System.Int32 integer;
			for (System.Collections.Hashtable hashtable = (System.Collections.Hashtable) e[i]; hashtable.Count > 2; SupportClass.HashtableRemove(((System.Collections.Hashtable) e[integer]), i))
			{
				System.Collections.IEnumerator enumeration = hashtable.Keys.GetEnumerator();
				//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				enumeration.MoveNext();
				integer = (System.Int32) enumeration.Current;
				double d = ((System.Double) hashtable[integer]);
				//UPGRADE_TODO: Method 'java.util.Enumeration.hasMoreElements' was converted to 'System.Collections.IEnumerator.MoveNext' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				while (enumeration.MoveNext())
				{
					//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					System.Int32 integer1 = (System.Int32) enumeration.Current;
					double d1 = ((System.Double) hashtable[integer1]);
					if (d1 > d)
					{
						d = d1;
						integer = integer1;
					}
				}
				SupportClass.HashtableRemove(hashtable, integer);
			}
		}
	}
	
	public virtual void  clear()
	{
		e = new System.Collections.ArrayList(10);
		v = new System.Collections.ArrayList(10);
	}
	
	public virtual System.Drawing.Point get_Renamed(int i)
	{
		return (System.Drawing.Point) v[i];
	}
	
	public virtual System.Collections.IEnumerator listNeighbors(int i)
	{
		return ((System.Collections.Hashtable) e[i]).Keys.GetEnumerator();
	}
	
	public virtual int neighbors(int i)
	{
		return ((System.Collections.Hashtable) e[i]).Count;
	}
	
	public virtual void  newNeighbor()
	{
		e = new System.Collections.ArrayList(10);
		for (int i = 0; i < v.Count; i++)
			e.Add(new System.Collections.Hashtable(2));
	}
	
	public virtual void  remove(int i)
	{
		v.RemoveAt(i);
		e.RemoveAt(i);
	}
	
	public virtual void  set_Renamed(int i, int j, int k)
	{
		v[i] = new System.Drawing.Point(j, k);
	}
	
	public virtual int size()
	{
		return v.Count;
	}
	
	internal System.Collections.ArrayList v;
	internal System.Collections.ArrayList e;
}