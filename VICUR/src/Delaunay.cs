// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Delaunay.java
using System;

public class Delaunay
{
	
	public Delaunay(int i)
	{
		tris = new System.Collections.ArrayList(i);
		nodes = new System.Collections.ArrayList(3 * i);
		edges = new System.Collections.ArrayList(3 * i);

		removepoints = new System.Collections.ArrayList(3 * i);
	}
	
	public virtual void  Clear()
	{
		System.Collections.ArrayList temp_arraylist;
		temp_arraylist = nodes;
		temp_arraylist.RemoveRange(0, temp_arraylist.Count);
		System.Collections.ArrayList temp_arraylist2;
		temp_arraylist2 = edges;
		temp_arraylist2.RemoveRange(0, temp_arraylist2.Count);
		System.Collections.ArrayList temp_arraylist3;
		temp_arraylist3 = tris;
		temp_arraylist3.RemoveRange(0, temp_arraylist3.Count);
	}
	
	public virtual void  Insert(int i, int j, int k)
	{
		//i = x-coordinate, j  = y-coordinate, k = z-coordinate
		Node node = new Node(i, j, k);
		
		nodes.Add(node); //add all vertices into node array
		if (nodes.Count < 3)
			return ;

		if (nodes.Count == 3)
		{
			Node node1 = (Node) nodes[0];
			Node node2 = (Node) nodes[1];
			Node node3 = (Node) nodes[2];

			Edge edge = new Edge(node1, node2);

			//Check if the node on the line
			if (edge.onSide(node3) == 0)
			{
				SupportClass.VectorRemoveElement(nodes, node);
				removepoints.Add(node);
				return ;
			}

			if (edge.onSide(node3) == - 1)
			{
				node1 = (Node) nodes[1];
				node2 = (Node) nodes[0];
				edge.Update(node1, node2);
			}

			//add new edges, new triangles
			Edge edge1 = new Edge(node2, node3);
			Edge edge2 = new Edge(node3, node1);
			edge.NextH = edge1;
			edge1.NextH = edge2;
			edge2.NextH = edge;
			hullStart = edge;
			tris.Add(new Triangle(edges, edge, edge1, edge2));

			//An: Add back all the removed nodes
			for (int t=0; t<removepoints.Count; t++)
			{
				node =  ((Node)removepoints[t]);
				SupportClass.VectorRemoveElement(removepoints,node);
				this.Insert(node.x, node.y, node.type);
			}
			return ;
		}

		//if nodes.Count > 3 
		actE = (Edge) edges[0];
		int l;
		if (actE.onSide(node) == - 1)
		{
			if (actE.invE == null)
				l = - 1;
			else
				l = SearchEdge(actE.invE, node);
		}
		else
		{
			l = SearchEdge(actE, node);
		}

		if (l == 0)
		{
			SupportClass.VectorRemoveElement(nodes, node);
			removepoints.Add(node);
			return ;
		}
		if (l > 0)
		{
			ExpandTri(actE, node, l);
		//	return ;
		}
		else
		{
			ExpandHull(node);
		//	return ;
		}

		//An: Add back all the removed nodes
		for (int t=0; t<removepoints.Count; t++)
		{
			node =  ((Node)removepoints[t]);
			SupportClass.VectorRemoveElement(removepoints,node);
			this.Insert(node.x, node.y, node.type);
			
		}
		return;
	}
	
	internal virtual void  ExpandTri(Edge edge, Node node, int i)
	{
		Edge edge1 = edge;
		Edge edge2 = edge1.nextE;
		Edge edge3 = edge2.nextE;
		Node node1 = edge1.p1;
		Node node2 = edge2.p1;
		Node node3 = edge3.p1;
		if (i == 2)
		{
			Edge edge4 = new Edge(node1, node);
			Edge edge6 = new Edge(node2, node);
			Edge edge9 = new Edge(node3, node);
			edge.inT.RemoveEdges(edges);
			SupportClass.VectorRemoveElement(tris, edge.inT);
			tris.Add(new Triangle(edges, edge1, edge6, edge4.MakeSymm()));
			tris.Add(new Triangle(edges, edge2, edge9, edge6.MakeSymm()));
			tris.Add(new Triangle(edges, edge3, edge4, edge9.MakeSymm()));
			SwapTest(edge1);
			SwapTest(edge2);
			SwapTest(edge3);
			return ;
		}
		Edge edge5 = edge1.invE;
		if (edge5 == null || edge5.inT == null)
		{
			Edge edge7 = new Edge(node3, node);
			Edge edge10 = new Edge(node, node2);
			Edge edge12 = new Edge(node1, node);
			Edge edge13 = edge7.MakeSymm();
			edge12.AsIndex();
			edge1.MostLeft().NextH = edge12;
			edge12.NextH = edge10;
			edge10.NextH = edge1.nextH;
			hullStart = edge10;
			SupportClass.VectorRemoveElement(tris, edge1.inT);
			SupportClass.VectorRemoveElement(edges, edge1);
			edges.Add(edge12);
			edges.Add(edge10);
			edges.Add(edge7);
			edges.Add(edge13);
			tris.Add(new Triangle(edge2, edge7, edge10));
			tris.Add(new Triangle(edge3, edge12, edge13));
			SwapTest(edge2);
			SwapTest(edge3);
			SwapTest(edge7);
			return ;
		}
		else
		{
			Edge edge8 = edge5.nextE;
			Edge edge11 = edge8.nextE;
			Node node4 = edge11.p1;
			Edge edge14 = new Edge(node1, node);
			Edge edge15 = new Edge(node2, node);
			Edge edge16 = new Edge(node3, node);
			Edge edge17 = new Edge(node4, node);
			SupportClass.VectorRemoveElement(tris, edge.inT);
			edge.inT.RemoveEdges(edges);
			SupportClass.VectorRemoveElement(tris, edge5.inT);
			edge5.inT.RemoveEdges(edges);
			edge8.AsIndex();
			edge2.AsIndex();
			tris.Add(new Triangle(edges, edge2, edge16, edge15.MakeSymm()));
			tris.Add(new Triangle(edges, edge3, edge14, edge16.MakeSymm()));
			tris.Add(new Triangle(edges, edge8, edge17, edge14.MakeSymm()));
			tris.Add(new Triangle(edges, edge11, edge15, edge17.MakeSymm()));
			SwapTest(edge2);
			SwapTest(edge3);
			SwapTest(edge8);
			SwapTest(edge11);
			SwapTest(edge14);
			SwapTest(edge15);
			SwapTest(edge16);
			SwapTest(edge17);
			return ;
		}
	}
	
	internal virtual void  ExpandHull(Node node)
	{ 
		Edge edge2 = null;
		Edge edge4 = hullStart;
		Edge edge5 = null;
		Edge edge6 = null;
		do 
		{
			Edge edge3 = edge4.nextH;
			if (edge4.onSide(node) == - 1)
			{
				if (edge6 != null)
				{
					Edge edge = edge4.MakeSymm();
					Edge edge1 = new Edge(edge4.p1, node);
					edge2 = new Edge(node, edge4.p2);
					if (edge5 == null)
					{
						hullStart = edge6;
						edge6.NextH = edge1;
						edge6 = edge1;
					}
					else
					{
						edge5.LinkSymm(edge1);
					}
					edge5 = edge2;
					tris.Add(new Triangle(edges, edge, edge1, edge2));
					SwapTest(edge4);
				}
			}
			else
			{
				if (edge5 != null)
					break;
				edge6 = edge4;
			}
			edge4 = edge3;
		}
		while (true);
		edge6.NextH = edge2;
		edge2.NextH = edge4;
	}
	
	internal virtual int SearchEdge(Edge edge, Node node)
	{
		Edge edge1 = null;
		int i;
		if ((i = edge.nextE.onSide(node)) == - 1)
			if (edge.nextE.invE != null)
			{
				return SearchEdge(edge.nextE.invE, node);
			}
			else
			{
				actE = edge;
				return - 1;
			}
		if (i == 0)
			edge1 = edge.nextE;
		Edge edge2 = edge.nextE;
		int j;
		if ((j = edge2.nextE.onSide(node)) == - 1)
			if (edge2.nextE.invE != null)
			{
				return SearchEdge(edge2.nextE.invE, node);
			}
			else
			{
				actE = edge2.nextE;
				return - 1;
			}
		if (j == 0)
			edge1 = edge2.nextE;
		if (edge.onSide(node) == 0)
			edge1 = edge;
		if (edge1 != null)
		{
			actE = edge1;
			if (edge1.nextE.onSide(node) == 0)
			{
				actE = edge1.nextE;
				return 0;
			}
			return edge1.nextE.nextE.onSide(node) != 0?1:0;
		}
		else
		{
			actE = edge2;
			return 2;
		}
	}
	
	internal virtual void  SwapTest(Edge edge)
	{
		Edge edge1 = edge.invE;
		if (edge1 == null || edge1.inT == null)
			return ;
		Edge edge2 = edge.nextE;
		Edge edge3 = edge2.nextE;
		Edge edge4 = edge1.nextE;
		Edge edge5 = edge4.nextE;
		if (edge.inT.InCircle(edge4.p2) || edge1.inT.InCircle(edge2.p2))
		{
			edge.Update(edge4.p2, edge2.p2);
			edge1.Update(edge2.p2, edge4.p2);
			edge.LinkSymm(edge1);
			edge3.inT.Update(edge3, edge4, edge);
			edge5.inT.Update(edge5, edge2, edge1);
			edge2.AsIndex();
			edge4.AsIndex();
			SwapTest(edge2);
			SwapTest(edge4);
			SwapTest(edge3);
			SwapTest(edge5);
		}
	}
	
	public virtual void  DrawPoints(System.Drawing.Graphics g, System.Drawing.Color color)
	{
		SupportClass.GraphicsManager.manager.SetColor(g, color);
		for (int i = 0; i < nodes.Count; i++)
			g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[i]).x - 1, ((Node) nodes[i]).y - 1, 2, 2);
	}
	
	public virtual void  DrawTriangles(System.Drawing.Graphics g, System.Drawing.Color color)
	{
		SupportClass.GraphicsManager.manager.SetColor(g, color);
		if (nodes.Count == 1)
			g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
		if (nodes.Count == 2)
			g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
		for (int i = 0; i < edges.Count; i++)
			((Edge) edges[i]).Draw(g);
		
		for (int j = 0; j < tris.Count; j++)
		{
			Triangle triangle = (Triangle) tris[j];
			triangle.anEdge.Draw(g);
			triangle.anEdge.nextE.Draw(g);
			triangle.anEdge.nextE.nextE.Draw(g);
		}
	}
	
	public virtual void  DrawCircles(System.Drawing.Graphics g, System.Drawing.Color color)
	{
		SupportClass.GraphicsManager.manager.SetColor(g, color);
		for (int i = 0; i < tris.Count; i++)
			((Triangle) tris[i]).DrawCircles(g);
	}
	
	public virtual void  DrawVoronoiDiagram(System.Drawing.Graphics g, System.Drawing.Color color)
	{
		SupportClass.GraphicsManager.manager.SetColor(g, color);
		for (int i = 0; i < edges.Count; i++)
		{
			Edge edge = (Edge) edges[i];
			double d;
			double d1;
			if (edge.invE == null || edge.invE.inT == null)
			{
				double d2 = edge.p1.y - edge.p2.y;
				double d3 = edge.p2.x - edge.p1.x;
				double d4 = System.Math.Sqrt(d2 * d2 + d3 * d3);
				d2 /= d4;
				d3 /= d4;
				d = edge.inT.cx + 9999D * d2;
				d1 = edge.inT.cy + 9999D * d3;
			}
			else
			{
				d = edge.invE.inT.cx;
				d1 = edge.invE.inT.cy;
			}
			//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
			g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), (int) edge.inT.cx, (int) edge.inT.cy, (int) d, (int) d1);
		}
	}
	
	public virtual System.Collections.Hashtable voronoi()
	{
		System.Collections.Hashtable hashtable = new System.Collections.Hashtable();
		for (int i = 0; i < edges.Count; i++)
		{
			Edge edge = (Edge) edges[i];
			//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
			SupportClass.PutElement(hashtable, new System.Drawing.Point((int) edge.inT.cx, (int) edge.inT.cy), new System.Object());
		}
		
		return hashtable;
	}
	
	public virtual void  crust(System.Drawing.Graphics g)
	{
		for (int i = 0; i < edges.Count; i++)
		{
			Edge edge = (Edge) edges[i];
			if (edge.p1.type == 0 && edge.p2.type == 0)
				edge.Draw(g);
		}
	}
	
	
	internal System.Collections.ArrayList nodes;
	internal System.Collections.ArrayList edges;
	internal System.Collections.ArrayList tris;	
	internal Edge hullStart;
	internal Edge actE;

	//An: solve the problem of more than 2 points on the line. 
	internal System.Collections.ArrayList removepoints; 


}