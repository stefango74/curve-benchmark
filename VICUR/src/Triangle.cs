// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Delaunay.java
using System;

class Triangle
{
	
	public Triangle(Edge edge, Edge edge1, Edge edge2)
	{
		Update(edge, edge1, edge2);
	}
	
	public Triangle(System.Collections.ArrayList vector, Edge edge, Edge edge1, Edge edge2)
	{
		Update(edge, edge1, edge2);
		vector.Add(edge);
		vector.Add(edge1);
		vector.Add(edge2);
	}
	
	public virtual void  Update(Edge edge, Edge edge1, Edge edge2)
	{
		anEdge = edge;
		edge.NextE = edge1;
		edge1.NextE = edge2;
		edge2.NextE = edge;
		edge.Tri = this;
		edge1.Tri = this;
		edge2.Tri = this;
		FindCircle();
	}
	
	public virtual Edge GetEdge()
	{
		return anEdge;
	}
	
	internal virtual bool InCircle(Node node)
	{
		return node.Distance(cx, cy) < rad;
	}
	
	internal virtual void  RemoveEdges(System.Collections.ArrayList vector)
	{
		SupportClass.VectorRemoveElement(vector, anEdge);
		SupportClass.VectorRemoveElement(vector, anEdge.nextE);
		SupportClass.VectorRemoveElement(vector, anEdge.nextE.nextE);
	}
	
	internal virtual void  FindCircle()
	{
		double d = anEdge.p1.x;
		double d1 = anEdge.p1.y;
		double d2 = anEdge.p2.x;
		double d3 = anEdge.p2.y;
		double d4 = anEdge.nextE.p2.x;
		double d5 = anEdge.nextE.p2.y;
		double d6 = (d3 - d5) * (d2 - d) - (d3 - d1) * (d2 - d4);
		double d7 = (d + d2) * (d2 - d) + (d3 - d1) * (d1 + d3);
		double d8 = (d2 + d4) * (d2 - d4) + (d3 - d5) * (d3 + d5);
		cx = (d7 * (d3 - d5) - d8 * (d3 - d1)) / d6 / 2D;
		cy = (d8 * (d2 - d) - d7 * (d2 - d4)) / d6 / 2D;
		rad = anEdge.p1.Distance(cx, cy);
	}
	
	public virtual void  DrawCircles(System.Drawing.Graphics g)
	{
		//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
		int i = (int) (cx - rad);
		//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
		int j = (int) (cy - rad);
		//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
		int k = (int) (2D * rad);
		//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
		int l = (int) (2D * rad);
		//UPGRADE_WARNING: Narrowing conversions may produce unexpected results in C#. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1042"'
		g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), (int) (cx - rad), (int) (cy - rad), (int) (2D * rad), (int) (2D * rad));
	}
	
	internal Edge anEdge;
	internal double cx;
	internal double cy;
	internal double rad;
}