using System;

namespace Vision
{
	/// <summary>
	/// 
	/// </summary>
	public class StCurve : System.Collections.ArrayList
	{
		public StCurve()
		{
			// 
			// TODO: Add constructor logic here
			//
		}

		public const int TerminalLength = 10;
		public const float ExtenalRate = 2.0519F;
		public const float ExtenalRateSqr = 4.21F;
		public int curveID=0;
		public float TestPotencialPoint(Point2D potencialpoint,bool head)
		{
			return 0;
		}
		
		public double DistanceMeanClosedCurve()
		{
			double distanceMean = 0;
			int pointscount = this.Count;
			double [] dis=new double[pointscount];

			for(int i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}	
	
			dis[pointscount-1]=((Point2D)this[0]).Distance((Point2D)this[pointscount-1]);

			for (int i = 0; i <= pointscount-1; i++)
				distanceMean = distanceMean + dis[i];
					
			distanceMean = distanceMean / (pointscount);
			return distanceMean;
		}

		//for open curve
		public double DistanceMean()
		{
						
			double distanceMean = 0;
			int pointscount = this.Count;
			double [] dis=new double[pointscount];

			for(int i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}		

			for (int i = 0; i < pointscount-1; i++)
				distanceMean = distanceMean + dis[i];
					
			distanceMean = distanceMean / (pointscount-1);
			return distanceMean;
		}

		public double DistanceMeanRange(int firstIndex, int lastIndex)
		{
			//double [] dis=new double[lastIndex-firstIndex];
			double disMean = 0;

			for(int i=firstIndex;i<lastIndex;i++)
			{
				disMean = disMean + ((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}	

			return disMean/(lastIndex-firstIndex);
		}

		public double StdRange(int firstIndex, int lastIndex)
		{
			double [] dis=new double[lastIndex-firstIndex];
			double disMean = DistanceMeanRange(firstIndex, lastIndex);
			int j=0;
			double std = 0;

			for(int i=firstIndex;i<lastIndex;i++)
			{				
				dis[j]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
				j++;
			}	

			for (int i=0; i<dis.Length; i++)
			{
				std = std + (dis[i]-disMean)*(dis[i]-disMean);
			}

			std = std/(dis.Length-1);			
			std = Math.Sqrt(std);
			
			return std;

		}
		public double StdDistance()
		{
			
			double distanceMean = DistanceMean();
			int pointscount = this.Count;
			double [] dis=new double[pointscount];
			double distanceVariance=0;

			for(int i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}		

			//compute distance variance
			for (int i = 0; i < pointscount-1; i++)
				distanceVariance = distanceVariance + ((double)dis[i] - distanceMean)*((double)dis[i] - distanceMean);

			if (pointscount > 2 ) 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 2));
			}
			else 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 1));
			}

			return distanceVariance;
			
		}
		public double AngleMean()
		{
			int pointscount = this.Count;
			
			double angleMean=0;

			//compute angle mean
			if (pointscount > 3)
			{
				for (int i = 1; i < pointscount-1; i++)
					angleMean = angleMean + ((Vision.Point2D)this[i]).Angle;
					
				angleMean = angleMean / (pointscount-2);

				//compute angle variance
//				for (i = 1; i < pointscount-1; i++)
//					angleVariance = angleVariance + (((Vision.Point2D)this[i]).Angle - angleMean)*((double)dis[i] - angleMean);
//
//				angleVariance = Math.Sqrt(distanceVariance / (pointscount - 3));
//				
			}
			else if (pointscount ==3)	//only 1 angle
			{
				angleMean = ((Vision.Point2D)this[1]).Angle;
			}
			else
				angleMean = 180;

			return angleMean;
		}
		public override object Clone()
		{
			Vision.StCurve curve = new StCurve();
			for (int i=0; i<this.Count; i++)
			{
				//Vision.Point2D pt = new Point2D(
				Vision.Point2D pt = new Point2D(((Vision.Point2D)this[i]).X,((Vision.Point2D)this[i]).Y);
				pt.Angle = ((Vision.Point2D)this[i]).Angle;
				
				curve.AddPoint(pt);
			}
			return curve;			
		}

		public bool NearHead(Point2D tempPoint)
		{
			return true;
		}

		public Point2D ReadHead()
		{
			switch (this.Count )
			{
				case 0: return null; 
				default:return (Point2D)this[0];
			}
			
		}

		public Point2D ReadTail()
		{
			switch (this.Count )
			{
				case 0: return null; 
				default:return (Point2D)this[this.Count-1];
			}			
		}


		public bool AddPoint(Point2D mypoint)
		{
	        if(!this.CatchPoint(mypoint))
				this.Add(mypoint);
			return true;
		}
		public bool AddPoint(int x,int y)
		{
			Point2D mypoint=new Point2D(x,y);
			if(!this.CatchPoint(mypoint))
				this.Add(mypoint);
			return true;
		}
		public bool AddPoint(StCurve mycurve)
		{
			int i=0;
			if (mycurve.Count!=0)
				for(i=0;i<=mycurve.Count;i++)
					if (!this.AddPoint((Point2D)mycurve[i]))
						return false;
			return true;
		}

		public bool Headconnectable=true;
		public bool TailConnectable=true;

		public bool CatchPoint(Point2D mypoint)
		{
			int i=0;
			for(i=0;i<this.Count;i++)
			{
				Point2D pt=(Point2D	) this[i];
				if (pt.X== mypoint.X && pt.Y== mypoint.Y)
					return true;
			}
			return false;
		}

		public int CatchPointEx(Point2D mypoint)
		{
			int i=0;
			for(i=0;i<this.Count;i++)
			{
				Point2D pt=(Point2D	) this[i];
				if (pt.X== mypoint.X && pt.Y== mypoint.Y)
					return i;
			}
			return -1;
		}

		public int Closed=0;

		public int ExternalLength(int externalindex)
		{
			
			
			int i=0;
			if(this.Count>=TerminalLength)
			switch(externalindex)
			{
				case 0:
				{
				    this.ReadSegmentLegth(0,TerminalLength);
					return (int) ExtenalRateSqr*i;
				}
				case -1:
				{
					i=this.ReadSegmentLegth(this.Count-1-TerminalLength,this.Count-1);
					return (int) (ExtenalRateSqr*i);
				}
				default: return 0;
			}
			else
				return -1;
			
			


		}

		public int ReadSegmentLegth(int firstpoint, int secondpoint)
		{
			int i=0,j=0;
			if (firstpoint>secondpoint)
			{ 
				i= firstpoint;
				firstpoint=secondpoint;
				secondpoint=i;
			}
			if (firstpoint> this.Count|| secondpoint>this.Count||firstpoint==secondpoint)
				return -1;
			else
			{
				for(i=firstpoint;i<secondpoint;i++)
				{
					j=j+(((Point2D)this[i]).Y-((Point2D)this[i+1]).Y)*(((Point2D)this[i]).Y-((Point2D)this[i+1]).Y)
					   +(((Point2D)this[i]).X-((Point2D)this[i+1]).X)*(((Point2D)this[i]).X-((Point2D)this[i+1]).X);	
				}
				return j;
			}
		}
		public System.Collections.ArrayList ReadSegments(int firstpoint, int secondpoint)
		{
			int i=0,j=0;
			System.Collections.ArrayList rt;
			rt=new System.Collections.ArrayList();
			
			if (firstpoint<secondpoint)
			{
				if (firstpoint> this.Count|| secondpoint>this.Count||firstpoint==secondpoint)
					return rt;
				else
				{
					for(i=firstpoint;i<secondpoint;i++)
					{
						j=(((Point2D)this[i]).Y-((Point2D)this[i+1]).Y)*(((Point2D)this[i]).Y-((Point2D)this[i+1]).Y)
							+(((Point2D)this[i]).X-((Point2D)this[i+1]).X)*(((Point2D)this[i]).X-((Point2D)this[i+1]).X);	
						rt.Add(Math.Sqrt((double)j));
					}
					return rt;
				}
			}
			if (firstpoint>=secondpoint)
			{ 
				if (firstpoint> this.Count|| secondpoint>this.Count||firstpoint==secondpoint)
					return rt;
				else
				{
					for(i=firstpoint;i>secondpoint;i--)
					{
						j=(((Point2D)this[i]).Y-((Point2D)this[i-1]).Y)*(((Point2D)this[i]).Y-((Point2D)this[i-1]).Y)
							+(((Point2D)this[i]).X-((Point2D)this[i-1]).X)*(((Point2D)this[i]).X-((Point2D)this[i-1]).X);	
						rt.Add(Math.Sqrt((double)j));
					}
					return rt;
				}
			}
			return rt;
		}
		public int ExternalLengthEx(int externalindex,double l)
		{
			
			double h,havg,s,sig,lo,et;
			
			System.Collections.ArrayList rt;
			if (externalindex==0)
				rt=this.ReadSegments(0,this.Count-1);
			else
			    rt=this.ReadSegments(this.Count-1,0);					
		
			if (rt.Count>1)
			{
				lo=(double)rt[0];
				h=(l+lo)/2;
				s=(Math.Abs(l-lo))/(Math.Sqrt(2));
				Optimismfilter op=new Optimismfilter();
				sig=op.Variance(rt);
				havg=op.Average(rt);
				et=havg/sig;
				if(s!=0)
					{
					  et=havg*h/s*Math.Pow(et+1,sig/h);
					   return (int)et;			
					 }
				else
					return 10000000;
			}
			else
				return (int)(2.05*(double)rt[0]);

		}
		public int ReadHeadToTailDistanceSqr()
		{
			if (this.Count>2)
			{
				return (int) ((Point2D)this[0]).DistanceSqr((Point2D)this[this.Count-1]);
			}
			else return (int)( ((Point2D)this[0]).DistanceSqr((Point2D)this[this.Count-1])*ExtenalRateSqr);
			
		}

		public void PushPoint(Point2D mypoint)
		{
			
			this.Insert(0,mypoint);
			
		}

		public Point2D FindNearestPoint(Point2D mypoint)
		{

			int i=0,dis=0,j=0,indx=0;
			dis=mypoint.DistanceSqr((Point2D)this[0]);
			for(i=0;i<this.Count;i++)
			{
				j=mypoint.DistanceSqr((Point2D)this[i]);
				if (j<=dis)
				{
					dis=j;
					indx=i;
				}
			}	
			return (Point2D)this[indx];
		}

		public bool AbSorbPoint(Point2D mypoint)
		{
			Point2D np;
			np=this.FindNearestPoint(mypoint);

			return true;
		}
		public bool CheckInterSection(Point2D a,Point2D b)
		{
			
			Optimismfilter op=new Optimismfilter();
			int i,j;
			Point2D c;
			Point2D d;
			bool cr;
			cr=false;
			if (this.Count>=2)
					for(j=1;j<this.Count;j++)
					{
						c=(Point2D)this[j-1];
						d=(Point2D)this[j];
						if(!(c.Equals(b)||d.Equals(b)))
						{
							cr=op.CheckCross(c,d,a,b);
							if (cr)
								return cr;
						}
					}
			return cr;
		}
		// Return the relationship between two open curves
		public int CurveRelationship(StCurve cl)
		{
			Point2D pt1, pt2, pt3, pt4;

			double dt1 = 0, dt2 = 0, dt3 = 0, dt4 = 0;

			pt1 = (Point2D)this[0];
			pt2 = (Point2D)this[this.Count-1];
			pt3 = (Point2D)cl[0];
			pt4 = (Point2D)cl[cl.Count-1];

			// head to head-1
			dt1 = pt1.Distance(pt3);
			
			// head to tail-2
			dt2 = pt1.Distance(pt4);

			// tail to head-3
			dt3 = pt2.Distance(pt3);

			// tail to tail-4
			dt4 = pt2.Distance(pt4);

			if( (dt1 <= dt2) && (dt1 <= dt3) && (dt1 <= dt4) )
			{
				return 1;
			}
			else if( (dt2 <= dt1) && (dt2 <= dt3) && (dt2 <= dt4) )
			{
				return 2;
			}
			else if( (dt3 <= dt1) && (dt3 <= dt2) && (dt3 <= dt4) )
			{
				return 3;
			}
			else 
			{
				return 4;
			}
		}

		public double ConnectiveValue(int order,double x,double y)
		{
			if(this.Count<2) return -100d;  // It is an Abnormal curve . It should not have less than two  points!
			
			int i=0,j=0,pointscount=0;
			int testcount=0;
			if(this.Count>=4)
				pointscount=4;
			else
				pointscount=this.Count;
			
			testcount=pointscount-1;
			double [] BestConnectiveValue=new double [testcount];
			for(j=0;j<testcount;j++)
			
			{
			pointscount=j+2;
			#region calculate the connective value. We should concider the close priority.
			double [,] cr=new double[2,pointscount+1]; //0-->x,1-->y
			
		   //If the order >0, compute from the end of the curve
			if(order!=0)
			{
				for(i=0;i<pointscount;i++)
				{
					cr[0,i]=((Point2D)this[i+this.Count-pointscount]).X;
					cr[1,i]=((Point2D)this[i+this.Count-pointscount]).Y;
				}								
			}
			else  
			{
				//compute from the front of the curve
				for(i=0;i<pointscount;i++)
				{
					cr[0,i]=((Point2D)this[pointscount-i-1]).X;
					cr[1,i]=((Point2D)this[pointscount-i-1]).Y;
				}
			}
			cr[0,pointscount]=x;
			cr[1,pointscount]=y;
			#endregion

			BestConnectiveValue[j]= ConnectiveValue(pointscount+1,cr);
			}
			
			double BestValue=-100d;
			for(j=0;j<testcount;j++)
			{
				if(BestConnectiveValue[j]>BestValue)
					BestValue=BestConnectiveValue[j];
			}
			return BestValue;
		}

		#region compute the connective value based on distance-only algorithm
		public double DistanceBasedConnectiveValue(int order,Point2D nearestPoint)
		{
			if(this.Count<2) return -100d;  // It is an Abnormal curve . It should not have less than two  points!
			
			int i=0,j=0,pointscount=0;
			//int testcount=0;
			//if(this.Count>=4)
			//	pointscount=4;
			//else
			
			//pointscount=this.Count;
			
			//testcount=pointscount-1;
			//double [] BestConnectiveValue=new double [testcount];

			//compute distance Mean
			pointscount = this.Count;
			
			double [] dis=new double[pointscount];			
			double distanceMean = 0;
			double distanceVariance = 0;


			for(i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}					
			for (i = 0; i < pointscount-1; i++)
				distanceMean = distanceMean + dis[i];
					
			distanceMean = distanceMean / (pointscount-1);

			//compute distance variance
			for (i = 0; i < pointscount-1; i++)
				distanceVariance = distanceVariance + ((double)dis[i] - distanceMean)*((double)dis[i] - distanceMean);

			if (pointscount > 2 ) 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 2));
			}
			else 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 1));
			}
			
			
			#region calculate the connective value. 
			double connectedValue = 1;
			double distance =0, stdeviation = 0;
			double neighborDistance = 0; //distance between neighbor and endpoint on the curve
			double nearestDistance = 0;  //distance between curve endpoint and nearest sample point

			if (distanceVariance !=0)
			{
				connectedValue = distanceMean/distanceVariance;
				connectedValue = Math.Pow((1 + connectedValue),1/connectedValue);
			}

			connectedValue = connectedValue*distanceMean;
			//	double [,] cr=new double[2,pointscount+1]; //0-->x,1-->y
			
			if(order!=0)  //point at the end side of curve
			{
					
				neighborDistance = ((Point2D)this[pointscount-1]).Distance((Point2D)this[pointscount-2]);
                nearestDistance =  ((Point2D)this[pointscount-1]).Distance(nearestPoint);
									
			}
			else
			{
				neighborDistance = ((Point2D)this[0]).Distance((Point2D)this[1]);
				nearestDistance =  ((Point2D)this[0]).Distance(nearestPoint);
			}

			distance = (neighborDistance + nearestDistance)/2;
			stdeviation = Math.Abs(neighborDistance - nearestDistance)/Math.Sqrt(2);

			if (stdeviation==0)
			{
				connectedValue = Double.MaxValue;
			}
			else
			{
				connectedValue = connectedValue*distance/stdeviation;
			}
			#endregion			
			return connectedValue;
		}
		#endregion


		#region compute connective value using new formula
		public double DSconnectivalue( double refangle, double refdistance, double smoothness)
		{
			if(this.Count<2) return -100d;  // It is an Abnormal curve . It should not have less than two  points!
			
			int i=0, pointscount=0;

			//compute distance Mean
			pointscount = this.Count;
			
			double [] dis=new double[pointscount];			
			double distanceMean = 0;
			double distanceVariance = 0;
			double angleMean = 0;
			double angleVariance = 0;
						
			for(i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}					
			
			for (i = 0; i < pointscount-1; i++)
				distanceMean = distanceMean + dis[i];
					
			distanceMean = distanceMean / (pointscount-1);

			//compute distance variance
			for (i = 0; i < pointscount-1; i++)
				distanceVariance = distanceVariance + ((double)dis[i] - distanceMean)*((double)dis[i] - distanceMean);

			if (pointscount > 2 ) 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 2));
			}
			else 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 1));
			}
			
			//compute angle mean
			if (pointscount > 3)
			{
				for (i = 1; i < pointscount-1; i++)
					angleMean = angleMean + ((Vision.Point2D)this[i]).Angle;
					
				angleMean = angleMean / (pointscount-2);

				//compute angle variance
				for (i = 1; i < pointscount-1; i++)
					angleVariance = angleVariance + (((Vision.Point2D)this[i]).Angle - angleMean)*((double)dis[i] - angleMean);

				angleVariance = Math.Sqrt(distanceVariance / (pointscount - 3));
				
			}
			else if (pointscount ==3)	//only 1 angle
			{
				angleMean = ((Vision.Point2D)this[1]).Angle;
			}else
				angleMean = 180;
			

			#region calculate the connective value. 
			double connectedValue = 0;
			//c1 = 0.57 
			double c1;
			//double c2= 1  - Math.Pow(c1,2);
			//double c2 = 1-c1;
			double c2;
			double temp = 0;

			c1=0.7; //angle
			c1 = smoothness;
			c2=1-c1; //distance
			
			
			temp = refdistance/(2*(distanceVariance + distanceMean));
			temp = c2*(Math.Pow(temp,2));
			

			connectedValue = c1*(Math.Pow((refangle/angleMean - 1),2));
			//connectedValue = c1*9/16*(Math.Pow((refangle/angleMean - 1),2));
			//connectedValue = c1*(Math.Pow((refangle/angleMean),2));
			connectedValue = connectedValue + temp +1; 

			//connectedValue = 1/Math.Sqrt(connectedValue);
			connectedValue = 1/connectedValue;

			#endregion			
			return connectedValue;
		}
		#endregion end new formula
		
		#region formula from experiment version1
		public double experiment_1( double refangle, double refdistance)
		{
//			Function YData=m1+m2*x1+m3*sin(x2*PI/2)+m4*(x3+1)*cos(((x4-x2)*m5)^2*PI/2)+m6*x1^2;
//
//			m1         	0.883788095789014
//			m2         	-1.16282654142143
//			m3         	0.145601004505025
//			m4         	0.0587871842738464
//			m5         	8.761418342343
//			m6         	0.269130460958252
//
//			x1 = ref_dis / ave_dis
//			x2 = ref_angle/180
//			x3 = std/(ave_dis*0.4931)
//			x4 = ave_angle/180

			if(this.Count<2) return -100d;  // It is an Abnormal curve . It should not have less than two  points!
			
			int i=0, pointscount=0;
			//compute distance Mean
			pointscount = this.Count;
			
			double [] dis=new double[pointscount];			
			double distanceMean = 0;
			double distanceVariance = 0;
			double angleMean = 0;
			double angleVariance = 0;
						
			
			for(i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}					
			
			for (i = 0; i < pointscount-1; i++)
				distanceMean = distanceMean + dis[i];
					
			distanceMean = distanceMean / (pointscount-1);

			//compute distance variance
			for (i = 0; i < pointscount-1; i++)
				distanceVariance = distanceVariance + ((double)dis[i] - distanceMean)*((double)dis[i] - distanceMean);

			if (pointscount > 2 ) 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 2));
			}
			else 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 1));
			}
			
			//compute angle mean
			if (pointscount > 3)
			{
				for (i = 1; i < pointscount-1; i++)
					angleMean = angleMean + ((Vision.Point2D)this[i]).Angle;
					
				angleMean = angleMean / (pointscount-2);

				//compute angle variance
				for (i = 1; i < pointscount-1; i++)
					angleVariance = angleVariance + (((Vision.Point2D)this[i]).Angle - angleMean)*((double)dis[i] - angleMean);

				angleVariance = Math.Sqrt(distanceVariance / (pointscount - 3));
				
			}
			else if (pointscount ==3)	//only 1 angle
			{
				angleMean = ((Vision.Point2D)this[1]).Angle;
			}
			else
				angleMean = 180;
			
			double x1 = refdistance / distanceMean;
			double x2 = refangle/180;
			double x3 = distanceVariance/(distanceMean*0.4931);
			double x4 = angleMean/180;

			#region calculate the connective value. 
			double connectedValue = 0;
			double temp = 0;

			double			m1=        	0.883788095789014;
			double			m2=         	-1.16282654142143;
			double			m3=         	0.145601004505025;
			double			m4=         	0.0587871842738464;
			double			m5=         	8.761418342343;
			double			m6=         	0.269130460958252;

			//Function YData=m1+m2*x1+m3*sin(x2*PI/2)+m4*(x3+1)*cos(((x4-x2)*m5)^2*PI/2)+m6*x1^2;
			
			connectedValue = m3*Math.Sin(x2*Math.PI/2);
			connectedValue = connectedValue + m1 + m2*x1;

			temp = ((x4-x2)*m5)*((x4-x2)*m5)*Math.PI/2;
			temp = m4*(x3+1)*Math.Cos(temp);
			temp =  temp + m6*x1*x1;
						
			connectedValue = connectedValue + temp; 		
			#endregion			
			return connectedValue;
		}
		#endregion

		#region formula from experiment
		public double experiment(double refangle, double refdistance)
		{
			
			double m1	=0.60381;
			double m2	=0.25396;
			double m3	=-1.09871;
			double m4=	0.35376;
			double m5=	-0.22202;
			double m6=	0.27243;
			double m7=	0.27791;
			double m8=	0.01845;
			double m9=	-0.09315;
	


			if(this.Count<2) return -100d;  // It is an Abnormal curve . It should not have less than two  points!
			
			int i=0, pointscount=0;
			//compute distance Mean
			pointscount = this.Count;
			
			double [] dis=new double[pointscount];			
			double distanceMean = 0;
			double distanceVariance = 0;
			double angleMean = 0;
			double angleVariance = 0;
						
			
			for(i=0;i<pointscount-1;i++)
			{
				dis[i]=((Point2D)this[i]).Distance((Point2D)this[i+1]);
			}					
			
			for (i = 0; i < pointscount-1; i++)
				distanceMean = distanceMean + dis[i];
					
			distanceMean = distanceMean / (pointscount-1);

			//compute distance variance
			for (i = 0; i < pointscount-1; i++)
				distanceVariance = distanceVariance + ((double)dis[i] - distanceMean)*((double)dis[i] - distanceMean);

			if (pointscount > 2 ) 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 2));
			}
			else 
			{
				distanceVariance = Math.Sqrt(distanceVariance / (pointscount - 1));
			}
			
			//compute angle mean
			if (pointscount > 3)
			{
				for (i = 1; i < pointscount-1; i++)
					angleMean = angleMean + ((Vision.Point2D)this[i]).Angle;
					
				angleMean = angleMean / (pointscount-2);

				//compute angle variance
				for (i = 1; i < pointscount-1; i++)
					angleVariance = angleVariance + (((Vision.Point2D)this[i]).Angle - angleMean)*((double)dis[i] - angleMean);

				angleVariance = Math.Sqrt(distanceVariance / (pointscount - 3));
				
			}
			else if (pointscount ==3)	//only 1 angle
			{
				angleMean = ((Vision.Point2D)this[1]).Angle;
			}
			else
				angleMean = 180;
			
			double x1 = refdistance / distanceMean;			
			double x2 = refangle/180;
			double x3 = distanceVariance/(distanceMean);
			double x4 = angleMean/180;
			double x5 = pointscount;

			#region calculate the connective value. 
			double connectedValue = 0;
			double temp = 0;

			if (x5 >4)
				x5 = 4;

			
//			Y = m1 + m2*x1^2 + m3*x1 + m4*sin(x2*PI/2) + m5*x3^2 + m6*x3
//+ m7*cos((x4-x2)*PI/2) + m8*x5^2 + m9*x5

			double A = m2*x1*x1;
			double B = m3*x1;
			double C = m4*Math.Sin(x2*Math.PI/2);
			double D = m5*x3*x3;
			double E = m6*x3;
			double F = m7*Math.Cos((x4-x2)*Math.PI/2);
			double G = m8*x5*x5;
			double H = m9*x5;
			
			connectedValue = m1 + A + B + C;
			connectedValue = connectedValue + D + E + F;
			connectedValue = connectedValue + G + H;

			#endregion			
			return connectedValue;
		}
		#endregion


		private double distance(double x1,double x2,double y1,double y2)
		{
			return Math.Sqrt(Math.Pow(x1-x2,2)+Math.Pow(y1-y2,2));
		}

		
		private double angle(double x1,double x2,double x3,double y1,double y2,double y3)
		{
			double vx1=0,vx2=0,vy1=0,vy2=0,length1=0,length2=0;
			double cosTheta=0;
			double vk=0;
			bool Convex=true;
			vx1=x2-x1;
			vx2=x3-x2;
			vy1=y2-y1;
			vy2=y3-y2;
			length1=Math.Sqrt(vx1*vx1+vy1*vy1);
			length2=Math.Sqrt(vx2*vx2+vy2*vy2);
			cosTheta = (vx1*vx2+vy1*vy2)/(length1*length2); //simplified form of law of cosine 
			//vk = a.m_X * b.m_Y - a.m_Y * b.m_X;
			vk = vx1*vy2-vy1*vx2; 
//			double d1=0,d2=0;
//			d1=Math.Pow(vx2-vx1,2)+Math.Pow(vy2-vy1,2);
//			d2=Math.Pow(vx2+vx1,2)+Math.Pow(vy2+vy1,2);
			Convex=vk>0;
			if(Convex)
			{
				return Math.PI-Math.Acos(cosTheta);
			}
			else
			{
			
				return (Math.PI + Math.Acos(cosTheta));
			}			
		}

		public double ConnectiveValue(int count,double [,] cr)
		{
			double [] xi =new double[16];
			int i=0;
			double dvalue=0;

			double [] dis=new double[count];
			double [] ang=new double[count-1];

			double xi1=0,xi2=0,xi3=0,xi4=0;
			xi4=count-1;
			for(i=0;i<count-1;i++)
			{
				dis[i+1]=distance(cr[0,i],cr[0,i+1],cr[1,i],cr[1,i+1]);
			}
			int directiontest=0;
			int bigangle=0;
			for(i=0;i<count-2;i++)
			{
				ang[i+1]=angle(cr[0,i],cr[0,i+1],cr[0,i+2],cr[1,i],cr[1,i+1],cr[1,i+2]);
			//	if(ang[i+1]>Math.PI)
			//		directiontest++;
				if(Math.Abs(ang[i+1]-Math.PI)>Math.PI/3)
					bigangle++;
				if(ang[i+1]>Math.PI)
				{
					directiontest++;
					ang[i+1]=2*Math.PI-ang[i+1];
				}
			}

			if((directiontest!=0)&&(directiontest!=(count-2))&&(bigangle>0))
				return -50d;


			double myVariance = 0;
			double simplemean = 0;
			for (i = 1; i < count; i++)
				simplemean = simplemean + dis[i];

			simplemean = simplemean / (count-1);

			for (i = 1; i < count; i++)
				myVariance = myVariance + ((double)dis[i] - simplemean) *
					((double)dis[i] - simplemean);
			myVariance = Math.Sqrt(myVariance / (count - 2));
			xi3=myVariance/simplemean*100;

			myVariance = 0;
			simplemean = 0;
			for (i = 1; i < count-1; i++)
				simplemean = simplemean + ang[i];
			simplemean = simplemean / (count-2);
			if(count>3)
			{
				for (i = 1; i < count-1; i++)
					myVariance = myVariance + (ang[i] - simplemean) *
						(ang[i] - simplemean);
				myVariance = Math.Sqrt(myVariance / (count - 3));
			}
			xi1=simplemean;
			xi2=myVariance;

			xi[1]=1*27.5648;
			xi[2]=xi1*0.5151;
			xi[3]=xi2*0.0509;
			xi[4]=xi3*(-0.0611);

			xi[5]=xi4*(-12.7037);
			xi[6]=xi1*xi2*(-0.0009);
			xi[7]=xi1*xi3*(0.0002);
			xi[8]=xi1*xi4*(-0.0944);

			xi[9]=xi2*xi3*(0.0028);
			xi[10]=xi2*xi4*(0.1944);
			xi[11]=xi3*xi4*(0.0167);
			xi[12]=xi1*xi1*(-0.0043);

			xi[13]=xi2*xi2*(-0.0556);
			xi[14]=xi3*xi3*(-0.0044);
			xi[15]=xi4*xi4*(2.2778);
			
			for(i=1;i<16;i++)
			{
				dvalue=dvalue+xi[i];
			}

			return dvalue;

		}
		}
}
