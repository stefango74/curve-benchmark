using System;

namespace GeometryUtility
{
	/// <summary>
	///To define the common types used in 
	///Analytical Geometry calculations.
	/// </summary>
	
	//To define some constant Values 
	//used for local judgment 
	public struct ConstantValue
	{
		internal const  double SmallValue=0.00001;
		internal const double BigValue=99999;
		internal const double zeroV = 10E-5;
		//internal const double M_PI = 3.141516;
		internal const double GC = 120 * 3.1425926 / 180;
		//const double UC = 145 * 3.1415926 / 180;
		//const double IC = 45 * 3.1415926 / 180;
		internal const double del = 10E-6;
	}
	
	public enum VertexType
	{
		ErrorPoint,
		ConvexPoint,
		ConcavePoint		
	}

	public enum PolygonType
	{
		Unknown,
		Convex, 
		Concave	
	}

	public enum PolygonDirection
	{
		Unknown,
		Clockwise,
		Count_Clockwise
	}
}

