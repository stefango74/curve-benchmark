set title "Self-Intersecting curve reconstruction"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set yrange [0:*]
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'Self-intersecting.pdf'
plot "Self-Intersecting.dat" using 5:xticlabels(1) title "Distance", "Self-Intersecting.dat" using 7:xticlabels(1) title "Angle"
