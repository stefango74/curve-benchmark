import java.util.Date;


int r=50; // Edit this variable for changing sampling density
String path = "./BoundaryImages1070"; //The path where the input boundary images exists


String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    return null;
  }
}
void setup() {
  size(100,100);
  String[] filenames = listFileNames(path);
  File[] files = listFiles(path);
  println("Totally "+files.length+" files");
  for (int i = 0; i < files.length; i++) 
  {
    File f = files[i];    
    int UserFileNameStringLength = f.getName().length();
    String extension = f.getName().substring(UserFileNameStringLength-3);
    if(f.getName().endsWith("png")||f.getName().endsWith("jpg")||f.getName().endsWith("bmp")
    ||f.getName().endsWith("PNG")||f.getName().endsWith("JPG")||f.getName().endsWith("BMP"))
    {
       PImage im=loadImage(path+"/"+f.getName());
       PGraphics pim1;
       pim1=createGraphics(im.width,im.height);
       pim1.beginDraw();
       pim1.background(255);
       pim1.stroke(0);
       pim1.fill(0);
       PrintWriter op;
       op = createWriter("./TextFiles/"+f.getName()+".txt"); 
       for(int j=0;j<im.width;j++)
       for(int k=0;k<im.height;k++)
       {
         if(red(im.get(j,k))<200)
         {
           for(int k1=j-(r/2);k1<j+(r/2);k1++)
           for(int k2=k-(r/2);k2<k+(r/2);k2++)
           {
             if(k1>=0&&k2>=0&&k1<im.width&&k2<im.height)
               if(distance(j,k,k1,k2)<r/2.0)
                 im.set(k1,k2,color(255));
           }
           op.println(j+" "+k);
           pim1.ellipse(j,k,20,20);
           im.set(j,k,color(0,0,0));
         }
       }
       op.close();
       op.flush();
       pim1.endDraw();
       pim1.save("./SampledFigure/"+f.getName());
    }
  }
  exit();
}
double distance(int x1,int y1,int x2,int y2)
{
  return sqrt(sq(x2-x1)+sq(y2-y1));
}