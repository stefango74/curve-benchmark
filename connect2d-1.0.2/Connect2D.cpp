/*
 * Connect2D.cpp
 *
 *  Created on: 2011-05-17
 *      Author: Stefan Ohrhallinger
 */

#include "DisjointSets.h"

#include "Connect2D.h"

namespace connect2d
{

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_face_base_with_info_2<NTriangle, K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Delaunay;
typedef K::Point_2 Point;
typedef Delaunay::Vertex_handle Vertex;
typedef Delaunay::Face_handle Face;
typedef Delaunay::Edge DEdge;

#define SQR(a) ((a)*(a))

enum { FACE_UNMARKED, FACE_MARKED };

/*
 * construct Delaunay triangulation from point array
 */
void constructDT(Delaunay &dt, vector<pair<double, double> > *pointVec)
{
	int i;

	for (i = 0; i < (int)pointVec->size(); i++)
	{
		Point point((*pointVec)[i].first, (*pointVec)[i].second);
		Vertex vertex = dt.insert(point);
		vertex->info() = i;
	}

	// set index for infinite vertex for debugging purposes
	dt.infinite_vertex()->info() = -1;
}

/*
 * construct complementary data structure of NTriangle, NEdge for CGAL DT2D
 */
void createComplementaryDataStructure(Delaunay &dt, list<NEdge> &nEdges)
{
	int i;

	// traverse all triangles and create their edges + references to them
	stack<Face> triangleStack;
	triangleStack.push(dt.infinite_face());

	// while stack not empty
	while (triangleStack.size() > 0)
	{
		// pop triangle from stack
		Face fh = triangleStack.top();
		triangleStack.pop();
		NTriangle *currTri = &fh->info();

		// only handle unhandled ones
		if (currTri->edge(0) == NULL)
		{
			// reference its three edges (create if not yet existing)
			for (i = 0; i < 3; i++)
			{
				// look up at neighbor triangle
				Face oppFH = fh->neighbor(i);
				int oppIndex = oppFH->index(fh);
				NTriangle *oppTri = &oppFH->info();

				// edge exists already?
				NEdge *edge = oppTri->edge(oppIndex);

				if (edge == NULL)
				{
					// create new edge
					Vertex vertex[2] = { fh->vertex(fh->cw(i)), fh->vertex(fh->ccw(i)) };
					NEdge newEdge(vertex[0], vertex[1]);
					nEdges.push_back(newEdge);
					edge = &nEdges.back();

					// also push triangle on stack as unhandled
					triangleStack.push(oppFH);
				}

				currTri->setEdge(i, edge);	// reference edge
			}
		}
	}
}

/*
 * returns edge for halfedge
 */
NEdge *edgeForHE(DEdge he)
{
	return he.first->info().edge(he.second);
}

/*
 * returns degree of vertex
 */
int degreeOfVertex(Vertex vh)
{
	int degree = 0;
	Delaunay::Edge_circulator startEC = vh->incident_edges();
	Delaunay::Edge_circulator currEC = startEC;

	do
	{
		if (edgeForHE(*currEC)->exists())
			degree++;

		currEC++;
	} while (currEC != startEC);

	return degree;
}

/*
 * construct rho-complex
 * add edges in DG with ascending length until all vertices have degree >= 2
 */
void constructRhoComplex(Delaunay &dt, list<NEdge> &nEdges)
{
	int i;

	// initialize a tree for each vertex
	int treeCount = dt.number_of_vertices();
	DisjointSets dSet(treeCount);

	// sort all edges in delaunay graph by ascending edge length
	multimap<float, NEdge *> edgeMMap;

	for (list<NEdge>::iterator iter = nEdges.begin(); iter != nEdges.end(); iter++)
	{
		NEdge *edge = &*iter;
		Vertex v0 = edge->vertex(0);
		Vertex v1 = edge->vertex(1);

		// only use finite edges
		if (!dt.is_infinite(v0) && !dt.is_infinite(v1))
		{
			float distance = sqrt(SQR(v0->point().x() - v1->point().x()) + SQR(v0->point().y() - v1->point().y()));
			edgeMMap.insert(pair<float, NEdge *>(distance, edge));
		}
	}

	set<Vertex> leafVertexSet;

	// while more than one tree exists, or leaf vertices left
	while ((treeCount > 1) || (leafVertexSet.size() > 0))
	{
		// remove minimal edge from set
		multimap<float, NEdge *>::iterator edgeIter = edgeMMap.begin();
		NEdge *edge = edgeIter->second;
		edgeMMap.erase(edgeIter);

		// check if that edge connects two yet unconnected trees
		int root0 = dSet.FindSet(edge->vertex(0)->info());
		int root1 = dSet.FindSet(edge->vertex(1)->info());

		bool insert = false;

		// if not in same tree, connect the two and add edge to rho-complex
		if (root0 != root1)
		{
			dSet.Union(root0, root1);
			treeCount--;
			insert = true;
		}
		else
			// check if edge connects (1 or 2) leaf vertices
			insert = ((degreeOfVertex(edge->vertex(0)) == 1) || (degreeOfVertex(edge->vertex(1)) == 1));

		if (insert)
		{
			// if any leaf vertices become degree 2, remove from set
			for (i = 0; i < 2; i++)
			{
				Vertex vh = edge->vertex(i);

				if (degreeOfVertex(vh) == 1)
					leafVertexSet.erase(vh);
			}

			edge->setExists(true);

			// if any leaf vertices are created, add to set
			for (i = 0; i < 2; i++)
			{
				Vertex vh = edge->vertex(i);

				if (degreeOfVertex(vh) == 1)
					leafVertexSet.insert(vh);
			}
		}
	}
}

/*
 * return opposite halfedge for edge in DT
 */
DEdge oppositeDEdge(DEdge eh)
{
	Face fh = eh.first;
	int index = eh.second;
	Face oppFH = fh->neighbor(index);
	int oppIndex = oppFH->index(fh);

	return DEdge(oppFH, oppIndex);
}

/*
 * determines if a vertex in the enclosing boundary is conforming
 * condition: has at most 1 umbrella in exterior shell (umbrella = edge-pair bounding its contiguous space)
 */
bool determineVertexConforming(Vertex vh, int exteriorLabel, list<Face> &exteriorFaces)
{
	int exteriorUmbrellaCount = 0;
	Delaunay::Edge_circulator startEC = vh->incident_edges();

	// go to first existing edge
	while (!edgeForHE(*startEC)->exists())
		startEC++;

	Delaunay::Edge_circulator currEC = startEC;
	bool inExterior = false;

	do
	{
		DEdge oppHE = oppositeDEdge(*currEC);

		if (edgeForHE(oppHE)->exists())
		{
			// for every existing edge, check if it bounds the exterior shell space
			if (oppHE.first->info().label() == exteriorLabel)
			{
				exteriorUmbrellaCount++;	// if so, it is an umbrella in the exterior shell
				inExterior = true;	// beginning of exterior shell space
			}
			else
				inExterior = false;	// end of exterior shell space
		}

		// add faces in exterior shell space to list
		if (inExterior)
			exteriorFaces.push_back(oppHE.first);

		currEC++;
	} while (currEC != startEC);

	return (exteriorUmbrellaCount <= 1);
}

/*
 * determines delta rho in the enclosing boundary for adding that triangle
 */
float determineDeltaRho(Face currFH, int exteriorLabel)
{
	int i, j;
	float deltaRho = 0.0;

	// subtract existing edges, add new edges
	for (i = 0; i < 3; i++)
	{
		Point p[2];

		for (j = 0; j < 2; j++)
			p[j] = currFH->vertex((i + 1 + j) % 3)->point();

		float distance = sqrt(SQR(p[1].x() - p[0].x()) + SQR(p[1].y() - p[0].y()));

		if (currFH->info().edge(i)->exists())
			deltaRho -= distance;	// subtract existing edge
		else
			deltaRho += distance;	// add new edge
	}

	return deltaRho;
}

/*
 * add triangles to enclosing boundary so that it becomes a manifold (vertices can become interior)
 */
void inflate(Delaunay &dt)
{
	int i, j, nonConformingCount = 0, vertexCount = dt.number_of_vertices();
	bool isVertexConforming[vertexCount];

	// create priority queue of candidate triangles, sorted by delta rho
	multimap<float, Face> faceMMap;	// to keep candidates sorted by their non-unique value
	map<Face, float> faceReverseMap;	// to look up float value for candidate

	// classify all vertices
	for (Delaunay::Finite_vertices_iterator iter = dt.finite_vertices_begin(); iter != dt.finite_vertices_end(); iter++)
	{
		Vertex currVH = iter;
		int vIndex = currVH->info();
		list<Face> exteriorFaces;

		isVertexConforming[vIndex] = determineVertexConforming(currVH, FACE_MARKED, exteriorFaces);

		if (!isVertexConforming[vIndex])
		{
			// insert all exterior faces into priority queue
			for (list<Face>::iterator iter = exteriorFaces.begin(); iter != exteriorFaces.end(); iter++)
			{
				Face currFH = *iter;

				// only finite faces can be candidates
				if (!dt.is_infinite(currFH))
				{
					// calculate delta rho for candidate
					float deltaRho = determineDeltaRho(currFH, FACE_MARKED);
//						float deltaRho = determineDeltaRho(DEdge(currFH, currFH->index(currVH)));

					// insert candidate into priority queue (outside halfedge, ccw)
					pair<map<Face, float>::iterator, bool> result = faceReverseMap.insert(pair<Face, float>(currFH, deltaRho));

					if (result.second)	// only insert into multimap if not inserted already
						faceMMap.insert(pair<float, Face>(deltaRho, currFH));
				}
			}

			nonConformingCount++;
		}
	}

	// while candidate triangles left
	while (faceMMap.size() > 0)
	{
		// remove first element
		multimap<float, Face>::iterator iter = faceMMap.begin();
		Face currFH = iter->second;
		faceMMap.erase(iter);
		faceReverseMap.erase(currFH);

		// set new label (proper shell space) to triangle
		currFH->info().setLabel(FACE_UNMARKED);

		// set all its edges
		for (i = 0; i < 3; i++)
			currFH->info().edge(i)->setExists(true);

		// re-classify vertices of added triangle
		for (i = 0; i < 3; i++)
		{
			list<Face> exteriorFaces;
			Vertex currVH = currFH->vertex(i);
			isVertexConforming[currVH->info()] = determineVertexConforming(currVH, FACE_MARKED, exteriorFaces);
		}

		// collect all triangles incident to the vertices of the added triangle and remove them from candidates, then re-evaluate them and add if appropriate (with updated value)
		for (i = 0; i < 3; i++)
		{
			Vertex currVH = currFH->vertex(i);
			Delaunay::Face_circulator startFC = currVH->incident_faces();
			Delaunay::Face_circulator currFC = startFC;

			do
			{
				if (!dt.is_infinite(currFC))
				{
					// locate candidate in priority queue and remove if it exists
					map<Face, float>::iterator iter = faceReverseMap.find(currFC);

					if (iter != faceReverseMap.end())
					{
						faceReverseMap.erase(iter);

						// also locate and remove corresponding item in edgeMMap
						multimap<float, Face>::iterator iter2 = faceMMap.find(iter->second);

						while (iter2->second != currFC)
							iter2++;

						faceMMap.erase(iter2);
					}

					// all incident exterior triangles are potential candidates ...
					if (currFC->info().label() == FACE_MARKED)
					{
						// ... if they are incident to a non-conforming vertex
						j = 0;

						while ((j < 3) && isVertexConforming[currFC->vertex(j)->info()])
							j++;

						if (j != 3)
						{
							// calculate delta rho for candidate
							float deltaRho = determineDeltaRho(currFC, FACE_MARKED);
//								float deltaRho = determineDeltaRho(DEdge(currFC, currFC->index(currVH)));

							// insert candidate into priority queue (outside halfedge, ccw)
							pair<map<Face, float>::iterator, bool> result = faceReverseMap.insert(pair<Face, float>(currFC, deltaRho));

							if (result.second)	// only insert into multimap if not inserted already
								faceMMap.insert(pair<float, Face>(deltaRho, currFC));
						}
					}
				}

				currFC++;
			} while (currFC != startFC);
		}
	}
}

/*
 * returns if edge is in enclosing boundary
 * this is the case if the edge exists and at least one of its adjoining triangles is contained in the enclosing boundary
 */
bool isInEnclosingBoundary(DEdge he, int exteriorLabel)
{
	int i;
	Face fh[2];

	fh[0] = he.first;
	fh[1] = fh[0]->neighbor(he.second);

	if (fh[0]->info().edge(he.second)->exists())
	{
		for (i = 0; i < 2; i++)
			if (fh[i]->info().label() == exteriorLabel)
				return true;
	}

	return false;
}

/*
 * remove all edges not on the enclosing boundary
 * precondition: boundary is manifold and enclosing, so there are no edges outside it
 */
void removeInsideEdges(Delaunay &dt)
{
	// test all finite edges
	for (Delaunay::Finite_edges_iterator iter = dt.finite_edges_begin(); iter != dt.finite_edges_end(); iter++)
	{
		DEdge he = *iter;
		NEdge *edge = edgeForHE(he);

		// handle only existing edges
		if (edge->exists())
		{
			// if it is not in enclosing boundary, remove
			if (!isInEnclosingBoundary(he, FACE_MARKED))
				edge->setExists(false);
		}
	}
}

/*
 * return delta rho for a candidate, given by its face and its interior vertex index
 */
float determineDeltaRho(DEdge eh)
{
	int i, j;
	float deltaRho = 0.0;

	for (i = 0; i < 3; i++)
	{
		Point p[2];

		for (j = 0; j < 2; j++)
			p[j] = eh.first->vertex((eh.second + i + j) % 3)->point();

		float distance = sqrt(SQR(p[1].x() - p[0].x()) + SQR(p[1].y() - p[0].y()));

		// subtract boundary edge, add edges incident to interior vertex
		if (i == 1)
			distance = -distance;

		deltaRho += distance;
	}

	return deltaRho;
}

/*
 * sculptures from current boundary in DT
 * precondition: current boundary encloses all (finite) vertices in DT on or inside it, and is manifold
 */
void sculpture(Delaunay &dt)
{
	int i, vertexCount = dt.number_of_vertices();
	bool isVertexInterior[vertexCount];

	// initialize all vertices as interior
	for (i = 0; i < vertexCount; i++)
		isVertexInterior[i] = true;

	int interiorVertexCount = vertexCount;

	// get a convex hull vertex
	Vertex infVH = dt.infinite_vertex();
	Face infFH = infVH->incident_faces();
	Vertex chVH = infFH->vertex(infFH->cw(infFH->index(infVH)));

	// find existing edge incident to a convex hull vertex, starting from its edge to the infinite vertex
	Delaunay::Edge_circulator startEC = chVH->incident_edges(infFH);
	Delaunay::Edge_circulator currEC = startEC;

	while (!edgeForHE(*currEC)->exists())
		currEC++;

	DEdge startHE = oppositeDEdge(*currEC);	// get inside halfedge
	DEdge currHE = startHE;

	// traverse enclosing boundary (existing edges) in cw orientation (since infinite) and label vertices if interior
	do
	{
		// rotate edge ccw about vertex of currHE
		Face currFH = currHE.first;
		Vertex currVH = currFH->vertex(currFH->cw(currHE.second));
		currEC = currVH->incident_edges(currFH);

		do
		{
			currEC++;
		} while (!edgeForHE(*currEC)->exists());

		currHE = oppositeDEdge(*currEC);	// to advance to next vertex

		if (isVertexInterior[currVH->info()])
		{
			// mark vertex as non-interior
			isVertexInterior[currVH->info()] = false;

			// keep track of count of interior vertices
			interiorVertexCount--;
		}
	} while (currHE != startHE);

	// create priority queue of candidate edges (in a triangle with interior vertex), sorted by delta rho
	multimap<float, DEdge> edgeMMap;	// to keep candidates sorted by their non-unique value
	map<DEdge, float> edgeReverseMap;	// to look up float value for candidate

	// traverse enclosing boundary again
	do
	{
		// rotate edge ccw about vertex of currHE
		Face currFH = currHE.first;
		Vertex currVH = currFH->vertex(currFH->cw(currHE.second));
		currEC = currVH->incident_edges(currFH);

		do
		{
			currEC++;
		} while (!edgeForHE(*currEC)->exists());

		currHE = oppositeDEdge(*currEC);	// to advance to next vertex
		currFH = currHE.first;
		int index = currHE.second;

		if (isVertexInterior[currFH->vertex(index)->info()])
		{
			// calculate delta rho for candidate
			float deltaRho = determineDeltaRho(currHE);

			// insert candidate into priority queue (outside halfedge, ccw)
			DEdge oppHE = oppositeDEdge(currHE);
			edgeMMap.insert(pair<float, DEdge>(deltaRho, oppHE));
			edgeReverseMap.insert(pair<DEdge, float>(oppHE, deltaRho));
		}
	} while (currHE != startHE);

	// while priority queue not empty, sculpture its candidates triangles from enclosing boundary
	while (edgeMMap.size() > 0)
	{
		// remove first element
		multimap<float, DEdge>::iterator iter = edgeMMap.begin();
		DEdge currHE = iter->second;
		edgeMMap.erase(iter);
		edgeReverseMap.erase(currHE);

		// update triangle, edges and label exposed vertex as non-interior, keep track of their number
		DEdge oppHE = oppositeDEdge(currHE);
		Face oppFH = oppHE.first;
		int oppIndex = oppHE.second;
		oppFH->info().setLabel(FACE_MARKED);

		for (i = 0; i < 3; i++)
		{
			NEdge *edge = oppFH->info().edge(i);
			edge->setExists(i != oppIndex);	// remove previous enclosing boundary edge, add edges incident to exposed vertex
		}

		Vertex exposedVH = oppFH->vertex(oppIndex);
		isVertexInterior[exposedVH->info()] = false;
		interiorVertexCount--;

		// collect all faces incident to the exposed vertex to remove them as candidates
		Delaunay::Face_circulator startFC = exposedVH->incident_faces();
		Delaunay::Face_circulator currFC = startFC;

		do
		{
			// determine candidate edge
			DEdge currHE(currFC, currFC->index(exposedVH));
			DEdge oppHE = oppositeDEdge(currHE);	// from outside, ccw

			// if edge exists, it is a candidate
			if (edgeForHE(oppHE)->exists())
			{
				// locate entry in priority queue and remove
				map<DEdge, float>::iterator iter = edgeReverseMap.find(oppHE);

				// an existing edge may not be a candidate
				if (iter != edgeReverseMap.end())
				{
					edgeReverseMap.erase(iter);

					// also locate and remove corresponding item in edgeMMap
					multimap<float, DEdge>::iterator iter2 = edgeMMap.find(iter->second);

					while (iter2->second != oppHE)
						iter2++;

					edgeMMap.erase(iter2);
				}
			}

			currFC++;
		} while (currFC != startFC);

		// determine for the two new edges if they are candidates -> add to priority queue
		for (i = 0; i < 2; i++)
		{
			Face newFH = oppFH->neighbor((oppIndex + 1 + i) % 3);
			DEdge newHE(newFH, newFH->index(oppFH));
			DEdge oppHE = oppositeDEdge(newHE);

			if (isVertexInterior[newFH->vertex(newFH->index(oppFH))->info()])
			{
				// calculate delta rho for candidate
				float deltaRho = determineDeltaRho(newHE);

				// insert candidate into priority queue
				edgeMMap.insert(pair<float, DEdge>(deltaRho, oppHE));
				edgeReverseMap.insert(pair<DEdge, float>(oppHE, deltaRho));
			}
		}
	}
}

/*
 * locate enclosing boundary
 */
void locateEnclosingBoundary(Delaunay &dt)
{
	int i;
	set<Face> faceSet;

	// initialize all triangles in DT with label -1, put all infinite triangles into set as removal candidates
	for (Delaunay::All_faces_iterator iter = dt.all_faces_begin(); iter != dt.all_faces_end(); iter++)
	{
		Face face = iter;
		face->info().setLabel(FACE_UNMARKED);

		if (dt.is_infinite(face))
			faceSet.insert(face);
	}

	// while candidates left, remove them from boundary and update
	while (faceSet.size() > 0)
	{
		// remove a triangle
		set<Face>::iterator iter = faceSet.begin();
		Face currFH = *iter;
		faceSet.erase(iter);

		// only traverse if yet unmarked
		if (currFH->info().label() == FACE_UNMARKED)
		{
			currFH->info().setLabel(FACE_MARKED);

			for (i = 0; i < 3; i++)
			{
				DEdge currHE(currFH, i);
				NEdge *edge = edgeForHE(currHE);

				// test if edge exists to neighbor triangle
				if (!edge->exists())
				{
					// if not, add to set if not already traversed
					Face oppFH = currFH->neighbor(i);

					if (oppFH->info().label() == FACE_UNMARKED)
						faceSet.insert(oppFH);
				}
			}
		}
	}
}

void collectBoundaryIndices(Delaunay &dt, vector<int> &boundaryIndices)
{
	int i = 0;

	// allocate space for all vertices in vector
	boundaryIndices.resize(dt.number_of_vertices());

	// get a convex hull vertex
	Vertex infVH = dt.infinite_vertex();
	Face infFH = infVH->incident_faces();
	Vertex chVH = infFH->vertex(infFH->cw(infFH->index(infVH)));

	// find existing edge incident to a convex hull vertex, starting from its edge to the infinite vertex
	Delaunay::Edge_circulator startEC = chVH->incident_edges(infFH);
	Delaunay::Edge_circulator currEC = startEC;

	while (!edgeForHE(*currEC)->exists())
		currEC++;

	DEdge startHE = oppositeDEdge(*currEC);	// get inside halfedge
	DEdge currHE = startHE;

	// traverse enclosing boundary (existing edges) in cw orientation
	do
	{
		// rotate edge ccw about vertex of currHE
		Face currFH = currHE.first;
		boundaryIndices[i++] = currFH->vertex(currFH->ccw(currHE.second))->info();
		Vertex currVH = currFH->vertex(currFH->cw(currHE.second));
		currEC = currVH->incident_edges(currFH);

		do
		{
			currEC++;
		} while (!edgeForHE(*currEC)->exists());

		currHE = oppositeDEdge(*currEC);	// to advance to next vertex
	} while (currHE != startHE);

	// reduce vector size as needed (if not all points interpolated)
	boundaryIndices.resize(i);
}

/*
 * construct boundary from vector of input points
 */
Connect2D::Connect2D(vector<pair<double, double> > *pointVec)
{
	// construct delaunay triangulation from points
	constructDT(dt, pointVec);

	// initialize edge and triangle references
	createComplementaryDataStructure(dt, nEdges);

	// construct the rho-complex from the DT
	constructRhoComplex(dt, nEdges);

	// locat the enclosing boundary of the rho-complex
	locateEnclosingBoundary(dt);

	// inflate that boundary so that it becomes manifold
	inflate(dt);

	// remove edges not in the enclosing boundary from the graph
	removeInsideEdges(dt);

	// sculpture boundary to expose interior vertices onto it
	sculpture(dt);

	// traverse boundary to return its indices in order
	collectBoundaryIndices(dt, _boundary);
}

/*
 * return reference to constructed boundary
 */
vector<int> *Connect2D::getBoundary()
{
	return &_boundary;
}

}
