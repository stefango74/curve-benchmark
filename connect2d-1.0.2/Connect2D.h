/*
 * Connect2D.h
 *
 *  Created on: 2011-05-17
 *      Author: Stefan Ohrhallinger
 */

#ifndef CONNECT2D_H_
#define CONNECT2D_H_

#include <iostream>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_hierarchy_2.h>

namespace connect2d
{

struct NEdge;

class NTriangle
{
private:
	int _label;
	NEdge *_edge[3];

public:
	inline NTriangle()
	{
		_label = -1;

		for (int i = 0; i < 3; i++)
			_edge[i] = NULL;
	}

	inline void setLabel(int label)
	{
		_label = label;
	}

	inline int label()
	{
		return _label;
	}

	inline void setEdge(int index, NEdge *edge)
	{
		_edge[index] = edge;
	}

	inline NEdge *edge(int index)
	{
		return _edge[index];
	}
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_face_base_with_info_2<NTriangle, K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Delaunay;
typedef K::Point_2 Point;
typedef Delaunay::Vertex_handle Vertex;
typedef Delaunay::Face_handle Face;
typedef Delaunay::Edge DEdge;

using namespace std;

class NEdge
{
private:
	bool _exists;
	Vertex _vertex[2];

public:
	NEdge(Vertex v0, Vertex v1)
	{
		_exists = false;
		_vertex[0] = v0;
		_vertex[1] = v1;
	}

	inline void setExists(bool exists)
	{
		_exists = exists;
	}

	inline const bool exists()
	{
		return _exists;
	}

	inline void setVertex(int index, Vertex vertex)
	{
		_vertex[index] = vertex;
	}

	inline const Vertex vertex(int index)
	{
		return _vertex[index];
	}

	/*
	 * output edge as v0-v1
	 */
	friend ostream& operator<<(ostream& os, const NEdge &edge)
	{
		os << edge._vertex[0]->info() << "-" << edge._vertex[1]->info();

		return os;
	}
};

class Connect2D
{
private:
	Delaunay dt;
	list<NEdge> nEdges;
	vector<int> _boundary;

public:
	Connect2D(vector<pair<double, double> > *pointVec);
	vector<int> *getBoundary();
};

}

#endif /* CONNECT2D_H_ */
