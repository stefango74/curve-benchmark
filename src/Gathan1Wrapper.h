#ifndef GATHAN1WRAPPER_H_
#define GATHAN1WRAPPER_H_

#include "../crust-family/recon2d.h"
#include "CurveBenchmarkDataTypes.h"

class Gathan1Wrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges, float max_pv_angle, float min_vd_ratio);
};

#endif /* GATHAN1WRAPPER_H_ */
