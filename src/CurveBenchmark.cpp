//============================================================================
// Name        : CurveBenchmark.cpp
// Author      : 
// Version     :
// Copyright   : (C) Stefan Ohrhallinger, LGPL v3
// Description : Benchmark for evaluation of curve reconstruction
//============================================================================

#include <argtable2.h>
#include <iostream>
#include <list>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>

//boost headers
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/multi_polygon.hpp>
#include <boost/foreach.hpp>

#include "CurveBenchmarkDataTypes.h"
#include "DisjointSets.h"
#include "../stretchdenoise/src/PrecTimer.h"

// method wrapper includes:
#include "LenzWrapper.h"
#include "Connect2DWrapper.h"
#include "HnnCrustWrapper.h"
#include "FitconnectWrapper.h"
#include "StretchdenoiseWrapper.h"
#include "OptimalTransportWrapper.h"
//#include "VoronoiCurveWrapper.h"
//#include "ShapeHull2DWrapper.h"
#include "DISCURWrapper.h"
#include "VICURWrapper.h"
#include "CrawlWrapper.h"
#include "PeelWrapper.h"
#include "CrustWrapper.h"
#include "NNCrustWrapper.h"
#include "CCrustWrapper.h"
#include "Gathan1Wrapper.h"
#include "GathanGWrapper.h"

#define FREE_ARGS arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
#define MACRO_ERROR_EXIT FREE_ARGS exit(1);
#define MACRO_OK_EXIT FREE_ARGS exit(0);

using namespace std;
using namespace casa;

template<typename T> inline T CUB(T t) { return (SQR(t)*(t)); };

static double DEFAULT_SPACING=0.001;
int next_edge_index = 0;
double EPSILON=0.00001;

/*
 * read point coordinates from file
 */
bool readPointsFile(string filename, vector<CBPoint> &points)
{
	points.clear();
	ifstream file(filename);

	if (file)
	{
		while (file)
		{
			CBPoint p;
			file >> p.x >> p.y;

			if (file)
				points.push_back(p);
		}

		return true;
	}
	else
		return false;
}

/*
 * write edges (as pairs of point indices) to file
 */
void writeEdgeFile(string filename, list<CBEdge> &edges)
{
	ofstream file;
	file.open(filename);



	for (auto edge:edges)
		file << edge.v[0] << " " << edge.v[1] << endl;


	file.close();
}


unsigned int getIndex(CBPoint p, vector<CBPoint> &points){
CBPoint p1;
for(unsigned int i=0; i<points.size(); ++i){
	 p1=points[i];
	 if((fabs(p1.x-p.x)<EPSILON) && (fabs(p1.y-p.y)<EPSILON))
		 return i;
}

return -1;
}

/*
 * read edges (as pairs of point indices) from file
 */
bool readEdgeFile(string filename, list<CBEdge> &edges, vector<CBPoint> &points)
{
	edges.clear();
	ifstream file(filename);

	if (file)
	{
if(filename.substr(filename.find_last_of(".") + 1) == "edg"){
		while (file)
		{
			CBEdge edge;
			file >> edge.v[0] >> edge.v[1];

			if (file)
				edges.push_back(edge);
		}
}
else{
int first=1;
CBPoint p0, p1, p2;
CBEdge edge;
while (file)
		{			
			 
			if (first){
			file >> p1.x >> p1.y>> p2.x >> p2.y;
			p0=p1;
			first=0;
			}
			else{
			p1=p2;
			file>>p2.x >> p2.y;
			}
			edge.v[0]=getIndex(p1, points);
			edge.v[1]=getIndex(p2, points);

			if (file)
				edges.push_back(edge);
		}
		
		edge.v[0]=getIndex(p2, points);
		edge.v[1]=getIndex(p0, points);
		edges.push_back(edge);
}
//writeEdgeFile(filename+"1.edg", edges);
		return true;
	}
	else
		return false;
}


/*
 * write SVG file with points and edges
 */
void writeSVGFile(string filename, vector<CBPoint> &points, list<CBEdge> &edges)
{
	int i;

	// constants to control SVG output can be changed here:
	const int SVG_PIXEL_DIM = 1000;	// x and y resolution on screen
	const float SVG_LINE_WIDTH = 1.0;	// stroke size of boundary edges
	const float SVG_DOT_RADIUS = 4.0;	// radius of dots

	string svgHeaderPart1="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
		"<!-- Created with CurveBenchmark -->\n"
		"<svg\n"
		"	xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
		"	xmlns:cc=\"http://creativecommons.org/ns#\"\n"
		"	xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
		"	xmlns:svg=\"http://www.w3.org/2000/svg\"\n"
		"	xmlns=\"http://www.w3.org/2000/svg\"\n"
		"	xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n"
		"	xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n"
		"	width=\"";
	string svgHeaderPart2="\"\n"
		"	height=\"";
	string svgHeaderPart3="\"\n"
		"	id=\"svg2\"\n"
		"	version=\"1.1\"\n"
//		"	inkscape:version=\"0.48.1 r9760\"\n"
		"	sodipodi:docname=\"";
	string svgHeaderPart4 = "\">\n"
		"<defs\n"
		"	id=\"defs4\" />\n"
		"<metadata\n"
		"	id=\"metadata7\">\n"
		"	<rdf:RDF>\n"
		"		<cc:Work\n"
		"			rdf:about=\"\">\n"
		"			<dc:format>image/svg+xml</dc:format>\n"
		"			<dc:type\n"
		"				rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n"
		"			<dc:title></dc:title>\n"
		"		</cc:Work>\n"
		"	</rdf:RDF>\n"
		"</metadata>\n"
		"<g\n"
		"	inkscape:label=\"Layer 1\"\n"
		"	inkscape:groupmode=\"layer\"\n"
		"	id=\"layer1\">\n";
	string svgFooter = "	</g>\n"
		"</svg>";

	// determine maximum extension in either dimension
	double min[2] = { points[0].x, points[0].y };
	double max[2] = { points[0].x, points[0].y };

	for (i = 0; i < (int)points.size(); i++)
	{
		if (points[i].x < min[0])
			min[0] = points[i].x;

		if (points[i].x > max[0])
			max[0] = points[i].x;

		if (points[i].y < min[1])
			min[1] = points[i].y;

		if (points[i].y > max[1])
			max[1] = points[i].y;
	}

	double dim[2] = { max[0] - min[0], max[1] - min[1] };
	double maxDim = (dim[0] > dim[1]) ? dim[0] : dim[1];
	double factor = (SVG_PIXEL_DIM - 2*SVG_DOT_RADIUS)/maxDim;

	ofstream svgFile;
	svgFile.open(filename.data());
	svgFile << svgHeaderPart1 << SVG_PIXEL_DIM << svgHeaderPart2 << SVG_PIXEL_DIM << svgHeaderPart3 << filename.substr(0, filename.length() - 4) << svgHeaderPart4;

	// output boundary as list of edge paths

	for (auto edge:edges)
	{
		svgFile << "	<path style=\"fill:none;stroke:red;stroke-width:" << SVG_LINE_WIDTH << "px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" d=\"M";
		for (i = 0; i < 2; i++)
			svgFile << " " << ((points[edge.v[i]].x - min[0])*factor + SVG_DOT_RADIUS) << "," << ((points[edge.v[i]].y- min[1])*factor + SVG_DOT_RADIUS);

		svgFile << " z\" id=\"boundary\" inkscape:connector-curvature=\"0\" />\n";
	}

	// output dots for vertices
	for (i = 0; i < (int)points.size(); i++)
		svgFile << "	<circle cx=\"" << ((points[i].x - min[0])*factor + SVG_DOT_RADIUS) << "\" cy=\""  << ((points[i].y - min[1])*factor + SVG_DOT_RADIUS) << "\" r=\"" << SVG_DOT_RADIUS << "\" fill=\"#0\" id=\"circle" << i << "\"/>\n";

	svgFile << svgFooter;
	svgFile.close();
}

void determineBoundingBox(vector<CBPoint> &inPoints, double *min, double *max)
{
	int i;

	for (i = 0; i < 2; i++)
	{
		min[i] = numeric_limits<double>::max();
		max[i] = -numeric_limits<double>::max();
	}

	// compute translation offsets
	for (auto p:inPoints)
	{
		for (i = 0; i < 2; i++)
		{
			double c = (i == 0) ? p.x : p.y;

			if (c < min[i])
				min[i] = c;

			if (c > max[i])
				max[i] = c;
		}
	}
}

/*
 * transform (scale + translation) the point set into the unit square
 */
void determineUnitSquareTransform(vector<CBPoint> &inPoints, double &tx, double &ty, double &s)
{
	double min[2], max[2];
	determineBoundingBox(inPoints, min, max);

	// compute scale factor
	double size[2] = { max[0] - min[0], max[1] - min[1] };
	tx = -min[0];
	ty = -min[1];

	double scale = size[0];

	if (size[1] > scale)
		scale = size[1];

	s = 1.0/scale;
}

/*
 * transform (scale + translation) the point set into the unit square
 */
void transformIntoUnitSquare(vector<CBPoint> &inPoints, vector<CBPoint> &unitPoints, double tx, double ty, double s)
{
	int i;

	// transform into unit square
	for (i = 0; i < (int)inPoints.size(); i++)
	{
		unitPoints[i].x = (inPoints[i].x + tx)*s;
		unitPoints[i].y = (inPoints[i].y + ty)*s;
	}
}

/*
 * sample edges with given spacing, centrally distributed on the edges
 */
void sampleEdges(vector<CBPoint> &points, list<CBEdge> &edges, double spacing, vector<CBPoint> &samples, vector<CBPoint> &normals)
{
	int i;
/*
	// start with original points
	samples.resize(points.size());

	for (i = 0; i < (int)points.size(); i++)
		samples[i] = points[i];
*/
	// sample on edges between points
	for (auto edge:edges)
	{
		CBPoint p[2] = { points[edge.v[0]], points[edge.v[1]] }, vec, unitVec;
		vec.x = p[1].x - p[0].x;
		vec.y = p[1].y - p[0].y;
		double edgeLen = sqrt(vec.x*vec.x + vec.y*vec.y);
		unitVec.x = vec.x/edgeLen;
		unitVec.y = vec.y/edgeLen;
		CBPoint n = { -unitVec.y, unitVec.x };
		int count = (int)(edgeLen/spacing);
		double offset = 0.5*(edgeLen - count*spacing);
		CBPoint s;

		for (i = 0; i < count + 1; i++)
		{
			double t = (offset + i*spacing);
			s.x = p[0].x + t*unitVec.x;
			s.y = p[0].y + t*unitVec.y;
			samples.push_back(s);
			normals.push_back(n);
		}
	}
}

/*
 * determine nearest point inside any edge to pp, return distance and containing edge
 */
double nearestPointInEdges(CBPoint pp, vector<CBPoint> &points, list<CBEdge> &edges, CBEdge &contEdge)
{
	int i;
	double minDist = numeric_limits<double>::max(), dist;
	Point p(pp.x, pp.y);

	for (auto edge:edges)
	{
		Point v[2];

		for (i = 0; i < 2; i++)
		{
			CBPoint ep = points[edge.v[i]];
			v[i] = Point(ep.x, ep.y);
		}

		Point edgeVec = v[1] - v[0];
		Point pVec = p - v[0];
		double dot = pVec.dot(edgeVec);
		double endDot = edgeVec.dot(edgeVec);

		if ((dot >= 0.0) && (dot <= endDot))
		{
			// nearest point (orthogonal projection) is inside edge
			Point n(-edgeVec[1], edgeVec[0]);
			n.normalize();
			dist = abs(pVec.dot(n));
		}
		else
		{
			// determine nearer of the edge vertices
			dist = p.distance(v[0]);
			double dist2 = p.distance(v[1]);

			if (dist2 < dist)
				dist = dist2;
		}

		if (dist < minDist)
		{
			minDist = dist;
			contEdge = edge;
		}
	}

	return minDist;
}

/*
 * return unit normal vector for edge
 */
CBPoint getUnitNormal(CBEdge &edge, vector<CBPoint> &points)
{
	CBPoint p[2] = { points[edge.v[0]], points[edge.v[1]] }, vec, unitVec;
	vec.x = p[1].x - p[0].x;
	vec.y = p[1].y - p[0].y;
	double edgeLen = sqrt(vec.x*vec.x + vec.y*vec.y);
	unitVec.x = vec.x/edgeLen;
	unitVec.y = vec.y/edgeLen;
	CBPoint n = { -unitVec.y, unitVec.x };

	return n;
}


//sort the edges--->for symmetric difference
vector<CBPoint> point_array;

int * newIntRaw(int n)
{
	return (int *)malloc(sizeof(int) * n);
}
int * newInt(int n, int init)
{
	int *p = newIntRaw(n);
	int i;
	for (i = 0; i < n; ++i)
		p[i] = init;
	return p;
}
int ** newMap(int n, int m, int init)
{
	int **res = (int **)malloc(sizeof(int *) * n);
	int i;
	for (i = 0; i < n; ++i)
		res[i] = newInt(m, init);
	return res;
}


typedef struct
{
	int e;
	int n;
	int **map;
} Graph;

Graph * newGraph(vector<CBPoint> &inPoints, list<CBEdge> &origiEdges)
{
	int n, e;
	int i;
	int from, to;
	Graph *g = (Graph *)malloc(sizeof(Graph));
	
	
	// then change list to vector
	std::vector<CBEdge> edge_vector;
	edge_vector.reserve(origiEdges.size());
	std::copy(std::begin(origiEdges), std::end(origiEdges), std::back_inserter(edge_vector));
	
	n = (int)inPoints.size();
	e = (int)edge_vector.size();
	g->n = n;
	g->e = e;
	
	g->map = newMap(n, n, 0);
	for (i = 0; i < e; ++i) {
		from = edge_vector[i].v[0];
		to = edge_vector[i].v[1];
		g->map[from][to] = 1;
		g->map[to][from] = 1;
	}
	return g;
}

int *visited;
int *father;

void dfs(Graph *g, int s , vector<CBPoint> &inPoints)
{
	int i;
	visited[s] = 1;
	for (i = 0; i < g->n; ++i) {
		if (g->map[s][i] == 1) {
			if (visited[i] == 0) {
				father[i] = s;
				dfs(g, i , inPoints);
			}
			else if (visited[i] == 1 && i != father[s]) {
				int tmp = s;
				
				point_array.push_back(inPoints[i]);
				//                cout << "********************************************\n" << endl;
				//                printf("Sorted counter clock wise edges order\n\n");
				//                cout << "********************************************\n" << endl;
				//                printf("%d --> ", i);
				while (tmp != i) {
					//                    printf("%d --> ", tmp);
					point_array.push_back(inPoints[tmp]);
					tmp = father[tmp];
				}
				//                printf("%d\n", tmp);
				point_array.push_back(inPoints[tmp]);
				
			}
		}
	}
	visited[s] = 2;
}
void findRing(Graph *g , vector<CBPoint> &inPoints)
{
	int i;
	visited = newInt(g->n, 0);
	father = newInt(g->n, -1);
	for (i = 0; i < g->n; ++i)
		if (visited[i] == 0)
			dfs(g, i , inPoints);
}

string sort_edges_and_get_boost_input_string(vector<CBPoint> &inPoints, list<CBEdge> &origiEdge){
	
	string str;
	Graph *g = newGraph(inPoints,origiEdge);
	findRing(g , inPoints);
	
	
	str.append("POLYGON((");
	
	for (int i = (int)point_array.size() - 1 ; i >= 0 ; i--) {
		str.append(to_string(point_array[i].x));
		str.append("  ");
		str.append(to_string(point_array[i].y));
		if (i != 0 ){
			str.append(" , ");
		}
	}
	str.append("))");
	point_array.clear();
	return str;
}





//Symmetric difference
double display_symdifference(string output_image_path , string first_sym_difference_input, string second_sym_difference_input)
{

	typedef boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > polygon;
	typedef boost::geometry::model::d2::point_xy<double> point_type;
	
	polygon green, blue;
	
	boost::geometry::read_wkt(
							  first_sym_difference_input, green);
	
	boost::geometry::read_wkt(
							  second_sym_difference_input, blue);
	
	std::ofstream svg(output_image_path);
	
	boost::geometry::svg_mapper<point_type> mapper(svg, 400, 400);
	
	boost::geometry::model::multi_polygon<polygon> multi;
	
	
	boost::geometry::sym_difference(green, blue, multi);
	
	// Add geometries such that all these geometries fit on the map
	mapper.add(green);
	mapper.add(blue);
	mapper.add(multi);
	
	// Draw the geometries on the SVG map, using a specific SVG style
	mapper.map(green, "fill-opacity:0.3;fill:rgb(0 ,255, 0 );stroke:rgb(0,0,0);stroke-width:2");
	mapper.map(blue, "fill-opacity:0.3;fill:rgb(0 ,0 ,255);stroke:rgb(0,0,0);stroke-width:2");
	mapper.map(multi, "fill-opacity:0.3;fill:rgb(255,0,0);stroke:rgb(0,0,0);stroke-width:2");
/*	
	std::cout
	<< "green XOR blue:" << std::endl
	<< "total: " << boost::geometry::area(multi) << std::endl;
*/
return boost::geometry::area(multi);
}

double boost_sym_difference(vector<CBPoint> &first_inPoints, list<CBEdge> &first_origEdges ,vector<CBPoint> &second_inPoints, list<CBEdge> &second_origEdges ){
	
	string first_boost_sym_difference_input ;
	string second_boost_sym_difference_input ;
	
	
	first_boost_sym_difference_input = sort_edges_and_get_boost_input_string(first_inPoints, first_origEdges);
	//    cout << "********************************************\n" << endl;
	//    cout << "First polygon's boost input sentence : \n" << endl;
	//    cout << "********************************************\n" << endl;
	//    cout << first_boost_sym_difference_input << endl;
	
	second_boost_sym_difference_input = sort_edges_and_get_boost_input_string(second_inPoints, second_origEdges);
	
	//    cout << "********************************************\n" << endl;
	//    cout << "Second polygon's boost input sentence : \n" << endl;
	//    cout << "********************************************\n" << endl;
	//    cout << second_boost_sym_difference_input << endl;
	return (display_symdifference("mysvg.svg", first_boost_sym_difference_input, second_boost_sym_difference_input));
}


/*
 * evaluates the reconstructed curve with the ground truth by Hausdorff distance and exact coincidence of edges
 */
void evaluate(vector<CBPoint> &inPoints, vector<CBPoint> &outPoints, list<CBEdge> &inEdges, list<CBEdge> &outEdges, double sampleSpacing, Evaluation &eval)
{
	int i;
	// for each edge set, map edges by their vertices and sort their adjacent vertices

	map<int, set<int> > verticesMap[2];
	for (auto edge:inEdges)
	{
		for (i = 0; i < 2; i++)
		{
			int v = edge.v[i];
			verticesMap[0][v].insert(edge.v[1 - i]);
		}
	}

	for (auto edge:outEdges)
	{
		for (i = 0; i < 2; i++)
		{
			int v = edge.v[i];
			verticesMap[1][v].insert(edge.v[1 - i]);
		}
	}

	// now that the edge sets are sorted, we can compare them whether they are identical
	eval.isExact = (verticesMap[0].size() == verticesMap[1].size());
	map<int, set<int> >::iterator mapIter[2] = { verticesMap[0].begin(), verticesMap[1].begin() };

	while (eval.isExact && (mapIter[0] != verticesMap[0].end()))
	{
		eval.isExact = mapIter[0]->second.size() == mapIter[1]->second.size();
		set<int>::iterator setIter[2] = { mapIter[0]->second.begin(), mapIter[1]->second.begin() };

		while (eval.isExact && (setIter[0] != mapIter[0]->second.end()))
		{
			eval.isExact = (*setIter[0] == *setIter[1]);
			setIter[0]++;
			setIter[1]++;
		}

		mapIter[0]++;
		mapIter[1]++;
	}

	// compute Hausdorff distance from reconstruction edge set to original one (not bidirectional)

	// transform points into unit square
	vector<CBPoint> unitInPoints(inPoints.size()), unitOutPoints(outPoints.size());
	double tx, ty, s;
	determineUnitSquareTransform(inPoints, tx, ty, s);
	transformIntoUnitSquare(inPoints, unitInPoints, tx, ty, s);
	transformIntoUnitSquare(outPoints, unitOutPoints, tx, ty, s);

	// sample edges with fixed density
	vector<CBPoint> inSamples, outSamples, inNormals, outNormals;
	sampleEdges(unitInPoints, inEdges, sampleSpacing, inSamples, inNormals);
	sampleEdges((outPoints.size() == 0) ? unitInPoints : unitOutPoints, outEdges, sampleSpacing, outSamples, outNormals);

#ifdef OBSOLETE_MEASURE_BETWEEN_SAMPLES

	// collect nn and distances
	ANNkd_tree *kdTreeOrig = NULL, *kdTreeRecon = NULL;
	ANNpointArray ann_pointsOrig, ann_pointsRecon;

	ann_pointsOrig = annAllocPts(origSamples.size(), 2);

	for(i = 0; i < (int)origSamples.size(); i++)
	{
		auto p = ann_pointsOrig[i];
		p[0] = origSamples[i].x;
		p[1] = origSamples[i].y;
	}

	ann_pointsRecon = annAllocPts(reconSamples.size(), 2);

	for(i = 0; i < (int)reconSamples.size(); i++)
	{
		auto p = ann_pointsRecon[i];
		p[0] = reconSamples[i].x;
		p[1] = reconSamples[i].y;
	}

	kdTreeOrig = new ANNkd_tree(ann_pointsOrig, origSamples.size(), 2);
	kdTreeRecon = new ANNkd_tree(ann_pointsRecon, reconSamples.size(), 2);
	ANNpointArray search_point = annAllocPts(1, 2);
	ANNidxArray nnIdx = new ANNidx[1];
	ANNdistArray distances = new ANNdist[1];
	double rmseDist = 0.0, maxDist = 0.0, rmseAngle = 0.0, maxAngle = 0.0;

	for (i = 0; i < (int)reconSamples.size(); i++)
	{
		// determine nearest neighbor in origSamples for point in reconSamples
		search_point[0][0] = reconSamples[i].x;
		search_point[0][1] = reconSamples[i].y;
		kdTreeOrig->annkSearch(search_point[0], 1, nnIdx, distances);

		// distance to NN
		double dist = sqrt(distances[0]);
		rmseDist += dist*dist;

		if (dist > maxDist)
			maxDist = dist;

		// normal deviation to NN
		CBPoint reconN = reconNormals[i];
		CBPoint origN = origNormals[nnIdx[0]];
		double dot = reconN.x*origN.x + reconN.y*origN.y;

		// clamp to [-1.0,1.0] to avoid invalid acos result
		if (dot < -1.0)
			dot = -1.0;
		else
		if (dot > 1.0)
			dot = 1.0;

		double dev = acos(dot);
		rmseAngle += dev*dev;

		if (dev > maxAngle)
			maxAngle = dev;
	}

	for (i = 0; i < (int)origSamples.size(); i++)
	{
		// determine nearest neighbor in origSamples for point in reconSamples
		search_point[0][0] = origSamples[i].x;
		search_point[0][1] = origSamples[i].y;
		kdTreeRecon->annkSearch(search_point[0], 1, nnIdx, distances);

		// distance to NN
		double dist = sqrt(distances[0]);
		rmseDist += dist*dist;

		if (dist > maxDist)
			maxDist = dist;

		// normal deviation to NN
		CBPoint origN = origNormals[i];
		CBPoint reconN = reconNormals[nnIdx[0]];
		double dot = reconN.x*origN.x + reconN.y*origN.y;

		// clamp to [-1.0,1.0] to avoid invalid acos result
		if (dot < -1.0)
			dot = -1.0;
		else
		if (dot > 1.0)
			dot = 1.0;

		double dev = acos(dot);
		rmseAngle += dev*dev;

		if (dev > maxAngle)
			maxAngle = dev;
	}

	int totalSampleCount = origSamples.size() + reconSamples.size();
	rmseDist = sqrt(rmseDist/totalSampleCount);
	rmseAngle = sqrt(rmseAngle/totalSampleCount);
	eval.distMax = maxDist;
	eval.distRMSE = rmseDist;
	eval.angleMax = maxAngle;
	eval.angleRMSE = rmseAngle;

	delete nnIdx;
	delete distances;
	annDeallocPts(ann_pointsOrig);
	annDeallocPts(ann_pointsRecon);
#endif

	double rmseDist = 0.0, maxDist = 0.0, rmseAngle = 0.0, maxAngle = 0.0;

	for (i = 0; i < (int)outSamples.size(); i++)
	{
		// determine nearest point in input edge for point in outSamples
		CBEdge edge;
		double dist = nearestPointInEdges(outSamples[i], unitInPoints, inEdges, edge);
		rmseDist += dist*dist;

		if (dist > maxDist)
			maxDist = dist;

		// normal deviation to NN
		CBPoint outN = outNormals[i];
		CBPoint inN = getUnitNormal(edge, unitInPoints);
		double dot = outN.x*inN.x + outN.y*inN.y;

		// clamp to [-1.0,1.0] to avoid invalid acos result
		if (dot < -1.0)
			dot = -1.0;
		else
		if (dot > 1.0)
			dot = 1.0;

		double dev = acos(dot);
		rmseAngle += dev*dev;

		if (dev > maxAngle)
			maxAngle = dev;
	}

	for (i = 0; i < (int)inSamples.size(); i++)
	{
		// determine nearest point in output edge set for point in input samples
		CBEdge edge;
		double dist = nearestPointInEdges(inSamples[i], (outPoints.size() == 0) ? unitInPoints : unitOutPoints, outEdges, edge);
		rmseDist += dist*dist;

		if (dist > maxDist)
			maxDist = dist;

		// normal deviation to NN
		CBPoint inN = inNormals[i];
		CBPoint outN = getUnitNormal(edge, unitInPoints);
		double dot = outN.x*inN.x + outN.y*inN.y;

		// clamp to [-1.0,1.0] to avoid invalid acos result
		if (dot < -1.0)
			dot = -1.0;
		else
		if (dot > 1.0)
			dot = 1.0;

		double dev = acos(dot);
		rmseAngle += dev*dev;

		if (dev > maxAngle)
			maxAngle = dev;
	}

//compute symmetric difference 
double symDiffArea=boost_sym_difference(unitInPoints, inEdges, (outPoints.size() == 0) ? unitInPoints : unitOutPoints, outEdges);

	int totalSampleCount = inSamples.size() + outSamples.size();
	rmseDist = sqrt(rmseDist/totalSampleCount);
	rmseAngle = sqrt(rmseAngle/totalSampleCount);
	eval.distMax = maxDist;
	eval.distRMSE = rmseDist;
	eval.angleMax = maxAngle;
	eval.angleRMSE = rmseAngle;
     eval.symDiffArea=symDiffArea;
}
/*
 * verify whether curve (list of edges) is manifold, closed, and connected
 */
void determineCurveProperties(list<CBEdge> &edges, Result &result)
{
	int i, maxVIndex = 0;

	// map edges by their vertices
	map<int, list<int> > verticesMap;

	for (auto edge:edges)
	{
		for (i = 0; i < 2; i++)
		{
			int v = edge.v[i];
			verticesMap[v].push_back(edge.v[1 - i]);
			assert(v >= 0);

			if (v > maxVIndex)
				maxVIndex = v;
		}
	}

	// initialize with default values and then test if invalid
	result.isManifold = true;
	result.isClosed = true;

	// test whether any map entry has >2 vertices -> not manifold, or != 2 vertices -> not closed
	map<int, list<int> >::iterator mapIter = verticesMap.begin(), currIter = verticesMap.begin();

	while ((mapIter != verticesMap.end()) && (result.isManifold || result.isClosed))
	{
		int vCount = mapIter->second.size();

		if (vCount > 2)
			result.isManifold = false;

		if (vCount != 2)
			result.isClosed = false;

		if (vCount == 1)
			currIter = mapIter;

		mapIter++;
	}

	DisjointSets dSet(maxVIndex + 1);
	int root[2];

	// join all edge vertex pairs
	for (auto edge:edges)
	{
		for (i = 0; i < 2; i++)
			root[i] = dSet.FindSet(edge.v[i]);

		if (root[0] != root[1])
			dSet.Union(root[0], root[1]);
	}

	// test if all vertices of edges belong to the same joint set
	// (cannot simply use (dSet.NumSets() == 1) since not all vertex indices may be referenced in the edge set
	int rootV = edges.begin()->v[0];
	result.isConnected = true;

	for (auto edge:edges)
	{
		for (i = 0; i < 2; i++)
			if (dSet.FindSet(edge.v[i]) != rootV)
				result.isConnected = false;
	}
}

/*
 * add count outliers to points, with uniform random distribution inside their bounding box
 */
void addOutliers(vector<CBPoint> &points, int count)
{
	int i, j;
	double min[2], max[2];
	determineBoundingBox(points, min, max);

	for (i = 0; i < count; i++)
	{
		double coord[2];

		for (j = 0; j < 2; j++)
			coord[j] = min[j] + rand()/(double)RAND_MAX*(max[j] - min[j]);

		CBPoint p;
		p.x = coord[0];
		p.y = coord[1];
		points.push_back(p);
	}
}

/*
 * perturb points by uniform random noise [0..delta] in terms of bounding box diagonal
 */
void perturbPoints(vector<CBPoint> &points, double delta)
{
	int i;
	double min[2], max[2];
	determineBoundingBox(points, min, max);
	double diagonal = sqrt(SQR(max[0] - min[0]) + SQR(max[1] - min[1]));

	for (i = 0; i < (int)points.size(); i++)
	{
		points[i].x += (1.0 - 2.0*rand()/(double)RAND_MAX)*delta*diagonal;
		points[i].y += (1.0 - 2.0*rand()/(double)RAND_MAX)*delta*diagonal;
	}
}

/*
 * generate count points on unit circle and print on stdout
 */
void generateCircle(int count)
{
	int i;

	for (i = 0; i < count; i++)
	{
		double angle = 2*PI*(double)i/count;
		cout << cos(angle) << " " << sin(angle) << endl;
	}
}

/*
 * generate count points on unit circle, replacing an arc by tangents intersecting in an angle in degrees <= 180.0 and print on stdout
 */
int generateCorner(int count, float angle)
{
	int i, totalCount = 0;

	assert(angle <= 180.0);

	// generate circle as arc s.t. tangents intersect with the given angle
	for (i = 0; i < count*(180.0 + angle)/360.0; i++)
	{
		double a = 2*PI*(double)i/count;
		cout << cos(a) << " " << sin(a) << endl;
		totalCount++;
	}

	// generate tangents with equal sampling density as on circle (tangent length = tan(angle/2), count on circle is 2*PI*r)
	float len = 1.0/tan(PI*(0.5*angle)/180.0);
	float spacing = 2*PI/count;
	int lineCount = (int)ceil(len/spacing);
	float endAngleRad = 2*PI*(180.0 + angle)/360.0;
	float x = cos(endAngleRad), y = sin(endAngleRad);
	
		// DEBUG
//	cout << "linecount=" << lineCount << ", spacing=" << spacing << ", len=" << len << ", x=" << x << ", y=" << y << endl;

	// generate points on non-axis-aligned tangent towards and including corner point
	for (i = lineCount; i >= 0; i--)
	{
		cout << 1.0 - (1.0 - x)*i/lineCount << " " << -len - (-len - y)*i/lineCount << endl;
		totalCount++;
	}

	// generate points shifted by 0.5*spacing back to circle on axis-aligned tangent
	for (i = 0; i < lineCount; i++)
	{
		float yy = -len + (0.5 + i)*spacing;
		
		if (yy < 0.0)
		{
			cout << "1 " << yy << endl;
			totalCount++;
		}
	}
	
	return totalCount;
}
#ifdef TEST
void testNearestPoint()
{
	CBEdge _edge, _contEdge;
	list<CBEdge> _edges;
	_edge.v[0] = 0;
	_edge.v[1] = 1;
	_edges.push_back(_edge);
	vector<CBPoint> _points;
	CBPoint p0, p1, q;
	p0.x = 1;
	p0.y = 2;
	p1.x = 4;
	p1.y = 3;
	q.x = 7;
	q.y = 1;
	_points.push_back(p0);
	_points.push_back(p1);
	double dist = nearestPointInEdges(q, _points, _edges, _contEdge);
	cout << dist << endl;
}
#endif

/*
 * converts a string two comma-separated floats into a Point
 */
Point str2point(string str)
{
	int ofs = str.find(",");

	return Point(stof(str.substr(0, ofs)), stof(str.substr(ofs + 1, str.length() - ofs - 1)));
}

/*
 * parses SVG curveto string and extracts bezier control points
 */
void parseSVGCurveToString(string str, vector<Point> &bezierVec)
{
	int i;
	Point p[2];
	assert((str[0] == 'M') || (str[0] == 'm'));
	string::size_type ofs = str.find(' ', 2);
	string pStr = str.substr(2, ofs - 1);
	p[0] = str2point(pStr);
	char cc = str[ofs + 1];
	bool isRelative = (cc == 'c');
	assert(isRelative || (cc == 'C'));
	bool isEnd = false;
	ofs += 2;

	do
	{
		bezierVec.push_back(p[0]);

		for (i = 0; i < 3; i++)
		{
			int prevOfs = ofs;
			ofs = str.find(' ', ofs + 1);

			if (ofs == string::npos)
			{
				isEnd = true;
				ofs = str.length();
			}

			pStr.clear();
			pStr = str.substr(prevOfs, ofs - prevOfs);
			p[1] = str2point(pStr);

			if (isRelative)
				p[1] = p[1] + p[0];

			bezierVec.push_back(p[1]);
		}

		if (!isEnd)
			p[0] = p[1];

	} while (!isEnd);
}

/*
 * generate curve point set from SVG curveto string
 */
void generateCurveFromBezierString(string str, vector<Point> &curvePoints)
{
	int i, j;
	const int SAMPLE_COUNT = 300;
	string bezierStr(str);
	vector<Point> b;
	parseSVGCurveToString(bezierStr, b);

	// iterate all cubic bezier curves
	for (j = 0; j < (int)b.size()/4; j++)
	{
		// sample the cubic bezier curve by evaluating with parameter t [0..1]
		for (i = 0; i < SAMPLE_COUNT; i++)
		{
			float t = (float)i/SAMPLE_COUNT;
			Point p = b[j*4]*CUB(1 - t) + b[j*4 + 1]*3*t*SQR(1 - t) + b[j*4 + 2]*3*SQR(t)*(1 - t) + b[j*4 + 3]*CUB(t);
			curvePoints.push_back(p);
		}
	}
}

/*
 * return radius for circle through point p with normalized normal n and point q
 * q can be mirrored on the line through n, therefore the radius is the circumradius of the triangle pqq'
 */
float radiusForCircleThrough2PointsandNormal(Point p, Point n, Point q)
{
	float a = p.distance(q);
	Point n2(-n[1], n[0]);
	Point pq = p - q;
	float dist = abs(pq*n2);
	float b = 2*dist;

	if (b == 0)
		return 0.5*sqrt(pq.squared_length());	// distance pq = diameter

	float e = (2*a + b)*SQR(b)*(2*a - b);	// r=abc/sqrt((a+b+c)*(b+c-a)*(c+a-b)*(a+b-c)) -> isosceles a=b

	if (e <= 0.0)
		return numeric_limits<float>::max();	// triangle points are collinear, infinite radius

	float d = sqrt(e);
	return abs(SQR(a)*b/d);	// circumradius of triangle adapted to isosceles version
}

/*
 * compute LFS values for curve points
 */
void computeLFSForCurve(vector<Point> &curvePoints, vector<float> &lfsCurve, bool isClosed)
{
	int i, j;
	vector<Point> mPoints(curvePoints.size());

	for (i = 0; i < (int)curvePoints.size(); i++)
	{
		// compute normal
		Point prevP, nextP, currP = curvePoints[i];

		if (i > 0)
			prevP = curvePoints[i - 1];
		else
		{
			if (isClosed)
				prevP = curvePoints[curvePoints.size() - 1];
			else
				prevP = currP;
		}

		if (i < (int)curvePoints.size() - 1)
			nextP = curvePoints[i + 1];
		else
		{
			if (isClosed)
				nextP = curvePoints[0];
			else
				nextP = currP;
		}

		Point normal = prevP - nextP;
		normal.normalize();
		swap(normal[0], normal[1]);
		normal[0] = -normal[0];

		float minR = numeric_limits<float>::max();

		for (j = 0; j < (int)curvePoints.size(); j++)
			if (i != j)
			{
				// at point p, determine radius r for maximum empty circle with center through normal n (one neighbor q on its boundary)
				Point curr2P = curvePoints[j];
				float radius = radiusForCircleThrough2PointsandNormal(currP, normal, curr2P);

				if (radius < minR)
				{
					minR = radius;
					float direction = (normal*(curr2P - currP) < 0) ? -1.0 : 1.0;
					mPoints[i] = currP + normal*(radius*direction);
				}
			}
	}

	// compute lfs from nearest medial axis points
	ANNkd_tree *kdTree = NULL;
	ANNpointArray ann_points;

	ann_points = annAllocPts(mPoints.size(), 2);

	for(i = 0; i < (int)mPoints.size(); i++)
	{
		auto p = ann_points[i];
		p[0] = mPoints[i][0];
		p[1] = mPoints[i][1];
	}

	kdTree = new ANNkd_tree(ann_points, mPoints.size(), 2);
	ANNpointArray search_point = annAllocPts(1, 2);
	ANNidxArray nnIdx = new ANNidx[1];
	ANNdistArray distances = new ANNdist[1];

	for (i = 0; i < (int)curvePoints.size(); i++)
	{
		// get nearest neighbor in medial axis
		search_point[0][0] = curvePoints[i][0];
		search_point[0][1] = curvePoints[i][1];
		kdTree->annkSearch(search_point[0], 1, nnIdx, distances);
		lfsCurve[i] = sqrt(distances[0]);
	}

	delete nnIdx;
	delete distances;
	annDeallocPts(ann_points);
}

/*
 * return distance of p0 from line p1-p2
 */
float distancePFromLine(Point p0, Point p1, Point p2)
{
	Point normal(p2 - p1);
	swap(normal[0], normal[1]);
	normal[0] = -normal[0];
	normal.normalize();
	return abs(normal*(p0 - p1));
}

/*
 * generate point set (+edges) by sampling with condition < epsMax, max error to original curve and optionally manifold condition
 */
void sampleCurveByEps(vector<pair<vector<Point>, bool> > &curvePoints, float maxEps, float error,
		float perturb, vector<CBPoint> &samples/*, list<CBEdge> &origEdges*/, vector<float> &noise)
{
	int i, j;

	for (auto curve:curvePoints)
	{
		vector<float> lfsCurve;
		lfsCurve.resize(curve.first.size());
		computeLFSForCurve(curve.first, lfsCurve, curve.second);
		i = 0;
//		int startIndex = samples.size();

		do
		{
			// compute normal
			Point prevP, nextP, currP = curve.first[i];

			if (i > 0)
				prevP = curve.first[i - 1];
			else
			{
				if (curve.second)
					prevP = curve.first[curve.first.size() - 1];
				else
					prevP = currP;
			}

			if (i < (int)curve.first.size() - 1)
				nextP = curve.first[i + 1];
			else
			{
				if (curve.second)
					nextP = curve.first[0];
				else
					nextP = currP;
			}

			Point normal = prevP - nextP;
			normal.normalize();
			swap(normal[0], normal[1]);
			normal[0] = -normal[0];

			int prevI = i;
			prevP = curve.first[prevI];
			Point newP = prevP;

			if (perturb != 0.0)
			{
				// perturb p by up to perturb*lfs(p) along its normal in any direction
				float random = (float)rand()/RAND_MAX*2 - 1.0;
				newP = newP + normal*(random*perturb*lfsCurve[i]);
			}

			CBPoint sample;
			sample.x = newP[0];
			sample.y = newP[1];
			samples.push_back(sample);
			noise.push_back(perturb*lfsCurve[i]);

/*
			if (i > 0)
			{
				CBEdge edge;
				edge.v[0] = samples.size() - 1;
				edge.v[1] = samples.size();
				origEdges.push_back(edge);
			}
*/

			i++;

			// test candidate sample if it conforms to the sampling condition
			bool isConforming = true;

			while ((i < (int)curve.first.size()) && isConforming)
			{
				Point p = curve.first[i];

				j = prevI + 1;
				isConforming = true;

				// test eps and error conditions for each curve point between samples
				while ((j < i) && isConforming)
				{
					Point currP = curve.first[j];

					// test error from curve (of chord prevP-p)
					if (error > 0.0)
						isConforming = (distancePFromLine(currP, prevP, p) < error);

					if (isConforming)
					{
						// test for epsilon condition (a sample within dist/lfs < maxEps)
						float lfs = lfsCurve[j];
						float dist = currP.distance(prevP);

						if (dist/lfs < maxEps)
							isConforming = true;
						else
						{
							dist = currP.distance(p);
							isConforming = (dist/lfs < maxEps);
						}
					}

					j++;
				}

				if (isConforming)
					i++;
			}
		} while (i < (int)curve.first.size());
/*
		// if curve is closed
		if (curve.second)
		{
			CBEdge edge;
			edge.v[0] = samples.size();
			edge.v[1] = startIndex;
			origEdges.push_back(edge);
		}
*/
	}
}

/*
 * extracts cubic Bezier curves from SVG (only relative commands allowed, in the format <path d="m (x,y) c (x1 y1 x2 y2 x y)+ [z]">
 * and generates curve points by regularly sampling the curves
 */
bool readSVGFile(string filename, vector<pair<vector<Point>, bool> > &curvePoints, list<CBEdge> &origEdges)
{
	int i, pCount = 0;
//	string bunny = "m -755.54157,439.19348 c -2.81588,-35.3868 -73.42744,-49.1442 -84.52047,-72.1131 -16.34716,-33.84797 2.26058,-62.71611 17.45083,-81.27764 14.10537,-17.23587 13.61005,-19.65993 13.66234,-70.79573 0.0523,-51.13581 4.00051,-61.97973 16.1464,-62.10152 18.06052,-0.1811 11.86373,29.49677 12.70874,59.17833 1.04073,36.55644 -6.06677,54.03577 2.78541,55.27036 6.26796,0.87418 6.94735,-22.5034 11.8305,-59.79935 3.49259,-26.67529 5.60268,-54.70426 21.11452,-52.16528 15.83216,2.59141 15.67466,26.3087 8.40577,56.15545 -8.6868,35.66883 -11.40314,65.14933 10.84569,78.60485 46.36972,28.0432 87.88088,-40.45104 156.49582,-9.93625 51.81346,23.04275 60.58667,55.5695 62.10153,73.90081 4.46432,54.02268 -7.29574,55.14578 -1.24203,73.9008 6.05371,18.75502 19.00256,11.9741 19.25148,36.63989 0.40003,39.6392 -52.42394,41.64734 -156.16325,40.1841 -126.77474,-1.78816 -149.40364,-0.43075 -149.37621,-23.41669 0.0333,-27.93684 40.95673,-11.39249 38.50293,-42.22903";

	ifstream file(filename);
	string line, path;
	bool isInsidePathElement = false;

	if (file)
	{
		while (file)
		{
			getline(file, line);

			if (isInsidePathElement)
			{
				size_t pos = line.find("d=\"m");

				if (pos == string::npos)
					pos = line.find("d=\"M");

				bool isClosed = false;

				if (pos != string::npos)
				{
					pos += 3;
					size_t endPos = line.find("z");

					if (endPos == string::npos)
						endPos = line.find("Z");

					if (endPos != string::npos)
					{
						isClosed = true;
						endPos--;
					}
					else
						endPos = line.find("\"", pos);

					path = line.substr(pos, endPos - pos);
					curvePoints.push_back(pair<vector<Point>, bool>(vector<Point>(), isClosed));
					generateCurveFromBezierString(path, curvePoints.back().first);
					isInsidePathElement = false;

					for (i = 0; i < (int)curvePoints.back().first.size() - 1; i++)
					{
						CBEdge edge;
						edge.v[0] = pCount + i;
						edge.v[1] = pCount + i + 1;
						origEdges.push_back(edge);
					}

					if (isClosed)
					{
						CBEdge edge;
						edge.v[0] = pCount + curvePoints.back().first.size() - 1;
						edge.v[1] = pCount;
						origEdges.push_back(edge);
					}

					pCount += curvePoints.back().first.size();
				}
			}

			if (line.find("<path") != string::npos)
				isInsidePathElement = true;

		}

		return true;
	}
	else
		return false;
}


/*
 * reads input point/svg file and outputs reconstructed curve
 * if ground truth edges file is specified, it is evaluated against that
 */
int main(int argc, char **argv)
{
	struct arg_lit *help, *stats, *metrics;
	struct arg_int *gencircle;
	struct arg_str *alg;
	struct arg_file *infile, *gtfile, *outfile, *visfile;
struct arg_dbl *p_spacing, *p_eps, *p_delta, *p_outliers, *gencorner;
	struct arg_end *end;
	const char *progname = "curvebench";
string algorithms[] = { "lenz", "connect2d", "hnncrust", "fitconnect", "stretchdenoise", "optimaltransport", "discur", "vicur", "crawl", "peel", "crust", "nncrust", "ccrust", "gathan1", "gathang"};
const double MIN_OUTLIERS = 0.0, MAX_OUTLIERS = 1000.0;
	const double MIN_DELTA = 0.0, MAX_DELTA = 1.0, DEFAULT_DELTA = 0.0;
	const double MIN_EPS = 0.0, MAX_EPS = 1.0;
	bool hasGroundTruth = false;	string algorithmList = "algorithm:";	for (auto a:algorithms)
		algorithmList.append(" " + a);
		void *argtable[]={
			help = arg_lit0("h", "help", "displays command line arguments and options"),
			metrics = arg_lit0("m", "metrics", "output metrics on stdout as columns (without header, for scripts): isManifold | isClosed | isConnected | RMS dist (in terms of input bound diagonal) | max dist | RMS angle (radians) | max angle | isExact (non-noisy copy) | runtime (in s with ms precision) | symdiffarea"),
			stats = arg_lit0("s", "stats", "output results and statistics on stdout as columns: isManifold | isClosed | isConnected | RMS dist (in terms of input bound diagonal) | max dist | RMS angle | max angle | isExact (non-noisy copy) | runtime (in s with ms precision) | symdiffarea"),
gencircle = arg_int0("c", "generate-circle", "generates n points on a unit circle on stdout", "number of points"),
			gencorner = arg_dbl0("x", "corner", "for generate-circle: replace an arc with tangents intersecting in an angle a in degrees <= 180 on stdout", "angle"),
			alg = arg_str0("a", "algorithm", "string", algorithmList.c_str()),
			infile = arg_file0("i", "input", "input file", ".pts (points in x y lines format) or .svg (using contained cubic Bezier curves)"),
gtfile = arg_file0("g", "groundtruth", "ground truth file", ".edg (edges in v0 v1 lines format)"),
			outfile = arg_file0("o", "output", "output file", ".edg (edges in v0 v1 lines format)"),
			visfile = arg_file0("v", "visualize", "outputs visualization", "SVG file of reconstructed curve"),
			p_spacing = arg_dbl0("d", "density", "double", string(string("sampling density along edges for evaluation [0.0-1.0], default") + to_string(DEFAULT_SPACING)).c_str()),
			p_delta = arg_dbl0("n", "noise", "double", string("delta noise [" + to_string(MIN_DELTA) + "-" + to_string(MAX_DELTA) + "] (in terms of bounding box diagonal (point set input), or in terms of lfs (cubic Bezier SVG input)").c_str()),
			p_outliers = arg_dbl0("u", "outliers", "double", string("outlier percentage [" + to_string(MIN_OUTLIERS) + "-" + to_string(MAX_OUTLIERS) + "]").c_str()),
			p_eps = arg_dbl0("e", "epsilon", "double", string("epsilon sampling [" + to_string(MIN_EPS) + "-" + to_string(MAX_EPS)).c_str()),	// required for SVG cubic Bezier curve input
			end = arg_end(20)
	};

	/* verify the argtable[] entries were allocated successfully */
	if (arg_nullcheck(argtable) != 0)
	{
		/* NULL entries were detected, some allocations must have failed */
		printf("%s: insufficient memory\n", progname);
		MACRO_ERROR_EXIT
	}

	int nerrors = arg_parse(argc, argv, argtable);

	// special case: '--help' takes precedence over error reporting
	if (help->count > 0)
	{
		cout << "Usage: " << progname << endl;
		arg_print_syntax(stdout, argtable, "\n");
		cout << "This program reads a input point/SVG file and writes the reconstructed curve to a specified output file, SVG, and statistics." << endl;
		cout << "If ground truth edges file is specified, it is evaluated against that (in case of SVG file this is default ground truth)" << endl;
		arg_print_glossary(stdout, argtable, " %-25s %s\n");
		MACRO_ERROR_EXIT
	}

	// If the parser returned any errors then display them and exit
	if (nerrors > 0)
	{
		arg_print_errors(stderr, end, "curvebench");
		MACRO_ERROR_EXIT
	}

	// generate circle points on stdout and exit
	if (gencircle->count > 0)
	{
int count;
		
		if (gencorner->count > 0)
			count = generateCorner(*gencircle->ival, *gencorner->dval);
		else
		{
			count = *gencircle->ival;
			generateCircle(count);
		}
			
		if (outfile->count > 0)
		{
			list<CBEdge> edges;
			
			for (int i = 0; i < count; i++)
			{
				CBEdge edge;
				edge.v[0] = i;
				edge.v[1] = (i + 1) % count;
				edges.push_back(edge);
			}
			
			writeEdgeFile(string(*outfile->filename), edges);
		}

		MACRO_OK_EXIT
	}
if ((alg->count == 0) || (infile->count == 0))
	{
		cerr << "SYNTAX ERROR: algorithm name and input file are required parameters" << endl;
		MACRO_ERROR_EXIT
	}

	// parameter syntax is correct -> parse and handle input
	srand(0);	// consistent randomization
	vector<CBPoint> points, origPoints, newPoints;
	list<CBEdge> edges, origEdges;
	vector<float> noise;
	Result result;
	Evaluation eval;
	double sampleSpacing = DEFAULT_SPACING, delta = DEFAULT_DELTA, eps;
	int outlierCount = 0;

	if (p_delta->count > 0)
	{
		delta = *p_delta->dval;

		if ((delta < MIN_DELTA) || (delta > MAX_DELTA))
		{
			cerr << "SYNTAX ERROR: delta value not in range [" << MIN_DELTA << "-" << MAX_DELTA << "]" << endl;
			MACRO_ERROR_EXIT
		}
	}

	string infilename(*infile->filename);

	if (infilename.substr(infilename.find_last_of(".") + 1) == "svg")
	{
		vector<pair<vector<Point>, bool> > curvePoints;

		if (!readSVGFile(infilename, curvePoints, origEdges))
		{
			cerr << "INPUT ERROR: input file " << infilename << " could not be read." << endl;
			MACRO_ERROR_EXIT
		}

		if (p_eps->count > 0)
			eps = *p_eps->dval;
		else
		{
			cerr << "SYNTAX ERROR: epsilon not specified (required for cubic Bezier curve SVG input)" << endl;
			MACRO_ERROR_EXIT
		}

		if ((eps < MIN_EPS) || (eps > MAX_EPS))
		{
			cerr << "SYNTAX ERROR: epsilon value not in range [" << MIN_EPS << "-" << MAX_EPS << "]" << endl;
			MACRO_ERROR_EXIT
		}

		sampleCurveByEps(curvePoints, eps, 0.0, delta, points, noise);

		for (auto curve:curvePoints)
		{
			for (auto p:curve.first)
			{
				CBPoint point;
				point.x = p[0];
				point.y = p[1];
				origPoints.push_back(point);
			}
		}

		hasGroundTruth = true;
	}
	else
	{
		if (!readPointsFile(infilename, points))
		{
			cerr << "INPUT ERROR: input file " << infilename << " could not be read." << endl;
			MACRO_ERROR_EXIT
		}

		origPoints = points;

		if (p_delta->count > 0)
		{
			// constant noise extent
			perturbPoints(points, delta);
			noise.push_back(delta);
		}
	}

	if (p_spacing->count > 0)
		sampleSpacing = *p_spacing->dval;

	if (gtfile->count > 0)
	{
		string gtfilename = string(*gtfile->filename);

		if (!readEdgeFile(gtfilename, origEdges, origPoints))
{
			cerr << "INPUT ERROR: input file " << gtfilename << " could not be read." << endl;
			MACRO_ERROR_EXIT
		}

		hasGroundTruth = true;
	}

	if (p_outliers->count > 0)
	{
		double value = *p_outliers->dval;

		if ((value < MIN_OUTLIERS) || (value > MAX_OUTLIERS))
		{
			cerr << "SYNTAX ERROR: outlier percentage not in range [" << MIN_OUTLIERS << "-" << MAX_OUTLIERS << "]" << endl;
			MACRO_ERROR_EXIT
		}

		outlierCount = (int)points.size()*value/100.0;
		addOutliers(points, outlierCount);
	}

	string algName = string(*alg->sval);

	// reconstructs the curve from input points for the specified algorithm and outputs a list of edges and whether the curve is entirely manifold, closed and connected
	// distribute to the specific reconstruction method wrappers, with respective parameters
	PrecTimer timer;
	timer.start();

	if (algName.compare(algorithms[0]) == 0)
	{
		LenzWrapper wrapper = LenzWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
		timer.stop();
		result.timeElapsed = timer.getReal();
	}
	else
	if (algName.compare(algorithms[1]) == 0)
	{
		Connect2DWrapper wrapper = Connect2DWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
		timer.stop();
		result.timeElapsed = timer.getReal();
	}
	else
	if (algName.compare(algorithms[2]) == 0)
	{
		HnnCrustWrapper wrapper = HnnCrustWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
		timer.stop();
		result.timeElapsed = timer.getReal();
	}
	else
	if (algName.compare(algorithms[3]) == 0)
	{
		FitconnectWrapper wrapper = FitconnectWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, newPoints, edges);
		points = newPoints;	// replace old with new points with adjust coordinates
		timer.stop();
		result.timeElapsed = timer.getReal();
	}
	else
	if (algName.compare(algorithms[4]) == 0)
	{
		StretchdenoiseWrapper wrapper = StretchdenoiseWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, noise, newPoints, edges);
		points = newPoints;	// replace old with new points with adjust coordinates
		timer.stop();
		result.timeElapsed = timer.getReal();
	}
	else
	if (algName.compare(algorithms[5]) == 0){
				 
				OptimalTransportWrapper wrapper = OptimalTransportWrapper();
PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, newPoints, edges, 50);
				timer.stop();
result.timeElapsed = timer.getReal();
	}
	else
	if (algName.compare(algorithms[6]) == 0){
				 
				DISCURWrapper wrapper = DISCURWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[7]) == 0){
				 
				VICURWrapper wrapper = VICURWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[8]) == 0){
				 
				CrawlWrapper wrapper = CrawlWrapper();
PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[9]) == 0){
				 
				PeelWrapper wrapper = PeelWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges, 0);	// default: number of self-intersections
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[10]) == 0){
				 
				CrustWrapper wrapper = CrustWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[11]) == 0){
				 
				NNCrustWrapper wrapper = NNCrustWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges);
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[12]) == 0){
				 
				CCrustWrapper wrapper = CCrustWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges, 3.0);	// default cc_parameter
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[13]) == 0){
				 
				Gathan1Wrapper wrapper = Gathan1Wrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges, 60.0, 1.5);	// default max_pv_angle, min_vd_ratio
				timer.stop();
		result.timeElapsed = timer.getReal();
	}	
	else
	if (algName.compare(algorithms[14]) == 0){
				 
				GathanGWrapper wrapper = GathanGWrapper();
		PrecTimer timer;
		timer.start();
		wrapper.reconstruct(points, edges, 10.0);	// default min_corner_angle
				timer.stop();
result.timeElapsed = timer.getReal();
	}	
	else
	{
		cerr << "SYNTAX ERROR: algorithm '" << algName << "' not supported" << endl;
		MACRO_ERROR_EXIT
	}

	// collect results
	timer.stop();
	result.timeElapsed = timer.getReal();
	determineCurveProperties(edges, result);

	// evaluate with ground truth
	eval.distMax = 0.0;
	eval.distRMSE = 0.0;
	eval.angleMax = 0.0;
	eval.angleRMSE = 0.0;
		eval.symDiffArea=0.0;
eval.isExact = false;

	if (hasGroundTruth)
		evaluate(origPoints, points, origEdges, edges, sampleSpacing, eval);

	// output statistics
	if (stats->count > 0)
	{
		cout << "manif\t| closd\t| conn\t| RMSd\t| maxd\t| RMSα\t| maxα\t| exact\t| time\t| symDiff" << endl;
		cout << (result.isManifold ? "yes" : "no") << "\t| " << (result.isClosed ? "yes" : "no") << "\t| " <<
				(result.isConnected ? "yes" : "no") << "\t| " << std::fixed << std::setprecision(3) <<
				eval.distRMSE << "\t| " << eval.distMax << "\t| " << eval.angleRMSE << "\t| " << eval.angleMax << "\t| " <<
				(eval.isExact ? "yes" : "no") << "\t| " << result.timeElapsed*1000 << "\t"<<eval.symDiffArea<< endl;
	}

	// output metrics
	if (metrics->count > 0)
	{
		cout << "STAT " << (result.isManifold ? "yes" : "no") << " " << (result.isClosed ? "yes" : "no") << " " <<
				(result.isConnected ? "yes" : "no") << " " << std::fixed << std::setprecision(3) <<
				eval.distRMSE << " " << eval.distMax << " " << eval.angleRMSE << " " << eval.angleMax << " " <<
				(eval.isExact ? "yes" : "no") << " " << result.timeElapsed << " "<< eval.symDiffArea<< endl;
	}

	string filename = infilename.substr(0, infilename.find_last_of("."));

	if (outfile->count > 0)
		writeEdgeFile(string(*outfile->filename), edges);

	// output SVG file
	if (visfile->count > 0)
		writeSVGFile(string(*visfile->filename), (newPoints.size() == 0) ? points : newPoints, edges);

	MACRO_OK_EXIT
}
