#!/bin/bash

# Syntax: $0 evalname inputdir options ...

evalname=$1
inputdir=$2
hasGT=$3
shift
shift
shift
options="$@"

evalfile=${evalname}.dat
>${evalfile}

outputdir=${evalname}
mkdir ${outputdir}
htmlfile=${outputdir}/index.html

# default values for failure case: no match, max dist=0.5, max dist=PI, max runtime=60s, max sym diff=1
#FAILSTR="no no no 0.5 0.5 3.142 3.142 no 60 1"
FAILSTR="FAIL"

# NOTE: timeout includes time taken for evaluation!
TIMEOUT=60s

echo "Running `echo ${alglist}|wc -w` algorithms with `echo ${filelist}|wc -w` input sets each:"

echo "<table><caption><h1>Table ${evalname}</h1></caption>" > ${htmlfile}

# loop over all algorithms:
for alg in ${alglist}; do

    # per algorithm:
    echo -n "Running ${alg} "
    echo "<td align=\"center\">${alg}<br>" >> ${htmlfile}
    datafile=/tmp/${evalname}-${alg}.dat
    >${datafile} # initialize data file
    for file in ${filelist}; do if [ "${hasGT}" = "yes" ]; then groundtruth=" -g ${inputdir}/${file}.edg"; else groundtruth=""; fi; timeout ${TIMEOUT} ./CurveBenchmark -a ${alg} -i ${inputdir}/${file} ${groundtruth} -m ${options} -v ${outputdir}/${alg}-${file}.svg >> ${datafile}; test $? = 0 || echo ${FAILSTR} >> ${datafile}; echo -n "."; echo "<img src=\"${alg}-${file}.svg\" width=200 height=200><br>" >> ${outputdir}/index.html; done # run on all files
    echo "</td>" >> ${htmlfile}
    echo ""
    echo "${alg}`cat ${datafile} | ./addcol.sh`" >> ${evalfile}; # sum up columns per algorithm
#    rm ${datafile}
done

echo "</table>" >> ${htmlfile}
