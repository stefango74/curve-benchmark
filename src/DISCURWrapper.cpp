/*
 * DISCURWrapper.cpp
 *
 *  Created on: Aug 02, 2019
 *      Author: Stef
 */
#include "DISCURWrapper.h"
#include <vector>
#include <cassert>
#include <iostream>

#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/debug-helpers.h>


void DISCURWrapper::reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges)
{
	int i, j;
        char domain_name[] = "2DCR";
        MonoDomain *domain;
        domain = mono_jit_init (domain_name);
//printf("domain=%ld\n", (long)domain);
        MonoAssembly *assembly;
	char assemblyName[] = "../VICUR/src/bin/Debug/CRA.exe";
        assembly = mono_domain_assembly_open (domain, assemblyName);
//printf("assembly=%ld\n", (long)assembly);

        if (!assembly)
          printf("ERROR: could not load mono assembly %s\n", assemblyName);

        MonoImage *monoImage = mono_assembly_get_image(assembly);
//printf("image=%ld\n", (long)monoImage);
        MonoClass *monoClass = mono_class_from_name(monoImage, "CRT", "Mainform");
//printf("class=%ld\n", (long)monoClass);
        MonoMethodDesc* methodDesc = mono_method_desc_new("CRT.Mainform:computeDISCURWrapper", 0);
//printf("desc=%ld\n", (long)methodDesc);
        MonoMethod *method = mono_method_desc_search_in_class(methodDesc, monoClass);
//printf("method=%ld\n", (long)method);
	MonoArray *pointsArray = mono_array_new(domain, mono_get_single_class(), cbpoints.size()*2);
//printf("array=%ld\n", (long)pointsArray);

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		mono_array_set(pointsArray, float, i*2, cbpoints[i].x);
		mono_array_set(pointsArray, float, i*2 + 1, cbpoints[i].y);
	}

	void *monoArgs[1];
	monoArgs[0] = pointsArray;
	MonoArray *monoResult = (MonoArray*)mono_runtime_invoke(method, NULL, monoArgs, NULL);
	int resultLen = mono_array_length(monoResult);

//	for (i = 0; i < resultLen; i++)
//		std::cout << mono_array_get(monoResult, int, i) << " ";

	int pointCount = cbpoints.size();
//	std::cout << pointCount << ", " << resultLen << std::endl;
	assert(resultLen == pointCount*pointCount);

/*
//	MonoArray *monoResult = (MonoArray*)mono_runtime_invoke(method, NULL, monoArgs, NULL);
	MonoObject *monoResult = mono_runtime_invoke(method, NULL, monoArgs, NULL);
	int *intptr = (int*)mono_object_unbox(monoResult);
std::cout << (intptr[0]) << std::endl;
//	int resultLen = mono_array_length(monoResult);
	int pointCount = cbpoints.size();
//std::cout << "resultlen=" << resultLen << ", pointcount=" << pointCount << std::endl;
	//assert(resultLen == pointCount*pointCount);
//	vector<int> result(resultLen);
*/

	for (i = 0; i < pointCount; i++)
		for (j = 0; j < pointCount; j++)
			if (i < j)
			{
				int edgeValue = mono_array_get(monoResult, int, j*pointCount + i);
//std::cout << edgeValue;
				if (edgeValue == 1)
				{
					CBEdge cbedge;
					cbedge.v[0] = i;
					cbedge.v[1] = j;
					cbedges.push_back(cbedge);
				}
//std::cout << std::endl;
			}

}




