set title "Robustness to outliers"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set xtic auto rotate by 45 right
set yrange [0.00001:0.1]
#set logscale y
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'outliers.pdf'
plot "outliers-5.dat" using 5:xticlabels(1) title "5%", "outliers-10.dat" using 5:xticlabels(1) title "10%", "outliers-20.dat" using 5:xticlabels(1) title "20%"
