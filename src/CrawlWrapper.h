#ifndef CRAWLWRAPPER_H_
#define CRAWLWRAPPER_H_

#include "Crawl.h"

class CrawlWrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges);
};

#endif /* CRAWLWRAPPER_H_ */
