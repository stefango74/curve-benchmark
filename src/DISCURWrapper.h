/*
 * DISCURWrapper.h
 *
 *  Created on: Aug 02, 2019
 *      Author: Stef
 */

#ifndef DISCURWRAPPER_H_
#define DISCURWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"

using namespace std;

class DISCURWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges);
};

#endif /* DISCURWRAPPER_H_ */



