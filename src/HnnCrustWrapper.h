/*
 * HnnCrustWrapper.h
 *
 *  Created on: Dec 5, 2018
 *      Author: stef
 */

#ifndef HNNCRUSTWRAPPER_H_
#define HNNCRUSTWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"
#include "../hnn-crust-sgp16/src/Reconstruct2D.h"

class HnnCrustWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges);
};

#endif /* HNNCRUSTWRAPPER_H_ */
