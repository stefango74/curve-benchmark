/*
 * Connect2DWrapper.cpp
 *
 *  Created on: Dec 5, 2018
 *      Author: stef
 */

#include "Connect2DWrapper.h"

using namespace connect2d;

void Connect2DWrapper::reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges)
{
	int i;
	vector<pair<double, double> > points(cbpoints.size());

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		points[i].first = cbpoints[i].x;
		points[i].second = cbpoints[i].y;
	}

	Connect2D *instance = new Connect2D(&points);
	vector<int> *boundary = instance->getBoundary();

	for (i = 0; i < (int)boundary->size(); i++)
	{
		CBEdge cbedge;
		cbedge.v[0] = (*boundary)[i];
		cbedge.v[1] = (*boundary)[(i + 1) % boundary->size()];
		cbedges.push_back(cbedge);
	}
}

