/*
 * LenzWrapper.cpp
 *
 *  Created on: Dec 6, 2018
 *      Author: stef
 */

#include "LenzWrapper.h"
#include "../lenz/main.cpp"

void LenzWrapper::reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges)
{
	unsigned int i, j;

	if (cbpoints.size() > MAX_VERTICES)
		return;

	numVertices = cbpoints.size();
	memset(taken, 0, numVertices*numVertices*sizeof(taken[0]));

	for (i = 0; i < numVertices; i++)
	{
		V[i].x = cbpoints[i].x;
		V[i].y = cbpoints[i].y;
		V[i].degree = 0;
		V[i].indirectPos = i;
		unusedVertices[i] = i;
	}

	numUnusedVertices = numVertices;
	numEdgesCreated = 0;
	maxDegree = 0;
	numComponents = 0;
	reconstructLenz();

	for (i = 0; i < numVertices; i++)
		for (j = 0; j < numVertices; j++)
			if (taken[j*numVertices + i])
			{
				CBEdge cbedge;
				cbedge.v[0] = i;
				cbedge.v[1] = j;
				cbedges.push_back(cbedge);
			}
}

