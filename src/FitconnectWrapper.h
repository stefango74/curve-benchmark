/*
 * FitconnectWrapper.h
 *
 *  Created on: Dec 5, 2018
 *      Author: stef
 */

#ifndef FITCONNECTWRAPPER_H_
#define FITCONNECTWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"
#include "../fitconnect/src/Reconstruct2D.h"

class FitconnectWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, vector<CBPoint> &newcbpoints, list<CBEdge> &cbedges);
};

#endif /* FITCONNECTWRAPPER_H_ */
