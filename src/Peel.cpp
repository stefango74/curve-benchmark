#include"Peel.h"
namespace peel
{
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Delaunay;
typedef K::Point_2 Point;
//int wd,loop,ends1[NENDS][2];
Point shape[500000][2],input_points[500000],deleted[500000][2],shape1[500000][2],shape2[500000][2],tempv[1000][2];;
int vertex_count=0,shape_index=0,minx=9999,miny=9999,maxx=0,maxy=0,vertex_size,shape_fl,shape1_index=0,shape2_index=0,di=0;
Delaunay dt;
std::vector<std::pair<Point,Point>> boundary;
/**********PRIORITY QUEUE FUNCTIONS**********/
struct node
{
    float priority;
    Delaunay::Edge_iterator afi;
    struct node *link;
};
class prio_queue
{
private:
    node *front;
public:
    prio_queue()
    {
        front = NULL;
    }
    void pq_insert(float priority,Delaunay::Edge_iterator item)/*Inserting an item into priority queue*/
    {
        node *tmp, *q;
        tmp = new node;
        tmp->afi = item;
        tmp->priority = priority;
        if (front == NULL || priority < front->priority)
        {
            tmp->link = front;
            front = tmp;
        }
        else
        {
            q = front;
            while (q->link != NULL && q->link->priority <= priority)
                q=q->link;
            tmp->link = q->link;
            q->link = tmp;
        }
    }
    Delaunay::Edge_iterator pq_del() /* Deleting an item from priority queue*/
    {
        node *tmp;
        if(front == NULL)
            std::cout<<"Queue Underflow\n";
        else
        {
            tmp = front;
            Delaunay::Edge_iterator afi_new;
            afi_new=tmp->afi;
            front = front->link;
            free(tmp);
            return afi_new;
        }
    }
    bool pq_empty() /*Checks whether the priority queue is empty or not*/
    {
        if(front==NULL)
            return 1;
        return 0;
    }
};
prio_queue pq;
void pointset(void)
{
    for(int i=0;i<shape1_index;i++) /*Displaying edges*/
        boundary.push_back(std::pair<Point,Point>(shape1[i][0],shape1[i][1]));
}
/**********RECONSTRUCTION FUNCTIONS**********/
float distance(Point a, Point b)
{
    return (float)(sqrt(abs(((a.x()-b.x())*(a.x()-b.x())))+abs(((a.y()-b.y())*(a.y()-b.y())))));
}
void insert_to_shape(Point a,Point b)
{
    for(int i=0;i<shape_index;i++)
        if((shape[i][0]==a&&shape[i][1]==b)||(shape[i][0]==b&&shape[i][1]==a))
            return;
    shape[shape_index][0]=a;
    shape[shape_index][1]=b;
    shape_index++;
}
bool notsmallest(Delaunay::Vertex_handle a,Delaunay::Vertex_handle b)
{
    Delaunay::Vertex_handle vh1;
    Delaunay::Vertex_circulator vc=dt.incident_vertices(a),done(vc);
    double min1=99999;
    if (vc != 0) {
        do {
            if(distance(vc->point(),a->point())<min1)
            {
                min1=distance(vc->point(),a->point());
                vh1=vc;
            }
        }while(++vc != done);
        if(vh1->point()==b->point())
            return 0;
    }
    Delaunay::Vertex_circulator vc1=dt.incident_vertices(b),done1(vc1);
    min1=99999;
    if (vc1 != 0) {
        do {
            if(distance(vc1->point(),b->point())<min1)
            {
                min1=distance(vc1->point(),b->point());
                vh1=vc1;
            }
        }while(++vc1 != done1);
        if(vh1->point()==a->point())
            return 0;
    }
    return 1;
}
int search_in_shape(Point a,Point b)
{
    for(int i=0;i<shape_index;i++)
        if((shape[i][0]==a&&shape[i][1]==b)||(shape[i][0]==b&&shape[i][1]==a))
            return i;
    return -1;
}
int deleted_edge(Point a,Point b)
{
    for(int i=0;i<di;i++)
        if((deleted[i][0]==a&&deleted[i][1]==b)||(deleted[i][0]==b&&deleted[i][1]==a))
            return 1;
    return 0;
}
int notexist(Point a,Point b)
{
    for(int i=0;i<shape1_index;i++)
    {
        if(shape1[i][0]==a&&shape1[i][1]==b)
            return 0;
        if(shape1[i][0]==b&&shape1[i][1]==a)
            return 0;
    }
    return 1;
}
void Peel::_reconstruct(std::vector<Point> p,float len)
{
   for(int i=0;i<p.size();i++)
    dt.insert(Point(p.at(i).x(),p.at(i).y()));/*Compute Delaunay triangulation*/
    Delaunay::Vertex_iterator vi=dt.vertices_begin();
    do{
        if(vi->point().x()>maxx)
            maxx=vi->point().x();
        if(vi->point().y()>maxy)
            maxy=vi->point().y();
        if(vi->point().x()<minx)
            minx=vi->point().x();
        if(vi->point().y()<miny)
            miny=vi->point().y();
        vi++;
    }while(vi!=dt.vertices_end());
//    printf("Enter the parameter for restoring self-intersections (0 if no self-intersections): ");
   // float len;
   // scanf("%f",&len);
    for (Delaunay::Finite_faces_iterator it = dt.finite_faces_begin(); it != dt.finite_faces_end(); it++)
    {
        if(distance(dt.triangle(it)[0],dt.triangle(it)[1])>=distance(dt.triangle(it)[0],dt.triangle(it)[2])
           &&distance(dt.triangle(it)[0],dt.triangle(it)[1])>=distance(dt.triangle(it)[1],dt.triangle(it)[2]))
        {
            deleted[di][0]=dt.triangle(it)[0];
            deleted[di][1]=dt.triangle(it)[1];
            di++;
            int index=search_in_shape(dt.triangle(it)[0],dt.triangle(it)[1]);
            if(index>-1)
            {
                for(int i=index;i<shape_index-1;i++)
                {
                    shape[i][0]=shape[i+1][0];
                    shape[i][1]=shape[i+1][1];
                }
                shape_index--;
            }
            if(!deleted_edge(dt.triangle(it)[0],dt.triangle(it)[2]))
                insert_to_shape(dt.triangle(it)[0],dt.triangle(it)[2]);
            if(!deleted_edge(dt.triangle(it)[1],dt.triangle(it)[2]))
                insert_to_shape(dt.triangle(it)[2],dt.triangle(it)[1]);
        }
        if(distance(dt.triangle(it)[0],dt.triangle(it)[2])>=distance(dt.triangle(it)[0],dt.triangle(it)[1])
           &&distance(dt.triangle(it)[0],dt.triangle(it)[2])>=distance(dt.triangle(it)[1],dt.triangle(it)[2]))
        {
            deleted[di][0]=dt.triangle(it)[0];
            deleted[di][1]=dt.triangle(it)[2];
            di++;
            int index=search_in_shape(dt.triangle(it)[0],dt.triangle(it)[2]);
            if(index>-1)
            {
                for(int i=index;i<shape_index-1;i++)
                {
                    shape[i][0]=shape[i+1][0];
                    shape[i][1]=shape[i+1][1];
                }
                shape_index--;
            }
            if(!deleted_edge(dt.triangle(it)[0],dt.triangle(it)[1]))
                insert_to_shape(dt.triangle(it)[0],dt.triangle(it)[1]);
            if(!deleted_edge(dt.triangle(it)[1],dt.triangle(it)[2]))
                insert_to_shape(dt.triangle(it)[1],dt.triangle(it)[2]);
        }
        if(distance(dt.triangle(it)[2],dt.triangle(it)[1])>=distance(dt.triangle(it)[0],dt.triangle(it)[1])
           &&distance(dt.triangle(it)[2],dt.triangle(it)[1])>=distance(dt.triangle(it)[0],dt.triangle(it)[2]))
        {
            deleted[di][0]=dt.triangle(it)[2];
            deleted[di][1]=dt.triangle(it)[1];
            di++;
            int index=search_in_shape(dt.triangle(it)[2],dt.triangle(it)[1]);
            if(index>-1)
            {
                for(int i=index;i<shape_index-1;i++)
                {
                    shape[i][0]=shape[i+1][0];
                    shape[i][1]=shape[i+1][1];
                }
                shape_index--;
            }
            if(!deleted_edge(dt.triangle(it)[0],dt.triangle(it)[1]))
                insert_to_shape(dt.triangle(it)[0],dt.triangle(it)[1]);
            if(!deleted_edge(dt.triangle(it)[0],dt.triangle(it)[2]))
                insert_to_shape(dt.triangle(it)[0],dt.triangle(it)[2]);
        }
    }
    for(int i=0;i<shape_index;i++)
    {
        Point first[1000];
        int fi=0;
        float first_dist[1000];
        for(int j=0;j<shape_index;j++)
        {
            if(j!=i)
                if(shape[i][0]==shape[j][0]||shape[i][0]==shape[j][1])
                {
                    if(shape[i][0]==shape[j][0])
                        first[fi]=shape[j][1];
                    if(shape[i][0]==shape[j][1])
                        first[fi]=shape[j][0];
                    fi++;
                }
        }
        int degree_count=0;
        float smallest=9999,smallest1=9999;
        if(fi>1)
        {
            for(int j=0;j<fi;j++)
                first_dist[j]=distance(shape[i][0],first[j]);
            float org_dist=distance(shape[i][0],shape[i][1]);
            smallest=first_dist[0];
            for(int j=0;j<fi;j++)
                if(first_dist[j]<org_dist)
                {
                    if(first_dist[j]<smallest)
                        smallest=first_dist[j];
                    degree_count++;
                }
        }
        for(int j=0;j<shape_index;j++)
        {
            if(j!=i)
                if(shape[i][1]==shape[j][0]||shape[i][1]==shape[j][1])
                {
                    if(shape[i][1]==shape[j][0])
                        first[fi]=shape[j][1];
                    if(shape[i][1]==shape[j][1])
                        first[fi]=shape[j][0];
                    fi++;
                }
        }
        int degree_count1=0;
        if(fi>1)
        {
            for(int j=0;j<fi;j++)
                first_dist[j]=distance(shape[i][1],first[j]);
            float org_dist=distance(shape[i][0],shape[i][1]);
            smallest1=first_dist[0];
            for(int j=0;j<fi;j++)
                if(first_dist[j]<org_dist)
                {
                    if(first_dist[j]<smallest1)
                        smallest1=first_dist[j];
                    degree_count1++;
                }
        }
        if(smallest1>smallest)
            smallest=smallest1;
        if((degree_count<2&&degree_count1<2))
        {
            shape1[shape1_index][0]=shape[i][0];
            shape1[shape1_index][1]=shape[i][1];
            shape1_index++;
        }
    }
    for(int i=0;i<shape1_index;i++)
    {
        int deg1=0,deg2=0;
        for(int j=0;j<shape1_index;j++)
        {
            if(j!=i)
            {
                if(shape1[i][1]==shape1[j][0]||shape1[i][1]==shape1[j][1])
                {
                    deg1++;
                }
                if(shape1[i][0]==shape1[j][0]||shape1[i][0]==shape1[j][1])
                {
                    deg2++;
                }
            }
        }
        Point tv1,tv2;
        double min=9999;
        int fl=0;
        if(((deg2==0)))
            for (Delaunay::Finite_faces_iterator it = dt.finite_faces_begin(); it != dt.finite_faces_end(); it++)
            {
                if(dt.triangle(it)[0]==shape1[i][0])
                {
                    if(distance(dt.triangle(it)[0],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                    {
                        if(min>distance(dt.triangle(it)[0],dt.triangle(it)[1])&&notexist(dt.triangle(it)[0],dt.triangle(it)[1]))
                        {
                            min=distance(dt.triangle(it)[0],dt.triangle(it)[1]);
                            tv1=dt.triangle(it)[1];
                            tv2=dt.triangle(it)[0];
                            fl=1;
                        }

                    }
                    if(distance(dt.triangle(it)[0],dt.triangle(it)[2])<len*distance(shape1[i][0],shape1[i][1]))
                    {
                        if(min>distance(dt.triangle(it)[0],dt.triangle(it)[2])&&notexist(dt.triangle(it)[0],dt.triangle(it)[2]))
                        {
                            min=distance(dt.triangle(it)[0],dt.triangle(it)[2]);
                            tv1=dt.triangle(it)[2];
                            tv2=dt.triangle(it)[0];
                            fl=1;
                        }
                    }
                }
                else
                {
                    if(dt.triangle(it)[1]==shape1[i][0])
                    {
                        if(distance(dt.triangle(it)[0],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                        {
                            if(min>distance(dt.triangle(it)[0],dt.triangle(it)[1])&&notexist(dt.triangle(it)[0],dt.triangle(it)[1]))
                            {
                                min=distance(dt.triangle(it)[0],dt.triangle(it)[1]);
                                tv1=dt.triangle(it)[1];
                                tv2=dt.triangle(it)[0];
                                fl=1;
                            }
                        }
                        if(distance(dt.triangle(it)[2],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                        {
                            if(min>distance(dt.triangle(it)[2],dt.triangle(it)[1])&&notexist(dt.triangle(it)[2],dt.triangle(it)[1]))
                            {
                                min=distance(dt.triangle(it)[2],dt.triangle(it)[1]);
                                tv1=dt.triangle(it)[1];
                                tv2=dt.triangle(it)[2];
                                fl=1;
                            }
                        }
                    }
                    else
                    {
                        if(dt.triangle(it)[2]==shape1[i][0])
                        {
                            if(distance(dt.triangle(it)[0],dt.triangle(it)[2])<len*distance(shape1[i][0],shape1[i][1]))
                            {
                                if(min>distance(dt.triangle(it)[0],dt.triangle(it)[2])&&notexist(dt.triangle(it)[0],dt.triangle(it)[2]))
                                {
                                    min=distance(dt.triangle(it)[0],dt.triangle(it)[2]);
                                    tv1=dt.triangle(it)[2];
                                    tv2=dt.triangle(it)[0];
                                    fl=1;
                                }
                            }
                            if(distance(dt.triangle(it)[2],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                            {
                                if(min>distance(dt.triangle(it)[1],dt.triangle(it)[2])&&notexist(dt.triangle(it)[2],dt.triangle(it)[1]))
                                {
                                    min=distance(dt.triangle(it)[1],dt.triangle(it)[2]);
                                    tv1=dt.triangle(it)[1];
                                    tv2=dt.triangle(it)[2];
                                    fl=1;
                                }

                            }
                        }

                    }
                }

            }
        if(fl==1)
        {
            shape1[shape1_index][0]=tv1;
            shape1[shape1_index][1]=tv2;
            shape1_index++;
        }
        min=9999;
        fl=0;
        if(((deg1==0)))
            for (Delaunay::Finite_faces_iterator it = dt.finite_faces_begin(); it != dt.finite_faces_end(); it++)
            {
                if(dt.triangle(it)[0]==shape1[i][1])
                {
                    if(distance(dt.triangle(it)[0],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                    {
                        if(min>distance(dt.triangle(it)[0],dt.triangle(it)[1])&&notexist(dt.triangle(it)[0],dt.triangle(it)[1]))
                        {
                            min=distance(dt.triangle(it)[0],dt.triangle(it)[1]);
                            tv1=dt.triangle(it)[1];
                            tv2=dt.triangle(it)[0];
                            fl=1;
                        }

                    }
                    if(distance(dt.triangle(it)[0],dt.triangle(it)[2])<len*distance(shape1[i][0],shape1[i][1]))
                    {
                        if(min>distance(dt.triangle(it)[0],dt.triangle(it)[2])&&notexist(dt.triangle(it)[0],dt.triangle(it)[2]))
                        {
                            min=distance(dt.triangle(it)[0],dt.triangle(it)[2]);
                            tv1=dt.triangle(it)[2];
                            tv2=dt.triangle(it)[0];
                            fl=1;
                        }
                    }
                }
                else
                {
                    if(dt.triangle(it)[1]==shape1[i][1])
                    {
                        if(distance(dt.triangle(it)[0],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                        {
                            if(min>distance(dt.triangle(it)[0],dt.triangle(it)[1])&&notexist(dt.triangle(it)[0],dt.triangle(it)[1]))
                            {
                                min=distance(dt.triangle(it)[0],dt.triangle(it)[1]);
                                tv1=dt.triangle(it)[1];
                                tv2=dt.triangle(it)[0];
                                fl=1;
                            }
                        }
                        if(distance(dt.triangle(it)[2],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                        {
                            if(min>distance(dt.triangle(it)[2],dt.triangle(it)[1])&&notexist(dt.triangle(it)[2],dt.triangle(it)[1]))
                            {
                                min=distance(dt.triangle(it)[2],dt.triangle(it)[1]);
                                tv1=dt.triangle(it)[1];
                                tv2=dt.triangle(it)[2];
                                fl=1;
                            }
                        }
                    }
                    else
                    {
                        if(dt.triangle(it)[2]==shape1[i][1])
                        {
                            if(distance(dt.triangle(it)[0],dt.triangle(it)[2])<len*distance(shape1[i][0],shape1[i][1]))
                            {
                                if(min>distance(dt.triangle(it)[0],dt.triangle(it)[2])&&notexist(dt.triangle(it)[0],dt.triangle(it)[2]))
                                {
                                    min=distance(dt.triangle(it)[0],dt.triangle(it)[2]);
                                    tv1=dt.triangle(it)[2];
                                    tv2=dt.triangle(it)[0];
                                    fl=1;
                                }
                            }
                            if(distance(dt.triangle(it)[2],dt.triangle(it)[1])<len*distance(shape1[i][0],shape1[i][1]))
                            {
                                if(min>distance(dt.triangle(it)[1],dt.triangle(it)[2])&&notexist(dt.triangle(it)[2],dt.triangle(it)[1]))
                                {
                                    min=distance(dt.triangle(it)[1],dt.triangle(it)[2]);
                                    tv1=dt.triangle(it)[1];
                                    tv2=dt.triangle(it)[2];
                                    fl=1;
                                }

                            }
                        }

                    }
                }

            }
        if(fl==1)
        {
            shape1[shape1_index][0]=tv1;
            shape1[shape1_index][1]=tv2;
            shape1_index++;
        }
    }
}
std::vector<std::pair<Point,Point>> *Peel::getBoundary()
{
pointset();
return &boundary;
}
}
