#include "PeelWrapper.h"

using namespace peel;

void PeelWrapper::reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges, float len)
{
	int i;
	std::vector<Point> points(cbpoints.size());
	std::map<Point, int> pointMap;

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		points[i] = Point(cbpoints[i].x, cbpoints[i].y);
		pointMap[points[i]] = i;
	}

	Peel *peel = new Peel();
	peel->_reconstruct(points, len);
	std::vector<std::pair<Point,Point>> *boundary = peel->getBoundary();
	
	for (auto item:*boundary)
	{
		CBEdge cbedge;
		cbedge.v[0] = pointMap[item.first];
		cbedge.v[1] = pointMap[item.second];
		cbedges.push_back(cbedge);
	}
}

