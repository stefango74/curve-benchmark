#!/bin/bash

evalname=manifold
_alglist="lenz connect2d hnncrust stretchdenoise fitconnect optimaltransport"
_filelist="../Dataset/Text_FilesMPEG/*"

env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname} . yes

#gnuplot -c manifold-ismanifold.plot
#gnuplot -c manifold-runtime.plot

