set title "lfs-varying Noisy sampling"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set xtic auto rotate by 45 right
set yrange [0.001:0.1]
#set logscale y
set xtic 
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'sampling-noise.pdf'
plot "sampling-noise-0.1.dat" using 5:xticlabels(1) title "0.1", "sampling-noise-0.2.dat" using 5:xticlabels(1) title "0.2", "sampling-noise-0.4.dat" using 5:xticlabels(1) title "0.4"
