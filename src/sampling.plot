set title "LFS-varying sampling density"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set xtic auto rotate by 45 right
set yrange [0.0001:0.1]
#set logscale y
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'sampling-density.pdf'
plot "sampling-0.25.dat" using 5:xticlabels(1) title "0.25", "sampling-0.5.dat" using 5:xticlabels(1) title "0.5", "sampling-0.75.dat" using 5:xticlabels(1) title "0.75"
