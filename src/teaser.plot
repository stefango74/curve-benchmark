set title "Analysis of various algorithms"
set style data lp
set key over
set autoscale
set xtic auto rotate by 45 right
set yrange [0.001:2]
set logscale y
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'teaser.pdf'
plot "teaser.dat" using 5:xticlabels(1) title "Distance"
