#!/bin/bash

export LC_ALL=C # to fix locale issues of "." vs "," in numbers, resulting in integer .dat files and subsequent empty plots

echo -e "Replicating the Figures..."
echo -e "Generating Figures in the teaser:"
./run-teaser.sh
echo -e "Generating Tables 9 to 14 and 16 to 20:"
./run-sampling.sh
./run-noisy.sh
./run-lfsnoise.sh
./run-sampling-noise.sh
./run-outliers.sh
./run-manifold.sh
./run-non-manifold.sh
./run-sharp-corners.sh
./run-open-curves.sh
./run-multiple-curves.sh
echo -e "Generating Figures in Figure 15:"
./run-qualitative.sh
