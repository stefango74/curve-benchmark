set title "Non-manifold curve reconstruction"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set yrange [0.0:0.015]
#set logscale y
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'Non-manifold.pdf'
plot "Non-manifold.dat" using 5:xticlabels(1) notitle
