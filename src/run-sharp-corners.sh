#!/bin/bash

evalname=sharp-corners
_alglist="connect2d hnncrust fitconnect stretchdenoise discur vicur crawl peel crust nncrust ccrust gathan1 gathang lenz"

_filelist="sc1.txt sc2.txt sc3.txt sc4.txt sc5.txt sc6.txt sc7.txt sc8.txt sc9.txt sc10.txt sc11.txt sc12.txt sc13.txt sc14.txt sc15.txt sc16.txt sc17.txt sc18.txt sc19.txt sc20.txt sc21.txt"

_imagelist="test10_10.p2d
test10_16.p2d
test15_20.p2d
test20_10.p2d
test20_16.p2d
test25_20.p2d
test30_10.p2d
test30_16.p2d
test35_20.p2d
test40_10.p2d
test40_16.p2d
test45_20.p2d
test50_10.p2d
test50_16.p2d
test50_20.p2d
test5_20.p2d
test60_16.p2d
test60_20.p2d
test65_10.p2d
test70_16.p2d
test70_20.p2d
test75_10.p2d
test80_16.p2d
test80_20.p2d
test85_10.p2d
test90_20.p2d"

_filepath1=../testdata/sharp-corners/classic-data/

_filepath2=../testdata/sharp-corners/synthetic-data

_gt=../testdata/sharp-corners/synthetic-data


env alglist="${_alglist}" filelist="${_filelist}" imagelist="${_imagelist}" inputdir2=${_filepath2} gtdir=${_gt} ./run-evalsub-modified.sh ${evalname} ${_filepath1} yes 

gnuplot -c sharp-corners.plot



