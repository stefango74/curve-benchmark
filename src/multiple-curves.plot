set title "Reconstruction of multiple curves"
set style data histograms
set style histogram cluster
set style fill solid border
set autoscale
set xtic auto rotate by 45 right
set ytic auto
set yrange[0:100]
set xlabel "Algorithm"
set ylabel "Percentage of exactly reconstructed curves"
set term pdfcairo
set output 'multiple-curves.pdf'
plot "multiple-curves.dat" using 9:xticlabels(1) notitle
