/*
 * Connect2DWrapper.h
 *
 *  Created on: Dec 5, 2018
 *      Author: stef
 */

#ifndef CONNECT2DWRAPPER_H_
#define CONNECT2DWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"
#include "../connect2d-1.0.2/Connect2D.h"

using namespace std;

class Connect2DWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges);
};

#endif /* CONNECT2DWRAPPER_H_ */
