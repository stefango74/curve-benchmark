#!/usr/bin/awk -f
# Sum up numerical values by column (white-space separated) or compute percentage of yes's
#
# Usage:  $0 [file ...]
#

{
  if ($1 == "STAT")
  {
    for (i = 2; i <= NF; i++)
    {
	if ($i == "yes")
	    col[i] += 100;
	else
            col[i] += $i;
    }

    sum += 1;

    if (NF > maxcol)
	maxcol = NF;
  }
}

END {
    for (i = 2; i <= maxcol; i++)
    {
	printf " " col[i]/sum;
    }

    print "";
}
