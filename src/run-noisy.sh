#!/bin/bash

evalname=noisy
_alglist="connect2d hnncrust fitconnect stretchdenoise crawl peel crust nncrust ccrust gathan1 gathang lenz discur vicur"
_filelist="mc10.txt
mc11.txt
mc12.txt
mc13.txt
mc14.txt
mc15.txt
mc16.txt
mc19.txt
mc20.txt
mc21.txt
mc24.txt
mc26.txt
mc29.txt
mc32.txt
mc3.txt
mc44.txt
mc45.txt
mc47.txt
mc4.txt
mc52.txt
mc5.txt
mc6.txt
mc7.txt
mc8.txt
mc9.txt"

for noise in 0 0.003 0.01 0.03; do
  echo "Running with noise ${noise}:"
  env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname}-${noise} ../testdata/manifold/classic-data/ yes -n ${noise};
done

gnuplot -c noisy.plot
