#!/bin/bash

evalname=sampling-noise
_alglist="connect2d hnncrust fitconnect stretchdenoise crawl peel crust nncrust ccrust gathan1 gathang lenz discur vicur"
_filelist="bunny.svg"

for epsilon in 0.1 0.2 0.4; do
 echo "Running with epsilon ${epsilon}:"
 env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname}-${epsilon} ../testdata/sampling no -n0.333 -e${epsilon};
done

gnuplot -c sampling-noise.plot