#!/bin/bash

evalname=sampling
_alglist="connect2d hnncrust fitconnect stretchdenoise crawl peel crust nncrust ccrust gathan1 gathang lenz discur vicur"
_filelist="bunny.svg"

for epsilon in 0.25 0.5 0.75; do
echo "Running with epsilon ${epsilon}:"
env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname}-${epsilon} ../testdata/sampling no -e${epsilon};
done

gnuplot -c sampling.plot
