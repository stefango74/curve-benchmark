/*
 * StretchdenoiseWrapper.h
 *
 *  Created on: Nov 8, 2018
 *      Author: stef
 */

#ifndef STRETCHDENOISEWRAPPER_H_
#define STRETCHDENOISEWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"
#include "../stretchdenoise/src/Reconstruct2D.h"

class StretchdenoiseWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, vector<float> noise, vector<CBPoint> &newcbpoints, list<CBEdge> &cbedges);
};

#endif /* STRETCHDENOISEWRAPPER_H_ */
