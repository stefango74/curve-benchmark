#!/bin/bash

evalname=open-curves
_alglist="hnncrust fitconnect discur vicur crawl peel crust nncrust ccrust gathan1 gathang lenz"
_filelist="oc1.txt oc2.txt oc3.txt oc4.txt oc6.txt oc7.txt oc8.txt oc9.txt oc10.txt oc11.txt oc12.txt oc13.txt oc14.txt oc15.txt oc16.txt oc17.txt oc18.txt oc19.txt oc20.txt oc21.txt oc22.txt oc23.txt oc24.txt"

env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname} ../testdata/open-curves yes

gnuplot -c open-curves.plot
