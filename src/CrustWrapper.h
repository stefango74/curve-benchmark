#ifndef CRUSTWRAPPER_H_
#define CRUSTWRAPPER_H_

#include "../crust-family/recon2d.h"
#include "CurveBenchmarkDataTypes.h"

class CrustWrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges);
};

#endif /* CRUSTWRAPPER_H_ */
