#ifndef NNCRUSTWRAPPER_H_
#define NNCRUSTWRAPPER_H_

#include "../crust-family/recon2d.h"
#include "CurveBenchmarkDataTypes.h"

class NNCrustWrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges);
};

#endif /* NNCRUSTWRAPPER_H_ */
