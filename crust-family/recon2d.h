// header file for reconstruction algorithms

#ifndef RECON2D_H
#define RECON2D_H

//***********************************************************
// if REAL is undefined, then #define REAL for triangle.h

#ifndef REAL
#ifdef SINGLE
#define REAL float
#else /* not SINGLE */
#define REAL double
#endif /* not SINGLE */
#endif
//***********************************************************

// 2d crust algorithm
void crust2d(const REAL * pointcoord, const int num_points, 
	     int * & edgelist, int & num_edges);

void crust2d(const REAL * pointcoord, const int num_points, 
	     const struct triangulateio & deltri, 
	     const struct triangulateio & vor,
	     int * & edgelist, int & num_edges);

// 2d nearest neighbor algorithm
void nearest_recon2d(const REAL * pointcoord, const int num_points, 
		     int * & edgelist, int & num_edges);

void nearest_recon2d(const REAL * pointcoord, const int num_points, 
		     const struct triangulateio & deltri, 
		     int * & edgelist, int & num_edges);

// 2d conservative crust reconstruction algorithm
void conservative_crust2d
  (const REAL * pointcoord, const int num_points, const float cc_parameter,
   int * & edgelist, int & num_edges);

void conservative_crust2d
  (const REAL * pointcoord, const int num_points, const float cc_parameter,
   const struct triangulateio & deltri, const struct triangulateio & vor, 
   int * & edgelist, int & num_edges);

// gathanG curve reconstruction algorithm
void gathanG(const REAL * pointcoord, const int num_points, 
	     const float min_corner_angle, int * & edgelist, int & num_edges);

void gathanG(const REAL * pointcoord, const int num_points, 
	     const float min_corner_angle, const int max_iter,
	     int * & edgelist, int & num_edges);

void gathanG
  (const REAL * pointcoord, const int num_points, const float min_corner_angle,
   const struct triangulateio & deltri, const struct triangulateio & vor,
   int * & edgelist, int & num_edges);

void gathanG
  (const REAL * pointcoord, const int num_points, 
   const float min_corner_angle, const int max_iter,
   const struct triangulateio & deltri, const struct triangulateio & vor,
   int * & edgelist, int & num_edges);

// original gathan curve reconstruction algorithm
void gathan1(const REAL * pointcoord, const int num_points, 
	     const float max_pv_angle, const float min_vd_ratio,
	     int * & edgelist, int & num_edges);
void gathan1(const REAL * pointcoord, const int num_points, 
	     const float max_pv_angle, const float min_vd_ratio,
	     const struct triangulateio & deltri, 
	     const struct triangulateio & vor,
	     const REAL * pole_dir,
	     int * & edgelist, int & num_edges);

// algorithm to compute pole directions
void compute_pole_dir2d(const REAL * pointcoord, const int num_points, 
			const struct triangulateio & deltri, 
			const struct triangulateio & vor,
			REAL * pole_dir_coord);

#endif /* RECON2D_H */

